<?php

namespace App\Http\Controllers;

use App\Messagrie;
use Illuminate\Http\Request;

class MessagrieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $data)
    {
        $rules = [
            'nom' => ['required', 'string', 'max:255'],
            'prenom' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'max:255'],
            'tel' => ['required', 'string', 'max:255'],
            'message' => ['required', 'string'],
        ];

        $niceNames = array(
            'nom' => 'prenom',
            'prenom' => 'prenom',
            'email' => 'email',
            'tel' => 'telephone',
            'message' => 'message',
        );
        $this->validate($data,$rules,[],$niceNames);

        $messagrie = new Messagrie;
        $messagrie->nom = $data['nom'];
        $messagrie->prenom = $data['prenom'];
        $messagrie->email = $data['email'];
        $messagrie->tel = $data['tel'];
        $messagrie->message = $data['message'];
        $messagrie->save();

        \Session::flash('success', 'Votre message a ete envoyer avec succès');

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Messagrie  $messagrie
     * @return \Illuminate\Http\Response
     */
    public function show(Messagrie $messagrie)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Messagrie  $messagrie
     * @return \Illuminate\Http\Response
     */
    public function edit(Messagrie $messagrie)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Messagrie  $messagrie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Messagrie $messagrie)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Messagrie  $messagrie
     * @return \Illuminate\Http\Response
     */
    public function destroy(Messagrie $messagrie)
    {
        //
    }
}

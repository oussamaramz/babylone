<?php

namespace App\Http\Controllers;

use App\Etudiant;
use Illuminate\Http\Request;
use App\User;
use App\Contact;
use App\Messagrie;
use App\Gallerie;
use App\Album;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:users');
    }

    public function profile(){
        $navkey = 'profile';
        return view('back.admin.profile',compact('navkey'));
    }

    public function profileUpdate(Request $data){
        $user = User::where('id',1)->first();
        $user->email= $data->email;
        $user->password=Hash::make($data->password);
        $user->save();
        \Session::flash('success', 'Votre profile modifer avec success');
        return redirect()->back();
    }


    public function dash(){
        $EtudiantChart = array(Etudiant::whereMonth('created_at','=',date('1'))->get()->count(),
            Etudiant::whereMonth('created_at','=',date('2'))->get()->count(),
            Etudiant::whereMonth('created_at','=',date('3'))->get()->count(),
            Etudiant::whereMonth('created_at','=',date('4'))->get()->count(),
            Etudiant::whereMonth('created_at','=',date('5'))->get()->count(),
            Etudiant::whereMonth('created_at','=',date('6'))->get()->count(),
            Etudiant::whereMonth('created_at','=',date('7'))->get()->count(),
            Etudiant::whereMonth('created_at','=',date('8'))->get()->count(),
            Etudiant::whereMonth('created_at','=',date('9'))->get()->count(),
            Etudiant::whereMonth('created_at','=',date('10'))->get()->count(),
            Etudiant::whereMonth('created_at','=',date('11'))->get()->count(),
            Etudiant::whereMonth('created_at','=',date('12'))->get()->count());
        $EtudiantChart =json_encode($EtudiantChart);

        $MessagrieCharts = array(Messagrie::whereMonth('created_at','=',date('1'))->get()->count(),
            Messagrie::whereMonth('created_at','=',date('2'))->get()->count(),
            Messagrie::whereMonth('created_at','=',date('3'))->get()->count(),
            Messagrie::whereMonth('created_at','=',date('4'))->get()->count(),
            Messagrie::whereMonth('created_at','=',date('5'))->get()->count(),
            Messagrie::whereMonth('created_at','=',date('6'))->get()->count(),
            Messagrie::whereMonth('created_at','=',date('7'))->get()->count(),
            Messagrie::whereMonth('created_at','=',date('8'))->get()->count(),
            Messagrie::whereMonth('created_at','=',date('9'))->get()->count(),
            Messagrie::whereMonth('created_at','=',date('10'))->get()->count(),
            Messagrie::whereMonth('created_at','=',date('11'))->get()->count(),
            Messagrie::whereMonth('created_at','=',date('12'))->get()->count());
        $MessagrieCharts =json_encode($MessagrieCharts);
        $navkey = 'dash';
        return view('back.admin.dash',compact('navkey','EtudiantChart','MessagrieCharts'));
    }


    public function subscribers(){
        $subscribers = Etudiant::get()->sortByDesc('id');
        $navkey = 'subscribers';
        return view('back.admin.etudiants.subscribers',compact('navkey','subscribers'));
    }

    public function etudiants(){
        $etudiants = Etudiant::where('active','!=',null)->get();
        $navkey = 'etudiants';
        return view('back.admin.etudiants.list',compact('navkey','etudiants'));
    }

    public function EtudiantDetails($id){
        $etudiant = Etudiant::where('id',$id)->first();
        $etudiant->enabled = now();
        $etudiant->save();
        $navkey = 'subscribers';
        return view('back.admin.etudiants.details',compact('navkey','etudiant'));
    }

    public function EtudiantEdit($id){
        $etudiant = Etudiant::where('id',$id)->first();
        $navkey = 'etudiants';
        return view('back.admin.etudiants.edit',compact('navkey','etudiant'));
    }

    public function EtudiantUpdate($id,Request $data){
        $rules = [
            'nomCompletEtudiant' => ['required', 'string', 'max:255'],
            'nomCompletTuteur' => ['required', 'string', 'max:255'],
            'niveauActuel' => ['required'],
            'niveauDemande' => ['required'],
            'tel' => ['required', 'string', 'max:15'],
            'dateNaissance' => ['required', 'date'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'gender' => ['required'],
        ];

        $niceNames = array(
            'nomCompletEtudiant' => 'non et prenom d\'enfant',
            'nomCompletTuteur' => 'nom et prenom de tuteur',
            'niveauActuel' => 'niveau actuel',
            'niveauDemande' => 'niveau demande',
            'tel' => 'telephone',
            'dateNaissance' => 'date de naissance',
            'email' => 'email',
            'gender' => 'genre',
        );
        $this->validate($data,$rules,[],$niceNames);

        $etudiant = Etudiant::where('id',$id)->first();
        $etudiant->nomCompletEtudiant = $data['nomCompletEtudiant'];
        $etudiant->nomCompletTuteur = $data['nomCompletTuteur'];
        $etudiant->niveauActuel = $data['niveauActuel'];
        $etudiant->niveauDemande = $data['niveauDemande'];
        $etudiant->email = $data['email'];
        $etudiant->tel = $data['tel'];
        $etudiant->dateNaissance = $data['dateNaissance'];
        $etudiant->gender = $data['gender'];
        if ($data->hasFile('image'))
            {
            $file = $data->file('image');
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = $timestamp. '-' .$file->getClientOriginalName();

            $file->move(public_path().'/back/images/etudiant/', $name);
            $etudiant->image = $name;

        }
        $etudiant->update();

        \Session::flash('success', 'Etudiant modifer avec success');

        return redirect(route('admin.EtudiantDetails',['id'=>$etudiant->id]));
    }

    public function EtudiantEnabled($id){
        $etudiant = Etudiant::where('id',$id)->first();

        $etudiant->active = 1;
        $etudiant->update();
        return redirect()->back();
    }

    public function EtudiantDelete($id){
        $etudiant = Etudiant::where('id',$id)->first();
        $etudiant->delete();

        return 'done';
    }


    // contact
    public function contactConfig(){
        $contacts = Contact::get();
        $navkey = 'contact';
        return view('back.admin.contact',compact('navkey','contacts'));
    }
    public function contactUpdate(Request $data){
        $email = Contact::where('id',1)->first();
        $email->value=$data->email;
        $email->save();

        $adress = Contact::where('id',2)->first();
        $adress->value=$data->adresse;
        $adress->save();

        $tel = Contact::where('id',3)->first();
        $tel->value=$data->tel;
        $tel->save();

        $facebook = Contact::where('id',4)->first();
        $facebook->value=$data->facebook;
        $facebook->save();

        $instagram = Contact::where('id',5)->first();
        $instagram->value=$data->instagram;
        $instagram->save();

        $youtube = Contact::where('id',6)->first();
        $youtube->value=$data->youtube;
        $youtube->save();

        $adressENG = Contact::where('id',7)->first();
        $adressENG->value=$data->adresseENG;
        $adressENG->save();

        return redirect()->back();
    }

    public function messagrieList(){
        $messagries = Messagrie::get()->sortByDesc('id');
        $navkey = 'messagrie';
        return view('back.admin.messagrie',compact('navkey','messagries'));
    }

    public function messagedetails($id){
        $messagrie = Messagrie::where('id',$id)->first();
        $messagrie->lu = 1;
        $messagrie->save();
        $navkey = 'messagrie';
        return view('back.admin.messagedetails',compact('navkey','messagrie'));
    }

    public function deleteMessage($id){
        $messagrie = Messagrie::where('id',$id)->first();
        $messagrie->delete();
        return redirect(route('admin.messagrieList'));
    }



    // gallerie function
    public function gallerieList(){
        $gallereis = Gallerie::get();
        $albums = Album::get();
        $navkey = 'gallerie';
        return view('back.admin.gallerie.list',compact('navkey','gallereis','albums'));
    }

    public function gallerieAdd(){
        $albums = Album::get();
        $navkey = 'gallerie';
        return view('back.admin.gallerie.add',compact('navkey','albums'));
    }

    public function gallerieStore(Request $data){
        if ($images=$data->file('images'))
        {
            foreach ($images as $key => $item) {
                $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $name = $timestamp. '-gallery-' .$item->getClientOriginalName();

                $item->move(public_path().'/back/images/gallery/', $name);
                $gallery = new Gallerie;
                $gallery->img=$name;
                $gallery->album=$data->album;
                $gallery->save();
            }
        }

        return redirect()->route('admin.gallerieList');
    }

    // album routes

    public function albumList(){
        $albums = Album::get();
        $navkey = 'album';
        return view('back.admin.album.list',compact('navkey','albums'));
    }
    public function albumStore(Request $data){
        $albums = new Album;
        $albums->label = $data->label;
        $albums->save();

        $albumsNew = Album::where('id',$albums->id)->first();
        $alias = $albumsNew->id.'_'.$data->label;
        $albums->alias = Album::getAlias($alias);
        $albums->save();
        return redirect()->back();
    }
    public function albumUpdate(){

    }
    public function albumDelete($id){
        $albums = Album::where('id',$id)->first();
        $albums->delete();

        \Session::flash('error', 'Album supprimer avec success');
        return redirect()->back();
    }

    public function galleryDelete($id){
        $gallery = Gallerie::where('id',$id)->first();
        $gallery->delete();

        return redirect()->back();
    }

}

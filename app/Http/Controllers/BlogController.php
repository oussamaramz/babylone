<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;
use Carbon\Carbon;

class BlogController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:users');
    }

    public function index()
    {
        $blogs = Blog::where('enebled',1)->get();
        $navkey = 'blogs';
        return view('back.admin.blogs.list',compact('navkey','blogs'));
    }


    public function BlogDetails($id)
    {
        $blog = Blog::where('id',$id)->first();
        $navkey = 'blogs';
        return view('back.admin.blogs.item',compact('navkey','blog'));
    }

    public function blogAdd()
    {
        $navkey = 'blogs';
        return view('back.admin.blogs.add',compact('navkey'));
    }

    public function blogStore(Request $data)
    {
        if (isset($data->idblog)) {
            $rules = [
                'labelFr' => ['required', 'string', 'max:255'],
                'introFr' => ['required', 'string', 'max:255'],
                'detailsFr' => ['required','string'],
            ];

            $niceNames = array(
                'labelFr' => 'label francais',
                'introFr' => 'intro francais',
                'detailsFr' => 'details francais',
            );
        }else{
            $rules = [
                'labelFr' => ['required', 'string', 'max:255'],
                'introFr' => ['required', 'string', 'max:255'],
                'detailsFr' => ['required','string'],
                'image' => ['required','image'],
                'banner' => ['required', 'image'],
            ];

            $niceNames = array(
                'labelFr' => 'label francais',
                'introFr' => 'intro francais',
                'detailsFr' => 'details francais',
                'image' => 'image',
                'banner' => 'banner',
            );
        }
        $this->validate($data,$rules,[],$niceNames);

        if (isset($data->idblog)) {
            $blog = Blog::where('id',$data->idblog)->first();
        }else{
            $blog = new Blog;
        }


        $blog->labelFr = $data['labelFr'];
        $blog->introFr = $data['introFr'];
        $blog->detailsFr = $data['detailsFr'];
        $blog->keyWordsFR = $data['KeywordsFR'];
        $blog->metaTitleFR = $data['metaTitleFR'];
        $blog->metaDescriptionFR = $data['metaDscrptFR'];

        $blog->labelENG = $data['labelENG'];
        $blog->introENG = $data['introENG'];
        $blog->detailsENG = $data['detailsENG'];
        $blog->keyWordsENG = $data['KeywordsENG'];
        $blog->metaTitleENG = $data['metaTitleENG'];
        $blog->metaDescriptionENG = $data['metaDscrptENG'];

        $blog->enebled = 1;

        if ($data->hasFile('image'))
            {
            $file = $data->file('image');
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = $timestamp. '-Image-' .$file->getClientOriginalName();

            $file->move(public_path().'/back/images/blogs/', $name);
            $blog->image = $name;

        }

        if ($data->hasFile('banner'))
            {
            $file = $data->file('banner');
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = $timestamp. '-Banner-' .$file->getClientOriginalName();

            $file->move(public_path().'/back/images/blogs/', $name);
            $blog->banner = $name;

        }

        if ($data->hasFile('ogImage'))
            {
            $file = $data->file('ogImage');
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = $timestamp. '-ogImage-' .$file->getClientOriginalName();

            $file->move(public_path().'/back/images/blogs/', $name);
            $blog->imageOG = $name;

        }
        $blog->save();

        $blogNew = Blog::where('id',$blog->id)->first();
        $aliasFR = $blogNew->id.'_'.$data['labelFr'];
        $blogNew->aliasFr = Blog::getAlias($aliasFR);
        $aliasENG = $blogNew->id.'_'.$data['labelENG'];
        $blogNew->aliasENG = Blog::getAlias($aliasENG);
        $blogNew->save();

        \Session::flash('success', 'blog ajouté avec success');

        return redirect(route('admin.BlogDetails',['id'=>$blog->id]));
    }

    public function blogActive($id)
    {
        $blog = Blog::where('id',$id)->first();
        if ($blog->enebled == 1) {
            $blog->enebled = 0;
            $blog->save();
        }else{
            $blog->enebled = 1;
            $blog->save();
        }

        return redirect()->back();
    }




    public function blogEdit($id)
    {
        $blog = Blog::where('id',$id)->first();
        $navkey = 'blogs';
        return view('back.admin.blogs.edit',compact('navkey','blog'));
    }


    public function update(Request $request, Blog $blog)
    {
        //
    }


    public function blogDelete($id)
    {
        $blog = Blog::where('id',$id)->first();
        $blog->delete();
        \Session::flash('success', 'blog supprimé avec success');
        return redirect(route('admin.blogs'));
    }
}

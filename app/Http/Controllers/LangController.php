<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\URL;

class LangController extends Controller
{
    public function languageSwitch($local)
    {
        App::setlocale($local);
        session()->put('locale', $local);

        Artisan::call('cache:clear');
       Artisan::call('config:clear');
       Artisan::call('config:cache');
       Artisan::call('view:clear');
       Artisan::call('storage:link');

    // return redirect(route('babylone'));
    // return redirect()->back();
    $url   = URL::previous();
    
  $url_explode = explode("//",$url);
  $url_explode2 = explode("/",$url_explode[1]);
  $url_explode2[2] = $local;
  $redir = implode('/',$url_explode2);
  $url_explode[1]=$redir;
  $redir2 = implode('//',$url_explode);
  return redirect($redir2);
    }
}

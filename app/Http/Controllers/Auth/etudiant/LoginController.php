<?php

namespace App\Http\Controllers\Auth\etudiant;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
class LoginController extends Controller
{

    protected $redirectTo = 'etudiant/dash';
    protected $guard = 'etudiants';


    public function __construct()
    {
        $this->middleware('guest:etudiants', ['except' => ['logout'] ]);
    }



    public function login(Request $request)
    {
        //attempt to log ther user in
        if (Auth::guard('etudiants')->attempt(['email' => $request->username, 'password' => $request->password], $request->remember)) {
           return redirect(route('etudiant.dash'));
        }


        $errors = new MessageBag(['password' => ['Email and/or password invalid.']]);
        // if unsuccessful, then redirect back to login with the form date
        return redirect()->back()->withErrors($errors)->withInput($request->only('email', 'remember'));
    }


    public function logout(Request $request)
    {
        Auth::guard('etudiants')->logout();
        return redirect(url("/"));
    }

}

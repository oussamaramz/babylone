<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    // use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'admin/dash';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:users', ['except' => ['logout'] ]);
    }



    public function login(Request $request)
    {
        //attempt to log ther user in
        if (Auth::guard('users')->attempt(['email' => $request->username, 'password' => $request->password], $request->remember)) {
            return redirect(route('admin.dash'));
        }


        $errors = new MessageBag(['password' => ['Email and/or password invalid.']]);
        // if unsuccessful, then redirect back to login with the form date
        return redirect()->back()->withErrors($errors)->withInput($request->only('email', 'remember'));
    }


    public function logout(Request $request)
    {
        Auth::guard('users')->logout();
        return redirect(route("babylone"));
    }

}

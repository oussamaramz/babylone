<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use App\Notes;
use App\Etudiant;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Carbon\Carbon;

use Session;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(Request $data)
    {
        
        $rules = [
            'nomCompletEtudiant' => ['required', 'string', 'max:255'],
            'nomCompletTuteur' => ['required', 'string', 'max:255'],
            'niveauActuel' => ['required'],
            'niveauDemande' => ['required'],
            'message' => ['required', 'string'],
            'tel' => ['required', 'string', 'max:15'],
            'dateNaissance' => ['required', 'date'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:8'],
            'sexe' => ['required'],
            'Etablissement_actuel' => ['required','string'],
        ];

        $niceNames = array(
            'nomCompletEtudiant' => 'non et prenom d\'enfant',
            'nomCompletTuteur' => 'nom et prenom de tuteur',
            'niveauActuel' => 'niveau actuel',
            'niveauDemande' => 'niveau demande',
            'message' => 'message',
            'tel' => 'telephone',
            'dateNaissance' => 'date de naissance',
            'email' => 'email',
            'password' => 'mot de pass',
            'sexe' => 'Sexe',
            'Etablissement_actuel' => 'Etablissement actuel',
        );
        $this->validate($data,$rules,[],$niceNames);

        $record = Etudiant::where('email',$data['email'])->first();

        if ($record != null) {
            \Session::flash('error', 'Cette Email deja exixst');
            return redirect()->back();
        }

        $name = null;
        if ($data->hasFile('photo'))
            {
            $file = $data->file('photo');
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
            $name = $timestamp. '-photo-' .$file->getClientOriginalName();

            $data->file('photo')->move(public_path().'/back/images/etudiant/', $name);

            // $file->move(public_path().'/back/images/etudiant/', $name);

        }


        $etudiant = Etudiant::create([
            'nomCompletEtudiant' => $data['nomCompletEtudiant'],
            'nomCompletTuteur' => $data['nomCompletTuteur'],
            'niveauActuel' => $data['niveauActuel'],
            'niveauDemande' => $data['niveauDemande'],
            'messageInscription' => $data['message'],
            'role' => 2,
            'email' => $data['email'],
            'tel' => $data['tel'],
            'gender' => $data['sexe'],
            'image' => $name,
            'Etablissement_actuel' => $data['Etablissement_actuel'],
            'dateNaissance' => $data['dateNaissance'],
            'password' => Hash::make($data['password']),
            'remember_token' => Str::random(10),
        ]);

        $note = null;
        $notes = $data->file('notes');

        if($data->hasFile('notes'))
        {
            foreach ($notes as $file) {
                $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
                $note = $timestamp. '-note-' .$file->getClientOriginalName();

                $file->move(public_path().'/back/images/etudiant/', $note);

                // $file->move(public_path().'/back/images/etudiant/', $note);

                $note = Notes::create([
                    'etudiant' => $etudiant->id,
                    'noteFile' => $note,
                ]);
            }
        }

        \Session::flash('success', 'Votre registre est réussi');
        $navkey  = 's_identifier';

        return view('front.s_identifier',compact('navkey'));
    }


    public function redirectTo()
{
    return app()->getLocale() . '/home';
}
}

<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\App;
use Config;
use Closure;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (session()->has('locale')) {
            App::setlocale(session()->get('locale'));
        }else{
            session(['locale' => Config::get('app.locale')]);
        }
        return $next($request);
    }
}

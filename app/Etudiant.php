<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Etudiant extends Authenticatable
{
    use Notifiable;
    protected $guard = 'etudiants';


    protected $fillable = [
        'nomCompletEtudiant','nomCompletTuteur','niveauActuel','niveauDemande','messageInscription','tel','password','image','dateNaissance','gender','role','email','enabled','active','Etablissement_actuel'
    ];

}

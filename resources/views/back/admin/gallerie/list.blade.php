@extends('back.admin.layout.layout')
@section('style')
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/vendors/magnific-popup/magnific-popup.css')}}">

<style>
    .deleteBTN{
        position: absolute;
        margin-top: 15px;
        margin-left: -35px;
    }
</style>
@endsection
@section('content')

    <div class="row">
        <div id="breadcrumbs-wrapper" data-image="{{asset('assets/back/images/gallery/breadcrumb-bg.jpg')}}" class="breadcrumbs-bg-image" style="background-image: url(&quot;{{asset('assets/back/images/gallery/breadcrumb-bg.jpg')}}&quot;);">
            <!-- Search for small screen-->
            <div class="container">
                <div class="row">
                    <div class="col s12 m6 l6">
                        <h5 class="breadcrumbs-title mt-0 mb-0"><span>Galleries List</span></h5>
                    </div>
                    <div class="col s12 m6 16 right-align-md">
                        <a href="{{route('admin.gallerieAdd')}}" class="waves-effect pink accent-2 waves-light btn mb-1 mr-1">
                                <i class="material-icons left">add</i> Ajouter</a>
                        </div>
                </div>
            </div>
        </div>
        <div class="col s12">
            <div class="container">
                <div class="card">
                    <div class="card-content">
                        <ul class="nav nav-pills gallery-filters">
                            {{-- <li role="presentation" class="active" gallery-filter="all"><a href="#">{{__('msg.All')}}</a></li> --}}
                            <a class="waves-effect pink accent-2 waves-light btn mb-1 mr-1 active" gallery-filter="all">All</a>
                            @foreach ($albums as $item)
                            <a class="waves-effect pink accent-2 waves-light btn mb-1 mr-1 active" gallery-filter="{{$item->id}}">{{$item->label}}</a>
                            {{-- <li role="presentation" gallery-filter="{{$item->id}}"><a href="#">{{$item->label}}</a></li> --}}
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="section">
                    {{-- start --}}
                    <div class="section">
                        {{-- <div class="card">
                            <div class="card-content">
                                <p class="caption">Masonry with Magnific Popup is a responsive lightbox &amp; dialog script with focus on
                                    performance and providing best experience for user with any device</p>
                            </div>
                        </div> --}}
                        <div class="masonry-gallery-wrapper">
                            <div class="popup-gallery">
                                <div class="gallery-sizer"></div>
                                <div class="row">
                                    @foreach ($gallereis as $item)
                                    <div class="col s12 m6 l4 xl2 gallery-img-holder" filter-category="{{$item->album}}">
                                        <div>
                                            <a href="{{asset('back/images/gallery/'.$item->img)}}" title="The Cleaner">
                                                <img src="{{asset('back/images/gallery/'.$item->img)}}" class="responsive-img mb-10" alt="">
                                            </a>

                                        <a href="{{route('admin.galleryDelete',['id'=>$item->id])}}" class="btn-floating mb-1 btn-large waves-effect waves-light mr-1 red accent-2 deleteBTN">
                                            <i class="material-icons">delete</i>
                                        </a>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- end --}}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script src="{{asset('assets/back//vendors/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('assets/back//vendors/imagesloaded.pkgd.min.js')}}"></script>
<script src="{{asset('assets/back/js/scripts/media-gallery-page.js')}}"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

                <script>
                    $(document).ready(function(){
						$("a.deleteBTN").on("click",function(event){
						event.stopPropagation();
						Swal.fire({
							title: 'Êtes-vous sûr?',
							text: "Vous ne pourrez pas revenir en arrière!",
							icon: 'warning',
							showCancelButton: true,
							confirmButtonColor: '#3085d6',
							cancelButtonColor: '#d33',
							confirmButtonText: 'Oui, supprimez-le!',
							cancelButtonText: 'Annuler'
							}).then((result) => {
							if (result.isConfirmed) {
								window.location.href = this.getAttribute('href');
								Swal.fire(
								'Deleted!',
								'Your file has been deleted.',
								'success'
								)
							}
							})
						event.preventDefault();

						});
					});
                </script>

<script>
    jQuery('document').ready( function($) {
    $('.gallery-filters a').on('click', function() {
        $('.gallery-filters a').removeClass('active');
        $(this).addClass('active');
        filter = $(this).attr('gallery-filter');

        $('.gallery-img-holder').each( function() {
            if (filter == 'all') {
                $(this).fadeIn();
            } else {
                $(this).hide();
                if ($(this).attr('filter-category') == filter) {
                    $(this).fadeIn();
                }
            }
        });
    });
});
</script>
@endsection

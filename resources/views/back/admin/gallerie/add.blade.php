@extends('back.admin.layout.layout')
@section('style')

@endsection
@section('content')

    <div class="row">
        <div id="breadcrumbs-wrapper" data-image="{{asset('assets/back/images/gallery/breadcrumb-bg.jpg')}}" class="breadcrumbs-bg-image" style="background-image: url(&quot;{{asset('assets/back/images/gallery/breadcrumb-bg.jpg')}}&quot;);">
            <!-- Search for small screen-->
            <div class="container">
                <div class="row">
                    <div class="col s12 m6 l6">
                        <h5 class="breadcrumbs-title mt-0 mb-0"><span>Ajouter Galleries</span></h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s12">
            <div class="container">
                <div class="section">
                    {{-- start --}}
                    <form method="POST" action="{{route('admin.gallerieStore')}}" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                        <div class="row">
                            <div class="select-wrapper col s6">
                                <select tabindex="-1" name="album">
                                <option value="" disabled="" selected="">Select album</option>
                                @foreach ($albums as $item)
                                <option value="{{$item->id}}">{{$item->label}}</option>
                                @endforeach
                            </select>
                            </div>
                            <div class="file-field input-field col s6">
                                <div class="btn">
                                    <span>Image*</span>
                                    <input type="file" name="images[]" required oninvalid="this.setCustomValidity('Image est obligatoire')" multiple>
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="row">
                                <div class="input-field col s12">
                                    <button class="btn cyan waves-effect waves-light right" type="submit"
                                        name="action">Enregistrer
                                        <i class="material-icons right">send</i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>

                    {{-- end --}}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
@endsection

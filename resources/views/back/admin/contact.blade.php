@extends('back.admin.layout.layout')
@section('style')
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/vendors/flag-icon/css/flag-icon.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/back/fonts/fontawesome/css/all.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/css/pages/page-contact.css')}}">
@endsection
@section('content')

    <div class="row">
        <div class="col s12">
            <div class="container">
                <div class="section">
                    {{-- start --}}

                    <div id="contact-us" class="section">
                        <div class="app-wrapper">
                            <div class="contact-header">
                                <div class="row contact-us ml-0 mr-0">
                                    <div class="col s12 m12 l4 sidebar-title">
                                        <h5 class="m-0"><i class="material-icons contact-icon vertical-text-top">mail_outline</i> Informations de contact</h5>
                                        <span class="social-icons hide-on-med-and-down">
                                            <a href="{{$contacts[5]->value}}"><i class="fab fa-youtube ml-5"></i></a>
                                            <a href="{{$contacts[3]->value}}"><i class="fab fa-facebook-f ml-5"></i></a>
                                            <a href="{{$contacts[4]->value}}"><i class="fab fa-instagram ml-5"></i></a>
                                        </span>
                                    </div>
                                    <div class="col s12 m12 l8 form-header">
                                        <h6 class="form-header-text">
                                            <i class="material-icons"> mail_outline </i>
                                            Modifier Les Information de Contact
                                        </h6>
                                    </div>
                                </div>
                            </div>

                            <!-- Contact Sidenav -->
                            <div id="sidebar-list" class="row contact-sidenav ml-0 mr-0">
                                <div class="col s12 m12 l4">
                                    <!-- Sidebar Area Starts -->
                                    <div class="sidebar-left sidebar-fixed">
                                        <div class="sidebar">
                                            <div class="sidebar-content">
                                                <div class="sidebar-menu list-group position-relative">
                                                    <div class="sidebar-list-padding app-sidebar contact-app-sidebar" id="contact-sidenav">
                                                        {{-- <ul class="contact-list display-grid">
                                                            <li>
                                                                <h5 class="m-0">What will be next step?</h5>
                                                            </li>
                                                            <li>
                                                                <h6 class="mt-5 line-height">You are one step closer to build your perfect product</h6>
                                                            </li>
                                                            <li>
                                                                <hr class="mt-5">
                                                            </li>
                                                        </ul> --}}
                                                        <div class="row">
                                                            <!-- Place -->
                                                            <div class="col s12 place mt-4 p-0">
                                                                <div class="col s2 m2 l2"><i class="material-icons"> place </i></div>
                                                                <div class="col s10 m10 l10">
                                                                    <p class="m-0">{{$contacts[1]->value}}</p>
                                                                </div>
                                                            </div>
                                                            <!-- Phone -->
                                                            <div class="col s12 phone mt-4 p-0">
                                                                <div class="col s2 m2 l2"><i class="material-icons"> call </i></div>
                                                                <div class="col s10 m10 l10">
                                                                    <p class="m-0">{{$contacts[2]->value}}</p>
                                                                </div>
                                                            </div>
                                                            <!-- Mail -->
                                                            <div class="col s12 mail mt-4 p-0">
                                                                <div class="col s2 m2 l2"><i class="material-icons"> mail_outline </i></div>
                                                                <div class="col s10 m10 l10">
                                                                    <p class="m-0">{{$contacts[0]->value}}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <a href="#" data-target="contact-sidenav" class="sidenav-trigger"><i class="material-icons">menu</i></a>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- Sidebar Area Ends -->
                                </div>
                                <div class="col s12 m12 l8 contact-form margin-top-contact">
                                    <div class="row">
                                    <form class="col s12" method="post" action="{{route('admin.contactUpdate')}}">
                                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                            <div class="row">
                                                <div class="input-field col m6 s12">
                                                    <input id="email" type="text" class="validate" name="email" value="{{$contacts[0]->value}}">
                                                    <label for="email">Email</label>
                                                </div>
                                                <div class="input-field col m6 s12">
                                                    <input id="budget" type="text" class="validate" name="tel" value="{{$contacts[2]->value}}">
                                                    <label for="budget">Telephone</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col m12 s12">
                                                    <input id="name" type="text" class="validate" name="adresse" value="{{$contacts[1]->value}}">
                                                        <label for="name">Adresse</label>
                                                </div>
                                                <div class="input-field col m12 s12">
                                                    <input id="company" type="text" class="validate" name="adresseENG" value="{{$contacts[6]->value}}">
                                                    <label for="company">Adresse Englais</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col m12 s12">
                                                    <input id="name" type="text" class="validate" name="facebook" value="{{$contacts[3]->value}}">
                                                        <label for="name">Facbook</label>
                                                </div>
                                                <div class="input-field col m12 s12">
                                                    <input id="company" type="text" class="validate" name="instagram" value="{{$contacts[4]->value}}">
                                                    <label for="company">Instagram</label>
                                                </div>
                                                <div class="input-field col m12 s12">
                                                    <input id="company" type="text" class="validate" name="youtube" value="{{$contacts[5]->value}}">
                                                    <label for="company">Youtube</label>
                                                </div>
                                            </div>
                                            <button type="submit" class="waves-effect waves-light btn">Enregister</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- end --}}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')

@endsection

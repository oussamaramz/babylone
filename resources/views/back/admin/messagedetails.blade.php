@extends('back.admin.layout.layout')
@section('style')
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/css/pages/app-sidebar.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/css/pages/app-email.css')}}">
@endsection
@section('content')

    <div class="row">
        <div id="breadcrumbs-wrapper" data-image="{{asset('assets/back/images/gallery/breadcrumb-bg.jpg')}}" class="breadcrumbs-bg-image" style="background-image: url(&quot;{{asset('assets/back/images/gallery/breadcrumb-bg.jpg')}}&quot;);">
            <!-- Search for small screen-->
            <div class="container">
                <div class="row">
                    <div class="col s12 m6 l6">
                        <h5 class="breadcrumbs-title mt-0 mb-0"><span>Message Deails</span></h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s12">
            <div class="container">
                <div class="section">
                    {{-- start --}}
                    <!-- Content Area Starts -->
                    <div class="card-content pt-0">
                        <div class="row">
                            <div class="col s12">

                                <!-- Email Content -->
                                <div class="email-content">
                                    <div class="list-title-area">
                                        <div class="user-media">
                                            <div class="list-title">
                                                <span class="name">{{$messagrie->nom.' '.$messagrie->prenom}}</span>
                                            </div>
                                        </div>
                                        <div class="title-right">
                                            <span class="mail-time">{{$messagrie->created_at}}</span>
                                        </div>
                                    </div>
                                    <div class="email-desc">
                                    <p>{{$messagrie->message}}</p>
                                    </div>
                                </div>
                                <!-- Email Content Ends -->
                                <hr>
                                <!-- Email Footer -->
                                <div class="email-footer">
                                    <div class="footer-action">
                                        <div class="footer-buttons">
                                        <a href="{{route('admin.deleteMessage',['id'=>$messagrie->id])}}" class="btn reply mb-1 red delete"><i class="material-icons left">close</i><span>Supprimer</span></a>
                                        </div>
                                    </div>
                                </div>
                                <!-- Email Footer Ends -->
                            </div>
                        </div>
                    </div>
                    <!-- Content Area Ends -->
                    {{-- end --}}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

                <script>
                    $(document).ready(function(){
						$("a.delete").on("click",function(event){
						event.stopPropagation();
						Swal.fire({
							title: 'Êtes-vous sûr?',
							text: "Vous ne pourrez pas revenir en arrière!",
							icon: 'warning',
							showCancelButton: true,
							confirmButtonColor: '#3085d6',
							cancelButtonColor: '#d33',
							confirmButtonText: 'Oui, supprimez-le!',
							cancelButtonText: 'Annuler'
							}).then((result) => {
							if (result.isConfirmed) {
								window.location.href = this.getAttribute('href');
								Swal.fire(
								'Deleted!',
								'Your file has been deleted.',
								'success'
								)
							}
							})
						event.preventDefault();

						});
					});
                </script>
@endsection

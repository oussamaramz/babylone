@extends('back.admin.layout.layout')
@section('style')

@endsection
@section('content')

    <div class="row">
<div id="breadcrumbs-wrapper" data-image="{{asset('assets/back/images/gallery/breadcrumb-bg.jpg')}}" class="breadcrumbs-bg-image" style="background-image: url(&quot;{{asset('assets/back/images/gallery/breadcrumb-bg.jpg')}}&quot;);">
            <!-- Search for small screen-->
            <div class="container">
                <div class="row">
                    <div class="col s12 m6 l6">
                        <h5 class="breadcrumbs-title mt-0 mb-0"><span>Profile</span></h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s12">
            <div class="container">
                <div class="section">
                    {{-- start --}}
                    @include('back.alert')
                    <div class="col s12">
                        <div class="container">
                            <div class="section">
                                {{-- start --}}
                                <form method="POST" action="{{route('admin.profileUpdate')}}" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                    <div class="row">
                                        <div class="input-field col s12">
                                        <input type="text" id="email" name="email" value="{{auth()->user()->email}}">
                                            <label for="fn">Email</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <input type="password" id="password" name="password">
                                            <label for="fn">Mot de pass</label>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <button class="btn cyan waves-effect waves-light right" type="submit"
                                                    name="action">Modifier
                                                    <i class="material-icons right">send</i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                                {{-- end --}}
                            </div>
                        </div>
                    </div>

                    {{-- end --}}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')

@endsection

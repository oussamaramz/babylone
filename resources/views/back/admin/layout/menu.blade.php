<aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-dark sidenav-active-rounded">
        <div class="brand-sidebar">
            <h1 class="logo-wrapper"><a class="brand-logo darken-1" href="index.html"><img class="hide-on-med-and-down " src="{{ asset('assets/front/images/logo-white.png')}}" alt="babylone" /><img class="show-on-medium-and-down hide-on-med-and-up" src="{{ asset('assets/front/images/logo-white.png')}}" alt="babylone" /><span class="logo-text hide-on-med-and-down">Babylone</span></a><a class="navbar-toggler" href="#"><i class="material-icons">radio_button_checked</i></a></h1>
        </div>
        <ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow" id="slide-out" data-menu="menu-navigation" data-collapsible="accordion">






            <li class="{{ $navkey == 'dash' ? 'active' : ''}} bold">
                <a class="waves-effect waves-cyan {{ $navkey == 'dash' ? 'active' : ''}}" href="{{route('admin.dash')}}">
                    <i class="material-icons">settings_input_svideo</i>
                    <span class="menu-title" data-i18n="Mail">Tableau de Bord</span>
                </a>
            </li>
            <li class="{{ $navkey == 'subscribers' ? 'active' : ''}} bold">
                <a class="waves-effect waves-cyan {{ $navkey == 'subscribers' ? 'active' : ''}}" href="{{route('admin.subscribers')}}">
                    <i class="material-icons">people</i>
                    <span class="menu-title" data-i18n="Mail">Inscriptions</span></a>
            </li>
            {{-- <li class="{{ $navkey == 'etudiants' ? 'active' : ''}} bold">
                <a class="waves-effect waves-cyan {{ $navkey == 'etudiants' ? 'active' : ''}}" href="{{route('admin.etudiants')}}">
                    <i class="material-icons">people</i>
                    <span class="menu-title" data-i18n="Mail">Etudiants</span>
                    <span class="badge badge pill pink accent-2 float-right mr-2">{{\App\Etudiant::where('active','!=',null)->get()->count()}}</span>
                </a>
            </li> --}}
            <li class="{{ $navkey == 'blogs' ? 'active' : ''}} bold">
                <a class="waves-effect waves-cyan {{ $navkey == 'blogs' ? 'active' : ''}}" href="{{route('admin.blogs')}}">
                    <i class="material-icons">collections_bookmark</i>
                    <span class="menu-title" data-i18n="Mail">Articles</span>
                </a>
            </li>
            <li class="{{ $navkey == 'messagrie' ? 'active' : ''}} bold">
                <a class="waves-effect waves-cyan {{ $navkey == 'messagrie' ? 'active' : ''}}" href="{{route('admin.messagrieList')}}">
                    <i class="material-icons">email</i>
                    <span class="menu-title" data-i18n="Mail">Messagrie</span>
                </a>
            </li>
            <li class="{{ $navkey == 'gallerie' ? 'active' : ''}} bold">
                <a class="waves-effect waves-cyan {{ $navkey == 'gallerie' ? 'active' : ''}}" href="{{route('admin.gallerieList')}}">
                    <i class="material-icons">insert_photo</i>
                    <span class="menu-title" data-i18n="Mail">Gallerie</span>
                </a>
            </li>
            <li class="{{ $navkey == 'album' ? 'active' : ''}} bold">
                <a class="waves-effect waves-cyan {{ $navkey == 'album' ? 'active' : ''}}" href="{{route('admin.albumList')}}">
                    <i class="material-icons">insert_photo</i>
                    <span class="menu-title" data-i18n="Mail">Album</span>
                </a>
            </li>
            <li class="{{ $navkey == 'contact' ? 'active' : ''}} bold">
                <a class="waves-effect waves-cyan {{ $navkey == 'contact' ? 'active' : ''}}" href="{{route('admin.contactConfig')}}">
                    <i class="material-icons">contacts</i>
                    <span class="menu-title" data-i18n="Mail">Contact Config</span>
                </a>
            </li>

        </ul>
        <div class="navigation-background"></div><a class="sidenav-trigger btn-sidenav-toggle btn-floating btn-medium waves-effect waves-light hide-on-large-only" href="#" data-target="slide-out"><i class="material-icons">menu</i></a>
    </aside>

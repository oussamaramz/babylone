<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    @include('back.admin.layout.head')
    @yield('style')
</head>
<!-- END: Head-->

<body class="vertical-layout page-header-light vertical-menu-collapsible vertical-dark-menu preload-transitions 2-columns   " data-open="click" data-menu="vertical-dark-menu" data-col="2-columns">

    <!-- BEGIN: Header-->
    <header class="page-topbar" id="header">
        <div class="navbar navbar-fixed">
            <nav class="navbar-main navbar-color nav-collapsible sideNav-lock navbar-light">
                <div class="nav-wrapper">
                    <ul class="navbar-list right">
                        <li><a  href="{{route('admin.profile')}}" class="waves-effect waves-block waves-light notification-button"><i class="material-icons">person</i></a></li>
                            <li><a  href="{{route('Auth.logout')}}" class="waves-effect waves-block waves-light notification-button"><i class="material-icons">exit_to_app</i></a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>
    <!-- END: Header-->




    <!-- BEGIN: SideNav-->
    @include('back.admin.layout.menu')
    <!-- END: SideNav-->

    <!-- BEGIN: Page Main-->
    <div id="main">
        @yield('content')
    </div>
    <!-- END: Page Main-->

    @include('back.admin.layout.footer')
    @yield('script')
</body>

</html>

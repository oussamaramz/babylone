@extends('back.admin.layout.layout')
@section('content')

    <div class="row">
        <div class="col s12">
            <div class="container">
                <div class="section">
                    <div id="card-stats" class="pt-0">
                        <div class="row">
                            <div class="col s12 m6 l6 xl3">
                                <div class="card gradient-45deg-light-blue-cyan gradient-shadow min-height-100 white-text animate fadeLeft">
                                    <div class="padding-4">
                                        <div class="row">
                                            <div class="col s7 m7">
                                                <i class="material-icons background-round mt-5">people</i>
                                                <p>Inscriptions</p>
                                            </div>
                                            <div class="col s5 m5 right-align">
                                                <h5 class="mb-0 white-text">{{\App\Etudiant::get()->count()}}</h5>
                                                <p class="no-margin"></p>
                                                <p></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col s12 m6 l6 xl3">
                                <div class="card gradient-45deg-red-pink gradient-shadow min-height-100 white-text animate fadeLeft">
                                    <div class="padding-4">
                                        <div class="row">
                                            <div class="col s7 m7">
                                                <i class="material-icons background-round mt-5">collections_bookmark</i>
                                                <p>Articles</p>
                                            </div>
                                            <div class="col s5 m5 right-align">
                                                <h5 class="mb-0 white-text">{{\App\Blog::get()->count()}}</h5>
                                                <p class="no-margin"></p>
                                                <p></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col s12 m6 l6 xl3">
                                <div class="card gradient-45deg-amber-amber gradient-shadow min-height-100 white-text animate fadeRight">
                                    <div class="padding-4">
                                        <div class="row">
                                            <div class="col s7 m7">
                                                <i class="material-icons background-round mt-5">email</i>
                                                <p>Messages</p>
                                            </div>
                                            <div class="col s5 m5 right-align">
                                                <h5 class="mb-0 white-text">{{\App\Messagrie::get()->count()}}</h5>
                                                <p class="no-margin"></p>
                                                <p></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col s12 m6 l6 xl3">
                                <div class="card gradient-45deg-green-teal gradient-shadow min-height-100 white-text animate fadeRight">
                                    <div class="padding-4">
                                        <div class="row">
                                            <div class="col s7 m7">
                                                <i class="material-icons background-round mt-5">insert_photo</i>
                                                <p>Galleries</p>
                                            </div>
                                            <div class="col s5 m5 right-align">
                                                <h5 class="mb-0 white-text">{{\App\Gallerie::get()->count()}}</h5>
                                                <p class="no-margin"></p>
                                                <p></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12">
                                    <div class="sample-chart-wrapper">
                                        <canvas id="line-chart" height="400"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js"></script>
<script>
    // /*
// * ChartJS - Chart
// */

// Line chart
// ------------------------------
$(window).on("load", function() {
    //Get the context of the Chart canvas element we want to select
    var ctx = $("#line-chart");

    // Chart Options
    var chartOptions = {
       responsive: true,
       maintainAspectRatio: false,
       legend: {
          position: "bottom"
       },
       hover: {
          mode: "label"
       },
       scales: {
          xAxes: [
             {
                display: true,
                gridLines: {
                   color: "#f3f3f3",
                   drawTicks: false
                },
                scaleLabel: {
                   display: true,
                   labelString: "Month"
                }
             }
          ],
          yAxes: [
             {
                display: true,
                gridLines: {
                   color: "#f3f3f3",
                   drawTicks: false
                },
                scaleLabel: {
                   display: true,
                   labelString: "Value"
                }
             }
          ]
       },
       title: {
          display: true,
          text: "Line Chart - Legend"
       }
    };

    var inscriptions=[];
    inscriptions=jQuery.parseJSON('{{$EtudiantChart}}');
    var Messages=[];
    Messages=jQuery.parseJSON('{{$MessagrieCharts}}');

    // Chart Data
    var chartData = {
       labels: ['Janvier','Février','Mars','April','Mai','Jui','Juillet','Août','Septembre','Octobre','November','December'],
       datasets: [
          {
             label: "Inscriptions",
             data: $.each(inscriptions,function(key,valC){
                valC;
            }),
             fill: false,
             borderColor: "#e91e63",
             pointBorderColor: "#e91e63",
             pointBackgroundColor: "#FFF",
             pointBorderWidth: 2,
             pointHoverBorderWidth: 2,
             pointRadius: 4
          },
          {
             label: "Messages",
             data: $.each(Messages,function(key,valC){
                valC;
            }),
             fill: false,
             borderColor: "#03a9f4",
             pointBorderColor: "#03a9f4",
             pointBackgroundColor: "#FFF",
             pointBorderWidth: 2,
             pointHoverBorderWidth: 2,
             pointRadius: 4
          },
       ]
    };

    var config = {
       type: "line",

       // Chart Options
       options: chartOptions,

       data: chartData
    };

    // Create the chart
    var lineChart = new Chart(ctx, config);

 });

</script>
@endsection

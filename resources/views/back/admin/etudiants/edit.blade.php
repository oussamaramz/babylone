@extends('back.admin.layout.layout')
@section('style')

@endsection
@section('content')

    <div class="row">
        <div id="breadcrumbs-wrapper" data-image="{{asset('assets/back/images/gallery/breadcrumb-bg.jpg')}}" class="breadcrumbs-bg-image" style="background-image: url(&quot;../../../app-assets/images/gallery/breadcrumb-bg.jpg&quot;);">
            <!-- Search for small screen-->
            <div class="container">
                <div class="row">
                    <div class="col s12 m6 l6">
                        <h5 class="breadcrumbs-title mt-0 mb-0"><span>Modifier : {{$etudiant->nomCompletEtudiant}}</span></h5>
                    </div>
                </div>
            </div>
        </div>
        @include('back.alert')
        <div class="col s12">
            <div class="container">
                <div class="section">
                    {{-- start --}}
                    <form method="POST" action="{{route('admin.EtudiantUpdate',['id'=>$etudiant->id])}}" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="input-field col s12">
                            <input type="text" id="nomCompletEtudiant" name="nomCompletEtudiant" value="{{$etudiant->nomCompletEtudiant}}">
                                <label for="fn">Nom Complet Etudiant</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input type="text" id="nomCompletTuteur" name="nomCompletTuteur" value="{{$etudiant->nomCompletTuteur}}">
                                <label for="fn">Nom Complet Tuteur</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6">
                                <select name="niveauActuel">
                                    <option disabled="" selected="">Niveau Actuel</option>
                                    <optgroup label="Maternelle">
                                        <option value="ps" {{$etudiant->niveauActuel == 'ps' ? 'selected':''}}>Petite section</option>
                                        <option value="ms" {{$etudiant->niveauActuel == 'ms' ? 'selected':''}}>Moyenne section</option>
                                        <option value="gs" {{$etudiant->niveauActuel == 'gs' ? 'selected':''}}>Grande section</option>
                                    </optgroup>
                                    <optgroup label="Primaire">
                                        <option value="CP" {{$etudiant->niveauActuel == 'CP' ? 'selected':''}}>CP</option>
                                        <option value="CE1" {{$etudiant->niveauActuel == 'CE1' ? 'selected':''}}>CE1</option>
                                        <option value="CE2" {{$etudiant->niveauActuel == 'CE2' ? 'selected':''}}>CE2</option>
                                        <option value="CM1" {{$etudiant->niveauActuel == 'CM1' ? 'selected':''}}>CM1</option>
                                        <option value="CM2" {{$etudiant->niveauActuel == 'CM2' ? 'selected':''}}>CM2</option>
                                        <option value="C6" {{$etudiant->niveauActuel == 'C6' ? 'selected':''}}>C6</option>
                                    </optgroup>
                                </select>
                                <label>Niveau Actuel</label>
                              </div>

                              <div class="input-field col s6">
                                <select name="niveauDemande">
                                    <option disabled="" selected="">Niveau Demandé</option>
                                    <optgroup label="Maternelle">
                                        <option value="ps" {{$etudiant->niveauDemande == 'ps' ? 'selected':''}}>Petite section</option>
                                        <option value="ms" {{$etudiant->niveauDemande == 'ms' ? 'selected':''}}>Moyenne section</option>
                                        <option value="gs" {{$etudiant->niveauDemande == 'gs' ? 'selected':''}}>Grande section</option>
                                    </optgroup>
                                    <optgroup label="Primaire">
                                        <option value="CP" {{$etudiant->niveauDemande == 'CP' ? 'selected':''}}>CP</option>
                                        <option value="CE1" {{$etudiant->niveauDemande == 'CE1' ? 'selected':''}}>CE1</option>
                                        <option value="CE2" {{$etudiant->niveauDemande == 'CE2' ? 'selected':''}}>CE2</option>
                                        <option value="CM1" {{$etudiant->niveauDemande == 'CM1' ? 'selected':''}}>CM1</option>
                                        <option value="CM2" {{$etudiant->niveauDemande == 'CM2' ? 'selected':''}}>CM2</option>
                                        <option value="C6" {{$etudiant->niveauDemande == 'C6' ? 'selected':''}}>C6</option>
                                    </optgroup>
                                </select>
                                <label>Niveau Demande</label>
                              </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="email" type="email" name="email" value="{{$etudiant->email}}">
                                <label for="email">Email</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <input type="text" id="tel" name="tel" value="{{$etudiant->tel}}">
                                <label for="fn">Telephone</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="input-field col s12">
                                <input type="text" class="datepicker" name="dateNaissance" value="{{$etudiant->dateNaissance}}">
                                <label for="fn">Date Naissance</label>
                            </div>
                        </div>


                        <div class="file-field input-field">
                            <div class="col s2">
								<img src="{{asset('assets/back/images/etudiant'.$etudiant->image)}}" alt=""
									class="circle responsive-img">
							</div>
                            <div class="btn">
                                <span>Image</span>
                                <input type="file" name="image">
                            </div>
                            <div class="file-path-wrapper">
                                <input class="file-path validate" type="text">
                            </div>
                        </div>

                        <div class="col s12">
                            <p>Genre </p>
                            <p>
                                <label>
                                    <input name="gender" type="radio" {{$etudiant->gender == 1 ? 'checked':''}} value="1">
                                    <span>Homme</span>
                                </label>
                            </p>

                            <label>
                                <input name="gender" type="radio" {{$etudiant->gender == 0 ? 'checked':''}} value="2">
                                <span>Femme</span>
                            </label>
                            <div class="input-field">
                                <small class="errorTxt8"></small>
                            </div>
                        </div>
                        <div class="row">
                            <div class="row">
                                <div class="input-field col s12">
                                    <button class="btn cyan waves-effect waves-light right" type="submit"
                                        name="action">Enregistrer
                                        <i class="material-icons right">send</i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>

                    {{-- end --}}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')

@endsection

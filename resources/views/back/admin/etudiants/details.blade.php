@extends('back.admin.layout.layout')
@section('style')

@endsection
@section('content')

    <div class="row">
        <div id="breadcrumbs-wrapper" data-image="{{asset('assets/back/images/gallery/breadcrumb-bg.jpg')}}" class="breadcrumbs-bg-image" style="background-image: url(&quot;{{asset('assets/back/images/gallery/breadcrumb-bg.jpg')}}&quot;);">
            <!-- Search for small screen-->
            <div class="container">
                <div class="row">
                    <div class="col s12 m6 l6">
                    <h5 class="breadcrumbs-title mt-0 mb-0"><span>Details Etudiant : {{$etudiant->nomCompletEtudiant}}</span></h5>
                    </div>
                </div>
            </div>
        </div>
        @include('back.alert')
        <div class="col s12">
            <div class="container">
                <!-- users view start -->

                <div class="section users-view">
                    <!-- users view media object start -->
                    <div class="card-panel">
                        <div class="row">

                                <div class="col s12 m3">
                                    <table class="userInfoTable centered">
                                        <tr>
                                            <td>
                                                @if ($etudiant->image)
                                                <a href="#" class="avatar">
                                                    <img src="{{asset('back/images/etudiant/'.$etudiant->image)}}"
                                                        alt="users view avatar" class="z-depth-4 circle" height="120"
                                                        width="120">
                                                </a>
                                                @else
                                                <i class="material-icons large">account_circle</i>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><b style="font-size: 25px;">{{$etudiant->nomCompletEtudiant}}</b></td>
                                        </tr>
                                        <tr>
                                            <td class="users-view-latest-activity">
                                                {{$etudiant->email}}</td>
                                        </tr>
                                        <tr>
                                            <td class="users-view-latest-activity">
                                                {{$etudiant->tel}}</td>
                                        </tr>
                                        <tr>
                                            <td>{!! $etudiant->active != null ? '<span class=" users-view-status chip green lighten-5 green-text">Traité</span>' : '<span class=" users-view-status chip red lighten-5 red-text">Non Traité</span>' !!}</td>
                                        </tr>
                                        <tr>
                                            <td class="users-view-latest-activity">
                                                @if ($etudiant->active == null)
                                                <a href="{{route('admin.EtudiantEnabled',['id'=>$etudiant->id])}}"
                                                    class="btn-small indigo green">Traité <i
                                                        class="material-icons left">check</i></a>
                                                @endif
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                                <div class="col s12 m9">
                                    <div class="row">
                                        {{-- <div class="col12 quick-action-btns display-flex justify-content-end align-items-center mb-2">
                                            <a class="btn dropdown-settings waves-effect waves-light breadcrumbs-btn right ml-2"
                                                href="#!" data-target="dropdown1"><i
                                                    class="material-icons hide-on-med-and-up">settings</i><span
                                                    class="hide-on-small-onl">Actions</span><i
                                                    class="material-icons right">arrow_drop_down</i></a>
                                            <ul class="dropdown-content" id="dropdown1" tabindex="0" style="">
                                                <li tabindex="0"><a
                                                        href="{{route('admin.EtudiantEdit',['id'=>$etudiant->id])}}"
                                                        class="grey-text text-darken-2" href="user-profile-page.html">Modifier<i
                                                            class="material-icons left">create</i></a>
                                                </li>
                                                <li tabindex="0"><a
                                                        href="{{route('admin.EtudiantDelete',['id'=>$etudiant->id])}}"
                                                        class="
                                                            grey-text text-darken-2 delete" href="app-contacts.html">Supprimer<i
                                                            class="material-icons left">clear</i></a></li>
                                            </ul>
                                        </div> --}}

                                        <div class="col s12">
                                            <ul class="tabs">
                                                <li class="tab col m12"><a class="active" href="#info">Etudiant
                                                        Information</a>
                                                </li></ul>
                                        </div>
                                        <div id="info" class="col s12">

                                            <table class="">
                                                <tbody>
                                                    <tr>
                                                        <td>Nom Tuteur:</td>
                                                        <td class="users-view-verified">
                                                            {{$etudiant->nomCompletTuteur}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Niveau Actuel:</td>
                                                        <td class="users-view-verified">
                                                            {{$etudiant->niveauActuel}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>niveau Demande:</td>
                                                        <td class="users-view-verified">
                                                            {{$etudiant->niveauDemande}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Message Inscription:</td>
                                                        <td class="users-view-verified">
                                                            {{$etudiant->messageInscription}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Genre:</td>
                                                        <td class="users-view-verified">
                                                            {{$etudiant->gender == '1' ? 'Homme':($etudiant->gender == '0' ? 'Femme':'-')}}
                                                        </td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script>
    function check_pass() {
		if (document.getElementById('password').value ==
				document.getElementById('confirm_password').value) {
			document.getElementById('submit').disabled = true;
		} else {
			document.getElementById('submit').disabled = false;
		}
	}
</script>
<script>
    $(document).ready(function(){
                $("a.delete").on("click",function(event){
                event.stopPropagation();
                Swal.fire({
                    title: 'Êtes-vous sûr?',
                    text: "Vous ne pourrez pas revenir en arrière!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Oui, supprimez-le!',
                    cancelButtonText: 'Annuler'
                    }).then((result) => {
                    if (result.isConfirmed) {
                        window.location.href = this.getAttribute('href');
                        Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                        )
                    }
                    })
                event.preventDefault();

                });
            });
</script>
@endsection

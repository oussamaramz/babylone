@extends('back.admin.layout.layout')
@section('style')
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/vendors/data-tables/css/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/back/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/back/vendors/data-tables/css/select.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/back/css/pages/data-tables.css')}}">
@endsection
@section('content')

    <div class="row">
        <div id="breadcrumbs-wrapper" data-image="{{asset('assets/back/images/gallery/breadcrumb-bg.jpg')}}" class="breadcrumbs-bg-image" style="background-image: url(&quot;{{asset('assets/back/images/gallery/breadcrumb-bg.jpg')}}&quot;);">
            <!-- Search for small screen-->
            <div class="container">
                <div class="row">
                    <div class="col s12 m6 l6">
                        <h5 class="breadcrumbs-title mt-0 mb-0"><span>List des Demandes</span></h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s12">
            <div class="container">
                <div class="section">
                    {{-- start --}}

                    <div class="row">
                        <div class="col s12">
                            <div class="card">
                                <div class="card-content">
                                    <h4 class="card-title">List des Demandes</h4>
                                    <div class="row">
                                        <div class="col s12">
                                            <table id="page-length-option" class="display">
                                                <thead>
                                                    <tr>
                                                        <th>Nom etudiant</th>
                                                        <th>Nom tuteur</th>
                                                        <th>Niveau Actuel</th>
                                                        <th>Niveau Demande</th>
                                                        <th>Genre</th>
                                                        <th>Date Naissance</th>
                                                        <th>Lu</th>
                                                        <th>Status</th>
                                                        <th>Details</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @foreach ($subscribers as $item)
                                                    <tr>
                                                    <td>{{$item->nomCompletEtudiant}}</td>
                                                    <td>{{$item->nomCompletTuteur}}</td>
                                                    <td>{{$item->niveauActuel}}</td>
                                                    <td>{{$item->niveauDemande}}</td>
                                                    <td>{{$item->gender == '1' ? 'Homme':($item->gender == '0' ? 'Femme':'-')}}</td>
                                                    <td>{{$item->dateNaissance}}</td>
                                                    <td>{!! $item->enabled != null ? '<span class=" users-view-status chip green lighten-5 green-text">Lu</span>' : '<span class=" users-view-status chip orange lighten-5 orange-text">Non lu</span>' !!}</td>
                                                    <td>{!! $item->active != null ? '<span class=" users-view-status chip green lighten-5 green-text">Traité</span>' : '<span class=" users-view-status chip red lighten-5 red-text">Pas Traité</span>' !!}</td>
                                                    <td class="center-align">
                                                    <a href="{{route('admin.EtudiantDetails',['id'=>$item->id])}}"><i class="material-icons bleu-text">remove_red_eye</i></a>
                                                    </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>Nom etudiant</th>
                                                        <th>Nom tuteur</th>
                                                        <th>Niveau Actuel</th>
                                                        <th>Niveau Demande</th>
                                                        <th>Genre</th>
                                                        <th>Date Naissance</th>
                                                        <th>Lu</th>
                                                        <th>Status</th>
                                                        <th>Details</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- end --}}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script src="{{asset('assets/back//vendors/data-tables/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/back//vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/back//vendors/data-tables/js/dataTables.select.min.js')}}"></script>
{{-- <script src="{{asset('assets/back/js/scripts/data-tables.js')}}"></script> --}}
<script>
    $('#page-length-option').dataTable({
    "language": {
    "sEmptyTable":     "Aucune donnée disponible dans le tableau",
    "sInfo":           "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
    "sInfoEmpty":      "Affichage de l'élément 0 à 0 sur 0 élément",
    "sInfoFiltered":   "(filtré à partir de _MAX_ éléments au total)",
    "sInfoPostFix":    "",
    "sInfoThousands":  ",",
    "sLengthMenu":     "Afficher _MENU_ éléments",
    "sLoadingRecords": "Chargement...",
    "sProcessing":     "Traitement...",
    "sSearch":         "Rechercher :",
    "sZeroRecords":    "Aucun élément correspondant trouvé",
    "oPaginate": {
        "sFirst":    "Premier",
        "sLast":     "Dernier",
        "sNext":     "Suivant",
        "sPrevious": "Précédent"
    },
    "oAria": {
        "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
        "sSortDescending": ": activer pour trier la colonne par ordre décroissant"
    },
    "select": {
            "rows": {
                "_": "%d lignes sélectionnées",
                "0": "Aucune ligne sélectionnée",
                "1": "1 ligne sélectionnée"
            } 
    }
}
});
</script>
@endsection

@extends('back.admin.layout.layout')
@section('style')
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/css/pages/app-sidebar.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/css/pages/app-email.css')}}">
@endsection
@section('content')

    <div class="row">
        <div id="breadcrumbs-wrapper" data-image="{{asset('assets/back/images/gallery/breadcrumb-bg.jpg')}}" class="breadcrumbs-bg-image" style="background-image: url(&quot;{{asset('assets/back/images/gallery/breadcrumb-bg.jpg')}}&quot;);">
            <!-- Search for small screen-->
            <div class="container">
                <div class="row">
                    <div class="col s12 m6 l6">
                        <h5 class="breadcrumbs-title mt-0 mb-0"><span>List Messagries</span></h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s12">
            <div class="container">
                <div class="section">
                    {{-- start --}}
                    <!-- Content Area Starts -->
                    <div class="app-email">
                        <div class="content-area">
                            <div class="app-wrapper">
                                <div class="card card card-default scrollspy border-radius-6 fixed-width">
                                    <div class="card-content p-0 pb-2">
                                        <div class="collection email-collection">
                                            @foreach ($messagries as $item)
                                            <div class="email-brief-info collection-item animate fadeUp delay-1">
                                            <a href="{{route('admin.messagedetails',['id'=>$item->id])}}" class="list-content">
                                                    <div class="list-title-area">
                                                        <div class="user-media">
                                                        <div class="list-title">{{$item->nom.' '.$item->prenom}}</div>
                                                        </div>
                                                        <div class="title-right">
                                                            <span class="badge grey lighten-3">
                                                                @if ($item->lu == null)
                                                                <i class="light-yellow-text material-icons small-icons mr-2">
                                                                    fiber_manual_record </i>nouvaux
                                                                @else
                                                                <i class="light-green-text material-icons small-icons mr-2">
                                                                    fiber_manual_record </i>lu
                                                                @endif
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="list-desc">{{$item->message}}</div>
                                                </a>
                                                <div class="list-right">
                                                    <div class="list-date"> {{$item->created_at->format('j F, Y')}} </div>
                                                </div>
                                            </div>
                                            @endforeach

                                            <div class="no-data-found collection-item">
                                                <h6 class="center-align font-weight-500">No Results Found</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Content Area Ends -->
                    {{-- end --}}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script src="{{asset('assets/back/js/scripts/app-email.js')}}"></script>
@endsection

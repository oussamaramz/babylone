@extends('back.admin.layout.layout')
@section('style')
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/vendors/data-tables/css/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/back/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/back/vendors/data-tables/css/select.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/back/css/pages/data-tables.css')}}">
@endsection
@section('content')

    <div class="row">
        <div id="breadcrumbs-wrapper" data-image="{{asset('assets/back/images/gallery/breadcrumb-bg.jpg')}}" class="breadcrumbs-bg-image" style="background-image: url(&quot;{{asset('assets/back/images/gallery/breadcrumb-bg.jpg')}}&quot;);">
            <!-- Search for small screen-->
            <div class="container">
                <div class="row">
                    <div class="col s12 m6 l6">
                        <h5 class="breadcrumbs-title mt-0 mb-0"><span>List Albums</span></h5>
                    </div>
                    <div class="col s12 m6 16 right-align-md">
                        <a href="#addAlbum" class="waves-effect pink accent-2 waves-light btn mb-1 mr-1  modal-trigger">
                                <i class="material-icons left">add</i> Ajouter</a>
                        </div>
                </div>
            </div>
        </div>
        <div class="col s12">
            <div class="container">
                <div class="section">
                    {{-- start --}}

                    <div class="row">
                        <div class="col s12">
                            <div class="card">
                                <div class="card-content">
                                    <h4 class="card-title">List Albums</h4>
                                    <div class="row">
                                        <div class="col s12">
                                            <table id="page-length-option" class="display">
                                                <thead>
                                                    <tr>
                                                        <th>Label</th>
                                                        <th>Alias</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @foreach ($albums as $item)
                                                    <tr>
                                                    <td>{{$item->label}}</td>
                                                    <td>{{$item->alias}}</td>
                                                    <td class="center-align">
                                                    <a class="modal-trigger editAlbum" href="#editAlbum" data-id="{{$item->id}}" data-label="{{$item->label}}"><i class="material-icons bleu-text">remove_red_eye</i></a>
                                                    <a
                                                        href="{{route('admin.albumDelete',['id'=>$item->id])}}"
                                                        class="
                                                            grey-text text-darken-2 delete" href="app-contacts.html"><i
                                                            class="material-icons red-text">clear</i></a>
                                                    </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>Label</th>
                                                        <th>Alias</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- end --}}
                </div>
            </div>
        </div>
    </div>




    <!-- Modal add album -->
  <div id="addAlbum" class="modal">
    <div class="modal-content">
      <h4>Ajouter Album</h4>
      <form method="POST" action="{{route('admin.albumStore')}}">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <div class="row">
            <div class="input-field col s12">
                <input type="text" id="label" name="label" value="{{old('label')}}" required oninvalid="this.setCustomValidity('label est obligatoire')">
                    <label for="fn">Label* </label>
            </div>
        </div>
        <div class="row">
            <div class="row">
                <div class="input-field col s12">
                    <button class="btn cyan waves-effect waves-light right" type="submit"
                        name="action">Enregistrer
                        <i class="material-icons right">send</i>
                    </button>
                </div>
            </div>
        </div>
    </form>
    </div>
  </div>


      <!-- Modal add album -->
      <div id="editAlbum" class="modal">
        <div class="modal-content">
          <h4>Ajouter Album</h4>
          <form method="POST" action="{{route('admin.albumUpdate')}}">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <input type="hidden" name="idAlbum" class="idAlbum">
            <div class="row">
                <div class="input-field col s12">
                    <input placeholder="Placeholder" id="label" name="label" type="text" class="validate labelAlbum">
                    <label for="first_name1" class="active">Label*</label>
                </div>
            </div>
            <div class="row">
                <div class="row">
                    <div class="input-field col s12">
                        <button class="btn cyan waves-effect waves-light right" type="submit"
                            name="action">Enregistrer
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                </div>
            </div>
        </form>
        </div>
      </div>
@endsection
@section('script')
<script src="{{asset('assets/back//vendors/data-tables/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/back//vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/back//vendors/data-tables/js/dataTables.select.min.js')}}"></script>
{{-- <script src="{{asset('assets/back/js/scripts/data-tables.js')}}"></script> --}}
<script>
    $('#page-length-option').dataTable({
    "language": {
    "sEmptyTable":     "Aucune donnée disponible dans le tableau",
    "sInfo":           "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
    "sInfoEmpty":      "Affichage de l'élément 0 à 0 sur 0 élément",
    "sInfoFiltered":   "(filtré à partir de _MAX_ éléments au total)",
    "sInfoPostFix":    "",
    "sInfoThousands":  ",",
    "sLengthMenu":     "Afficher _MENU_ éléments",
    "sLoadingRecords": "Chargement...",
    "sProcessing":     "Traitement...",
    "sSearch":         "Rechercher :",
    "sZeroRecords":    "Aucun élément correspondant trouvé",
    "oPaginate": {
        "sFirst":    "Premier",
        "sLast":     "Dernier",
        "sNext":     "Suivant",
        "sPrevious": "Précédent"
    },
    "oAria": {
        "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
        "sSortDescending": ": activer pour trier la colonne par ordre décroissant"
    },
    "select": {
            "rows": {
                "_": "%d lignes sélectionnées",
                "0": "Aucune ligne sélectionnée",
                "1": "1 ligne sélectionnée"
            } 
    }
}
});
</script>
<script src="{{asset('assets/back/js/scripts/advance-ui-modals.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
    $(document).ready(function(){
                $("a.delete").on("click",function(event){
                event.stopPropagation();
                Swal.fire({
                    title: 'Êtes-vous sûr?',
                    text: "Vous ne pourrez pas revenir en arrière!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Oui, supprimez-le!',
                    cancelButtonText: 'Annuler'
                    }).then((result) => {
                    if (result.isConfirmed) {
                        window.location.href = this.getAttribute('href');
                        Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                        )
                    }
                    })
                event.preventDefault();

                });


                $('a.editAlbum').on("click",function(event){
                    var id = $(this).data('id');
                    var label = $(this).data('label');

                    $('input.idAlbum').val(id);
                    $('input.labelAlbum').val(label);
                });
            });
</script>
@endsection

@extends('back.admin.layout.layout')
@section('style')
<link rel="stylesheet" type="text/css" href="{{asset('assets/back/vendors/data-tables/css/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/back/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/back/vendors/data-tables/css/select.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/back/css/pages/data-tables.css')}}">
@endsection
@section('content')

    <div class="row">
        <div id="breadcrumbs-wrapper" data-image="{{asset('assets/back/images/gallery/breadcrumb-bg.jpg')}}" class="breadcrumbs-bg-image" style="background-image: url(&quot;{{asset('assets/back/images/gallery/breadcrumb-bg.jpg')}}&quot;);">
            <!-- Search for small screen-->
            <div class="container">
                <div class="row">
                    <div class="col s12 m6 l6">
                        <h5 class="breadcrumbs-title mt-0 mb-0"><span>List Articles</span></h5>
                    </div>
                    <div class="col s12 m6 16 right-align-md">
                    <a href="{{route('admin.blogAdd')}}" class="waves-effect pink accent-2 waves-light btn mb-1 mr-1">
                            <i class="material-icons left">add</i> Ajouter</a>
                    </div>
                </div>
            </div>
        </div>
        @include('back.alert')
        <div class="col s12">
            <div class="container">
                <div class="section">
                    {{-- start --}}

                    <div class="row">
                        <div class="col s12">
                            <div class="card">
                                <div class="card-content">
                                    <h4 class="card-title">List Articles</h4>
                                    <div class="row">
                                        <div class="col s12">
                                            <table id="page-length-option" class="display">
                                                <thead>
                                                    <tr>
                                                        <th>Image</th>
                                                        <th>Label</th>
                                                        <th>Intro</th>
                                                        <th>mot clé</th>
                                                        <th>Details</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @foreach ($blogs as $item)
                                                    <tr>
                                                    <td>
                                                        <img src="{{ asset('back/images/blogs/'.$item->image) }}" alt="" class="z-depth-4 " height="64" width="100">
                                                    </td>
                                                    <td>{{$item->labelFR}}</td>
                                                    <td>{{$item->introFR}}</td>
                                                    <td>{{$item->keyWordsFR}}</td>
                                                    <td class="center-align">
                                                    <a href="{{route('admin.BlogDetails',['id'=>$item->id])}}"><i class="material-icons bleu-text">remove_red_eye</i></a>
                                                    </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>Image</th>
                                                        <th>Label</th>
                                                        <th>Intro</th>
                                                        <th>mot clé</th>
                                                        <th>Details</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- end --}}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script src="{{asset('assets/back//vendors/data-tables/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/back//vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/back//vendors/data-tables/js/dataTables.select.min.js')}}"></script>
{{-- <script src="{{asset('assets/back/js/scripts/data-tables.js')}}"></script> --}}
<script>
    $('#page-length-option').dataTable({
    "language": {
    "sEmptyTable":     "Aucune donnée disponible dans le tableau",
    "sInfo":           "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
    "sInfoEmpty":      "Affichage de l'élément 0 à 0 sur 0 élément",
    "sInfoFiltered":   "(filtré à partir de _MAX_ éléments au total)",
    "sInfoPostFix":    "",
    "sInfoThousands":  ",",
    "sLengthMenu":     "Afficher _MENU_ éléments",
    "sLoadingRecords": "Chargement...",
    "sProcessing":     "Traitement...",
    "sSearch":         "Rechercher :",
    "sZeroRecords":    "Aucun élément correspondant trouvé",
    "oPaginate": {
        "sFirst":    "Premier",
        "sLast":     "Dernier",
        "sNext":     "Suivant",
        "sPrevious": "Précédent"
    },
    "oAria": {
        "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
        "sSortDescending": ": activer pour trier la colonne par ordre décroissant"
    },
    "select": {
            "rows": {
                "_": "%d lignes sélectionnées",
                "0": "Aucune ligne sélectionnée",
                "1": "1 ligne sélectionnée"
            } 
    }
}
});
</script>
@endsection

@extends('back.admin.layout.layout')
@section('style')

@endsection
@section('content')

    <div class="row">
        <div id="breadcrumbs-wrapper" data-image="{{asset('assets/back/images/gallery/breadcrumb-bg.jpg')}}" class="breadcrumbs-bg-image" style="background-image: url(&quot;{{asset('assets/back/images/gallery/breadcrumb-bg.jpg')}}&quot;);">
            <!-- Search for small screen-->
            <div class="container">
                <div class="row">
                    <div class="col s12 m6 l6">
                    <h5 class="breadcrumbs-title mt-0 mb-0"><span>Details Article : <b>{{$blog->labelFR}}</b></span></h5>
                    </div>

                    <a class='dropdown-trigger btn pink accent-2 right' href='#' data-target='dropdown1'>Actions</a>
  <!-- Dropdown Structure -->
  <ul id='dropdown1' class='dropdown-content'>
  <li><a href="{{route('admin.blogEdit',['id'=>$blog->id])}}" class="pink-text"><i class="material-icons">create</i>Modifier</a></li>
  <li><a href="{{route('admin.blogDelete',['id'=>$blog->id])}}" class="pink-text"><i class="material-icons">close</i>Supprimer</a></li>
  </ul>
                </div>
            </div>
        </div>
        @include('back.alert')
        <div class="col s12">
            <div class="container">
                <div class="section">
                    {{-- start --}}

                    <div class="row user-profile mt-1 ml-0 mr-0 ImageBanner">
                        <img class="responsive-img" alt="" src="{{asset('back/images/blogs/'.$blog->banner)}}">
                    </div>
                    <div class="section" id="user-profile">
                        <div class="row">
                            <!-- User Profile Feed -->
                            <div class="col s12 m4 l3 user-section-negative-margin">
                                <div class="row">
                                    <div class="col s12 center-align">
                                        <img class="responsive-img card-border z-depth-2 mt-2"
                                            src="{{asset('back/images/blogs/'.$blog->image)}}" alt="">
                                        <br>
                                        {!! $blog->enebled == 1? '<span
                                                class=" users-view-status chip green lighten-5 green-text">Active</span>':'<span
                                                class=" users-view-status chip red lighten-5 red-text">désactivé</span>' !!}
                                        @if ($blog->enebled != 1)
                                        <a href="{{route('admin.blogActive',['id'=>$blog->id])}}"
                                            class="btn-small indigo green">Activer <i
                                                class="material-icons left">check</i></a>
                                        @else
                                        <a href="{{route('admin.blogActive',['id'=>$blog->id])}}"
                                            class="btn-small btn-indigo red ">
                                            Desactiver<i class="material-icons left">clear</i></a>
                                        @endif
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col s12">
                                        <h6 class="m-0">Keywords Francais</h6>
                                        <p class="m-1">
                                            <?php $data=$blog->keyWordsFR;
                                             $data=explode(",",$data);?>
                                            @foreach ($data as $item)
                                            <div class="chip">
                                                {{$item}}
                                            </div>
                                            @endforeach
                                        </p>
                                    </div>
                                    <hr>
                                    <div class="col s12">
                                        <h6 class="m-0">Keywords Englais</h6>
                                        <p class="m-1">
                                            <?php $data=$blog->keyWordsENG;
                                             $data=explode(",",$data);?>
                                            @foreach ($data as $item)
                                            <div class="chip">
                                                {{$item}}
                                            </div>
                                            @endforeach
                                        </p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row user-projects">
                                    <h6 class="col s12">Meta Francais</h6>
                                    <p><span class="badge indigo lighten-5" style="float: left;color:black;	">Titre </span>:
                                        {{$blog->metaTitleFR}}</p><br>
                                    <p><span class="badge indigo lighten-5" style="float: left;color:black;	">Descriptif
                                        </span>:
                                        {{$blog->metaDescriptionFR}}</p>
                                </div>
                                <hr>
                                <div class="row user-projects">
                                    <h6 class="col s12">Meta Englais</h6>
                                    <p><span class="badge indigo lighten-5" style="float: left;color:black;	">Titre </span>:
                                        {{$blog->metaTitleENG}}</p><br>
                                    <p><span class="badge indigo lighten-5" style="float: left;color:black;	">Descriptif
                                        </span>:
                                        {{$blog->metaDescriptionENG}}</p>
                                </div>

                            </div>
                            <!-- User Post Feed -->
                            <div class="col s12 m8 l9">
                                {{-- tabs lang --}}
                                <div class="col s12">
                                    <ul class="tabs">
                                        <li class="tab col m6"><a class="active" href="#fr">article Francais</a>
                                        </li>
                                        <li class="tab col m6"><a href="#eng">article Englias</a></li>
                                    </ul>
                                </div>
                                {{-- contant tabs FR --}}
                                <div id="fr" class="col s12">
                                    <div class="card user-card-negative-margin z-depth-0" id="feed">
                                        <div class="card-content card-border-gray">
                                            <div class="row">
                                                <div class="col s12">
                                                    <h4>{{$blog->labelFR}}</h4>
                                                    <div class="progress">
                                                        <div class="determinate" style="width: 70%"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mt-5">
                                                <h5>À propos de cette catégorie</h5>
                                                <div class="col s11">
                                                    {{$blog->introFR}}
                                                </div>
                                            </div>
                                            <hr class="mt-5">
                                            <div class="row mt-5">
                                                <h5>Détails</h5>
                                                <div class="col s11">
                                                    {{$blog->detailsFR}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{-- contant tabs ENG --}}
                                <div id="eng" class="col s12">
                                    <div class="card user-card-negative-margin z-depth-0" id="feed">
                                        <div class="card-content card-border-gray">
                                            <div class="row">
                                                <div class="col s12">
                                                    <h4>{{$blog->labelENG}}</h4>
                                                    <div class="progress">
                                                        <div class="determinate" style="width: 70%"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mt-5">
                                                <h5>À propos de cette catégorie</h5>
                                                <div class="col s11">
                                                    {{$blog->introENg}}
                                                </div>
                                            </div>
                                            <hr class="mt-5">
                                            <div class="row mt-5">
                                                <h5>Détails</h5>
                                                <div class="col s11">
                                                    {{$blog->detailsENg}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- end --}}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

                <script>
                    $(document).ready(function(){
						$("a.delete").on("click",function(event){
						event.stopPropagation();
						Swal.fire({
							title: 'Êtes-vous sûr?',
							text: "Vous ne pourrez pas revenir en arrière!",
							icon: 'warning',
							showCancelButton: true,
							confirmButtonColor: '#3085d6',
							cancelButtonColor: '#d33',
							confirmButtonText: 'Oui, supprimez-le!',
							cancelButtonText: 'Annuler'
							}).then((result) => {
							if (result.isConfirmed) {
								window.location.href = this.getAttribute('href');
								Swal.fire(
								'Deleted!',
								'Your file has been deleted.',
								'success'
								)
							}
							})
						event.preventDefault();

						});
					});
                </script>
@endsection

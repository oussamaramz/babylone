@extends('back.admin.layout.layout')
@section('style')

@endsection
@section('content')

    <div class="row">
        <div id="breadcrumbs-wrapper" data-image="{{asset('assets/back/images/gallery/breadcrumb-bg.jpg')}}" class="breadcrumbs-bg-image" style="background-image: url(&quot;{{asset('assets/back/images/gallery/breadcrumb-bg.jpg')}}&quot;);">
            <!-- Search for small screen-->
            <div class="container">
                <div class="row">
                    <div class="col s12 m6 l6">
                        <h5 class="breadcrumbs-title mt-0 mb-0"><span>Ajouet Article</span></h5>
                    </div>
                    <div class="col s12 m6 16 right-align-md">
                        <a href="{{route('admin.blogs')}}" class="waves-effect pink accent-2 waves-light btn mb-1 mr-1">
                            <i class="material-icons left">dehaze</i> List Articles</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s12">
            <div class="container">
                <div class="section">
                    {{-- start --}}
                    @include('back.alert')
                    <div class="col s12">
                        <div class="container">
                            <div class="section">
                                {{-- start --}}
                                <form method="POST" action="{{route('admin.blogStore')}}" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                    <input type="hidden" name="idblog" value="{{$blog->id}}">
                                    {{-- tabs lang --}}
                                    {{-- <div class="col s12">
										<ul class="tabs">
											<li class="tab col m6"><a class="active" href="#fr">article Francais</a>
											</li>
											<li class="tab col m6"><a href="#eng">article Englias</a></li>
										</ul>
									</div> --}}

                                    {{-- contant tabs FR --}}
                                    <div id="fr" class="col s12 FR">
                                        <h4>Article en Francais</h4>
                                        <div class="progress">
                                            <div class="determinate" style="width: 70%"></div>
                                        </div>

                                        <div class="row">
                                            <div class="input-field col s10">
                                            <input type="text" id="labelFr" name="labelFr" value="{{$blog->labelFR}}" required oninvalid="this.setCustomValidity('Label est obligatoire')">
                                                <label for="fn">Label* </label>
                                            </div>
                                            <div class="input-field col s2">
                                                <ul id="translation-dropdown">
                                                    <li class="LangSwitch"><a href="#!" class="grey-text text-darken-1" data-language="en"><i class="flag-icon flag-icon-gb"></i> English</a></li>
                                                    <li class="LangSwitch"><a href="#!" class="grey-text text-darken-1" data-language="fr"><i class="flag-icon flag-icon-fr"></i> French</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col s10">
                                            <input type="text" id="introFr" name="introFr" value="{{$blog->introFR}}" required oninvalid="this.setCustomValidity('Intro est obligatoire')">
                                                <label for="fn">Intro* </label>
                                            </div>
                                            <div class="input-field col s2">
                                                <ul id="translation-dropdown">
                                                    <li class="LangSwitch"><a href="#!" class="grey-text text-darken-1" data-language="en"><i class="flag-icon flag-icon-gb"></i> English</a></li>
                                                    <li class="LangSwitch"><a href="#!" class="grey-text text-darken-1" data-language="fr"><i class="flag-icon flag-icon-fr"></i> French</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col s10">
                                            <input type="text" id="detailsFr" name="detailsFr" value="{{$blog->detailsFR}}" required oninvalid="this.setCustomValidity('Contenu d\'article est obligatoire')">
                                                <label for="fn">Contenu d'article* </label>
                                            </div>
                                            <div class="input-field col s2">
                                                <ul id="translation-dropdown">
                                                    <li class="LangSwitch"><a href="#!" class="grey-text text-darken-1" data-language="en"><i class="flag-icon flag-icon-gb"></i> English</a></li>
                                                    <li class="LangSwitch"><a href="#!" class="grey-text text-darken-1" data-language="fr"><i class="flag-icon flag-icon-fr"></i> French</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <br>
                                        <h4>SEO</h4>
                                        <div class="progress">
                                            <div class="determinate" style="width: 70%"></div>
                                        </div>

                                        <div class="row">
                                            <span class="badge orange" style="float: left">Keywords</span><br>
                                            @if ($blog->keyWordsFR == null)
                                            <div class="input-field col s10 chips chips-placeholder-FR KeywordsFR" id="KeywordsFR">
                                            </div>
                                            <div class="myTagsFR"></div>
                                            @else
                                            <div class=" input-field col s10 chips chips-initial-FR KeywordsFR" id="KeywordsFR">
                                            </div>
                                            <div class="myTagsFR"></div>
                                            @endif
                                            <div class="input-field col s2">
                                                <ul id="translation-dropdown">
                                                    <li class="LangSwitch"><a href="#!" class="grey-text text-darken-1" data-language="en"><i class="flag-icon flag-icon-gb"></i> English</a></li>
                                                    <li class="LangSwitch"><a href="#!" class="grey-text text-darken-1" data-language="fr"><i class="flag-icon flag-icon-fr"></i> French</a></li>
                                                </ul>
                                            </div>

                                        </div>
<blockquote>
                                                tape entre pour séparer vos Keywords
                                            </blockquote>
                                        <div class="row">
                                            <div class="input-field col s10 colKey">
                                                <input type="text" id="metaTitleFR" name="metaTitleFR" placeholder="Si rest le meta vide " value="{{$blog->metaTitleFR}}">
                                                <label for="fn" class="active">Meta Titre</label>
                                            </div>
                                            <div class="input-field col s2">
                                                <ul id="translation-dropdown">
                                                    <li class="LangSwitch"><a href="#!" class="grey-text text-darken-1" data-language="en"><i class="flag-icon flag-icon-gb"></i> English</a></li>
                                                    <li class="LangSwitch"><a href="#!" class="grey-text text-darken-1" data-language="fr"><i class="flag-icon flag-icon-fr"></i> French</a></li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="input-field col s10 colKey">
                                                <input type="text" id="metaDscrptFR" name="metaDscrptFR" placeholder="Si le meta vide" value="{{$blog->metaDescriptionFR}}">
                                                <label for="fn" class="active">Meta Description</label>
                                            </div>
                                            <div class="input-field col s2">
                                                <ul id="translation-dropdown">
                                                    <li class="LangSwitch"><a href="#!" class="grey-text text-darken-1" data-language="en"><i class="flag-icon flag-icon-gb"></i> English</a></li>
                                                    <li class="LangSwitch"><a href="#!" class="grey-text text-darken-1" data-language="fr"><i class="flag-icon flag-icon-fr"></i> French</a></li>
                                                </ul>
                                            </div>
                                        </div>

                                        <blockquote class="important">
                                            Si vous restez les metas vide ,il absorbe les valeurs des descrptive et titre de ce
                                            formation
                                        </blockquote>
                                    </div>



                                    {{-- contant tabs Eng --}}
                                    <div id="eng" class="col s12 EN" style="display: none;">
                                        <h4>Article en Englais</h4>
                                        <div class="progress">
                                            <div class="determinate pink" style="width: 70%"></div>
                                        </div>


                                        <div class="row">
                                            <div class="input-field col s10">
                                            <input type="text" id="labelENG" name="labelENG" value="{{$blog->labelENG}}">
                                                <label for="fn">Label </label>
                                            </div>
                                            <div class="input-field col s2">
                                                <ul id="translation-dropdown">
                                                    <li class="LangSwitch"><a href="#!" class="grey-text text-darken-1" data-language="en"><i class="flag-icon flag-icon-gb"></i> English</a></li>
                                                    <li class="LangSwitch"><a href="#!" class="grey-text text-darken-1" data-language="fr"><i class="flag-icon flag-icon-fr"></i> French</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col s10">
                                            <input type="text" id="introENG" name="introENG" value="{{$blog->introENg}}">
                                                <label for="fn">Intro </label>
                                            </div>
                                            <div class="input-field col s2">
                                                <ul id="translation-dropdown">
                                                    <li class="LangSwitch"><a href="#!" class="grey-text text-darken-1" data-language="en"><i class="flag-icon flag-icon-gb"></i> English</a></li>
                                                    <li class="LangSwitch"><a href="#!" class="grey-text text-darken-1" data-language="fr"><i class="flag-icon flag-icon-fr"></i> French</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="input-field col s10">
                                            <input type="text" id="detailsENG" name="detailsENG" value="{{$blog->detailsENg}}">
                                                <label for="fn">Contenu d'article </label>
                                            </div>
                                            <div class="input-field col s2">
                                                <ul id="translation-dropdown">
                                                    <li class="LangSwitch"><a href="#!" class="grey-text text-darken-1" data-language="en"><i class="flag-icon flag-icon-gb"></i> English</a></li>
                                                    <li class="LangSwitch"><a href="#!" class="grey-text text-darken-1" data-language="fr"><i class="flag-icon flag-icon-fr"></i> French</a></li>
                                                </ul>
                                            </div>
                                        </div>

                                        <br>
                                        <h4>SEO</h4>
                                        <div class="progress">
                                            <div class="determinate pink" style="width: 70%"></div>
                                        </div>

                                        <div class="row">
                                            <span class="badge orange" style="float: left">Keywords</span><br>
                                            @if ($blog->keyWordsENG == null)
                                            <div class="input-field col s10 chips chips-placeholder-ENG KeywordsENG" id="KeywordsENG">
                                            </div>
                                            <div class="myTagsENG"></div>
                                            @else
                                            <div class=" input-field col s10 chips chips-initial-ENG KeywordsENG" id="KeywordsENG">
                                            </div>
                                            <div class="myTagsENG"></div>
                                            @endif
                                            <div class="input-field col s2">
                                                <ul id="translation-dropdown">
                                                    <li class="LangSwitch"><a href="#!" class="grey-text text-darken-1" data-language="en"><i class="flag-icon flag-icon-gb"></i> English</a></li>
                                                    <li class="LangSwitch"><a href="#!" class="grey-text text-darken-1" data-language="fr"><i class="flag-icon flag-icon-fr"></i> French</a></li>
                                                </ul>
                                            </div>

                                        </div>
<blockquote>
                                                tape entre pour séparer vos Keywords
                                            </blockquote>
                                        <div class="row">
                                            <div class="input-field col s10 colKey">
                                                <input type="text" id="metaTitleENG" name="metaTitleENG" placeholder="Si rest le meta vide " value="{{$blog->metaTitleENG}}">
                                                <label for="fn" class="active">Meta Titre</label>
                                            </div>
                                            <div class="input-field col s2">
                                                <ul id="translation-dropdown">
                                                    <li class="LangSwitch"><a href="#!" class="grey-text text-darken-1" data-language="en"><i class="flag-icon flag-icon-gb"></i> English</a></li>
                                                    <li class="LangSwitch"><a href="#!" class="grey-text text-darken-1" data-language="fr"><i class="flag-icon flag-icon-fr"></i> French</a></li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="input-field col s10 colKey">
                                                <input type="text" id="metaDscrptENG" name="metaDscrptENG" placeholder="Si le meta vide" value="{{$blog->metaDescriptionENG}}">
                                                <label for="fn" class="active">Meta Description</label>
                                            </div>
                                            <div class="input-field col s2">
                                                <ul id="translation-dropdown">
                                                    <li class="LangSwitch"><a href="#!" class="grey-text text-darken-1" data-language="en"><i class="flag-icon flag-icon-gb"></i> English</a></li>
                                                    <li class="LangSwitch"><a href="#!" class="grey-text text-darken-1" data-language="fr"><i class="flag-icon flag-icon-fr"></i> French</a></li>
                                                </ul>
                                            </div>
                                        </div>

                                        <blockquote class="important">
                                            Si vous restez les metas vide ,il absorbe les valeurs des descrptive et titre de ce
                                            formation
                                        </blockquote>
                                    </div>



                                    <br>
                                    <div class="progress">
                                        <div class="determinate" style="width: 100%"></div>
                                    </div>
                                    <div class="row">
                                        @if($blog->image == null)
                        <div class="col s6">
                            <div class="file-field input-field">
                                <div class="btn">
                                    <span>Image*</span>
                                    <input type="file" name="image">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text" required>
                                </div>
                            </div>
                        </div>
                        @else
                        <div class="col s2">
                            <img src="{{asset('back/images/blogs/'.$blog->image)}}" alt=""
                                class="responsive-img">
                        </div>
                        <div class="col s4">
                            <div class="file-field input-field">
                                <div class="btn">
                                    <span>Image</span>
                                    <input type="file" name="image">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path validate" type="text">
                                </div>
                            </div>
                        </div>
                        @endif

                        @if($blog->banner == null)
                                    <div class="col s6">
                                        <div class="file-field input-field">
                                            <div class="btn">
                                                <span>Banner</span>
                                                <input type="file" name="banner">
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    @else
                                    <div class="col s2">
                                        <img src="{{asset('back/images/blogs/'.$blog->banner)}}" alt=""
                                            class="responsive-img">
                                    </div>
                                    <div class="col s4">
                                        <div class="file-field input-field">
                                            <div class="btn">
                                                <span>Banner</span>
                                                <input type="file" name="banner">
                                            </div>
                                            <div class="file-path-wrapper">
                                                <input class="file-path validate" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    </div>

                                    @if ($blog->imageOG == null)
                                    <div class="file-field input-field">
                                        <div class="btn">
                                            <span>Facebook Image</span>
                                            <input type="file" name="ogImage">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text">
                                        </div>
                                    </div>
                                    @else
                                    <div class="">
                                        <div class="row valign-wrapper">
                                            <div class="col s2">
                                                <img src="{{asset('back/images/blogs/'.$blog->imageOG)}}" alt=""
                                                    class="responsive-img">
                                            </div>
                                            <div class="col s10">
                                                <div class="file-field input-field">
                                                    <div class="btn">
                                                        <span>Facebook Image</span>
                                                        <input type="file" name="ogImage">
                                                    </div>
                                                    <div class="file-path-wrapper">
                                                        <input class="file-path validate" type="text">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    @endif

                                    <div class="row">
                                        <div class="row">
                                            <div class="input-field col s12">
                                                <button class="btn cyan waves-effect waves-light right" type="submit"
                                                    name="action">Enregistrer
                                                    <i class="material-icons right">send</i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                                {{-- end --}}
                            </div>
                        </div>
                    </div>

                    {{-- end --}}
                </div>
            </div>
        </div>
    </div>
@endsection




@section('script')
<script>
    $(document).ready(function(){
		var keywordsDATAFR = [];
		var keywords = <?php echo json_encode($blog->keyWordsFR); ?>;
        $('.KeywordsFR').chips();
        // if keyword not null
		if (keywords != null) {
			var keywords = keywords.split(',');
			console.log(keywords);
			for(i=0; i < keywords.length; i++) {
				keywordsDATAFR.push({tag: keywords[i]});
			}
			$('.chips-initial-FR').chips({
				data: keywordsDATAFR,
				onChipAdd: (event, chip) => {
					var data = chip.innerHTML.substr(0, chip.innerHTML.indexOf("<i"));
					console.log(data);
					keywords.push(data);

					$('div.myTagsFR').html('<input type="hidden" name="KeywordsFR" value=\'' + keywords + '\' >');
				},
				onChipDelete:(event,chip)=>{
					var data = chip.innerHTML.substr(0, chip.innerHTML.indexOf("<i"));
					keywords = $(keywords).not([data]).get();
					$('div.myTagsFR').html('<input type="hidden" name="KeywordsFR" value=\'' + keywords + '\' >');
				},
			});
			$('div.myTagsFR').html('<input type="hidden" name="KeywordsFR" value=\'' + keywords + '\' >');
        }
        // if keywords null

		$('.chips-placeholder-FR').chips({
			placeholder: 'Keywords Francais',
			secondaryPlaceholder: '+Keyword',
			onChipAdd: (event, chip) => {
                var data = JSON.stringify(M.Chips.getInstance($('.KeywordsFR')).chipsData);
                var json = JSON.parse(data);
                var data = [];
                $(json).each(function (i, val) {
                    $.each(val, function (k, v) {
                        data.push(v);
                    });
                });
                console.log(data);
                keywordsDATAFR=data;

                $('div.myTagsFR').html('<input type="hidden" name="KeywordsFR" value=\'' + data + '\' >');
            },
            onChipDelete:(event,chip)=>{
				var data = chip.innerHTML.substr(0, chip.innerHTML.indexOf("<i"));
				keywordsDATAFR = $(keywordsDATAFR).not([data]).get();
				$('div.myTagsFR').html('<input type="hidden" name="KeywordsFR" value=\'' + keywordsDATAFR + '\' >');
			},
		});
	});
</script>

<script>
    $(document).ready(function(){
        var keywordsDATAENG = [];
        var keywordsENG = <?php echo json_encode($blog->keyWordsENG); ?>;
        $('.KeywordsENG').chips();
        // if keyword not null
		if (keywordsENG != null) {
			var keywordsENG = keywordsENG.split(',');
			console.log(keywordsENG);
			for(i=0; i < keywordsENG.length; i++) {
				keywordsDATAENG.push({tag: keywordsENG[i]});
			}
			$('.chips-initial-ENG').chips({
				data: keywordsDATAENG,
				onChipAdd: (event, chip) => {
					var data = chip.innerHTML.substr(0, chip.innerHTML.indexOf("<i"));
					console.log(data);
					keywordsENG.push(data);

					$('div.myTagsENG').html('<input type="hidden" name="KeywordsENG" value=\'' + keywordsENG + '\' >');
				},
				onChipDelete:(event,chip)=>{
					var data = chip.innerHTML.substr(0, chip.innerHTML.indexOf("<i"));
					keywordsENG = $(keywordsENG).not([data]).get();
					$('div.myTagsENG').html('<input type="hidden" name="KeywordsENG" value=\'' + keywordsENG + '\' >');
				},
			});
			$('div.myTagsENG').html('<input type="hidden" name="KeywordsENG" value=\'' + keywordsENG + '\' >');
        }
        // if keywords null
		$('.chips-placeholder-ENG').chips({
			placeholder: 'Keywords Englais',
			secondaryPlaceholder: '+Keyword',
			onChipAdd: (event, chip) => {
                var data = JSON.stringify(M.Chips.getInstance($('.KeywordsENG')).chipsData);
                var json = JSON.parse(data);
                var data = [];
                $(json).each(function (i, val) {
                    $.each(val, function (k, v) {
                        data.push(v);
                    });
                });
                console.log(data);
                keywordsDATAENG=data;

                $('div.myTagsENG').html('<input type="hidden" name="KeywordsENG" value=\'' + data + '\' >');
            },
            onChipDelete:(event,chip)=>{
				var data = chip.innerHTML.substr(0, chip.innerHTML.indexOf("<i"));
				keywordsDATAENG = $(keywordsDATAENG).not([data]).get();
				$('div.myTagsENG').html('<input type="hidden" name="KeywordsENG" value=\'' + keywordsDATAENG + '\' >');
			},
		});
	});
</script>
<script>
    $(document).ready(function(){
        $('.LangSwitch').on('click', function() {
            // var optionSelected = $("option:selected", this);
            var lang = $('> a', this).data('language');
            console.log(lang)
            if (lang == 'fr') {
                $(".FR").css("display", "block");
                $(".EN").css("display", "none");
            }
            if (lang == 'en') {
                $(".FR").css("display", "none");
                $(".EN").css("display", "block");
            }

        })
    });
</script>
@endsection

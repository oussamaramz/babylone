@extends('front.layout.layout')
@section('style')
@endsection
@section('content')
<?php
    if (Config::get('app.locale') == 'fr') {
    $adress= \App\Contact::where('id',2)->first();
    }else{
    $adress= \App\Contact::where('id',7)->first();
    }
?>
<section class="details-card">
    <div class="container">
        <div class="row">
            <h1 class="h-t" >{{__('msg.blog-title')}}</h1>
            @foreach ($blogs as $item)
            <div class="col-md-4">
                <div class="card-content">
                    <div class="card-img">
                    <a href="{{route('singleBlog',['alias'=>Config::get('app.locale') == 'fr' ? $item->aliasFR:$item->aliasENG])}}"> <img src="{{asset('back/images/blogs/'.$item->image)}}" alt=""></a>
                    <a href="{{route('singleBlog',['alias'=>Config::get('app.locale') == 'fr' ? $item->aliasFR:$item->aliasENG])}}"><span><h4>{{Config::get('app.locale') == 'fr' ? $item->labelFR:$item->labelENG}}</h4></span></a>
                    </div>
                    <div class="card-desc">
                        <a href=""><h3>{{Config::get('app.locale') == 'fr' ? $item->labelFR:$item->labelENG}}</h3></a>
                        <a href=""><p>{{Config::get('app.locale') == 'fr' ? $item->introFR:$item->introENG}}</p>
                        </a>
                        <div class="card-lire text-right">
                            <a href="{{route('singleBlog',['alias'=>Config::get('app.locale') == 'fr' ? $item->aliasFR:$item->aliasENG])}}" >{{__('msg.Lire')}}</a>
                        </div>

                    </div>
                </div>
            </div>
            @endforeach
            <span class="loadMore"></span>

            <button id="blog_accordion" class="accordion text-center">{{__('msg.Charge_plus')}}</button>
        </div>


    </div>
</section>
@endsection

@section('script')
<script type="text/javascript">
    var pageNumber = 2;

    $(document).ready(function() {
        $('button#blog_accordion').click(function() {
            $.ajax({
                type : 'GET',
                url: "{{route('nouveautes',session()->get('locale'))}}?page=" +pageNumber,
                success : function(data){
                    pageNumber +=1;
                        if(data.length == 0){
                        }else{
                            $('span.loadMore').append(data.html);
                        }
                },error: function(data){

                },
            })
        });
    });
</script>
@endsection

@extends('front.layout.layout')
@section('content')
<div role="main" class="main main-page">

    <div class="clearfix"></div>
    <div id="myCarousel" class="carousel  fade" data-ride="carousel" style="max-height: 90vh;">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active"
                style="background: url('{{ asset('assets/front/images/slider/slide1.png')}}') center no-repeat ; background-size: cover; ">
                <!-- <img src="images/slider2.jpg" alt="Los Angeles" style="width:100%;"> -->


                <div class="carousel-caption ">
                <p class="P-S">{{__('msg.slider_title_1')}}<br>{{__('msg.slider_title_1-1')}}</p>
                    <hr class="hr-slide" />
                    <div class="left">
                        <h2 id="Pr-titre"> <span class="f-s"> {{__('msg.slider_sub_title_1')}}</span> <br> {{__('msg.slider_sub_title_1-1')}}
                        </h2>
                        <p>{{__('msg.slider_sub_title_1-2')}}</p>
                        <a href="{{route('about',session()->get('locale'))}}" class="btn btn-outline btn-2 rounded-0 margin-top-10">{{__('msg.Decouvrir_Plus')}}</a>
                    </div>

                </div>
            </div>

            <div class="item"
                style="background: url('{{ asset('assets/front/images/slider/slider-2-banner.png')}}') center no-repeat ; background-size: cover; ">
                <!-- <img src="images/slider.jpg" alt="Chicago" style="width:100%;"> -->

                <div class="carousel-caption ">
                    <div class="left">

                        <div class="title-sub">
                            <h4>{{__('msg.slider_sub_title_2')}}</h4>
                        </div>
                        <h2>{{__('msg.slider_sub_title_2-1')}} <br> {{__('msg.slider_sub_title_2-2')}}</h2>
                        <a href="{{route('about',session()->get('locale'))}}" class="btn btn-outline btn-2 rounded-0 margin-top-10">{{__('msg.Decouvrir_Plus')}}</a>

                    </div>

                </div>
            </div>

            <div class="item"
                style="background: url('{{ asset('assets/front/images/slider/slider-1-banner.png')}}') center no-repeat ; background-size: cover;">
                <!-- <img src="images/slider3.jpg" alt="New york" style="width:100%;"> -->

                <div class="carousel-caption ">
                    <div class="left">
                        <div class="title-sub">
                            <h4>{{__('msg.slider_sub_title_2')}}</h4>
                        </div>
                        <h2>{{__('msg.slider_sub_title_3-1')}} <br> {{__('msg.slider_sub_title_3-2')}}</h2>
                        <a href="{{route('about',session()->get('locale'))}}" class="btn btn-outline btn-2 rounded-0 margin-top-10">{{__('msg.Decouvrir_Plus')}}</a>


                    </div>

                </div>
            </div>
        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <div class="help">
        <div class="container">
            <div class="content-inner">
                <div>
                    <div data-drupal-messages-fallback class="hidden"></div>

                </div>

            </div>
        </div>
    </div>

    <div class="fw-before-content area">
        <div>
            <div id="block-gavias-emon-gaviasblockbuiderhome1"
                class="margin-bottom-0 block block-gavias-blockbuilder block-gavias-blockbuilder-blockgavias-blockbuilder-block____1 no-title">


                <div class="content block-content">
                    <div class="gavias-blockbuilder-content">

                        <div class=" gbb-row secAbt" style="">
                            <div class="bb-inner default">
                                <div class="bb-container container">
                                    <div class="row">
                                        <div class="row-wrapper clearfix">
                                            <div class="gsc-column col-lg-5 col-md-7 col-sm-12 col-xs-12">
                                                <div class="column-inner  bg-size-cover">
                                                    <div class="column-content-inner">
                                                        <div class="animate" data-anim-type="fadeInUp">
                                                            <div class="widget gsc-image text-center ">
                                                                <div class="widget-content">

                                                                    <img src="{{ asset('assets/front/images/like.png')}}"
                                                                        alt="" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="gsc-column col-lg-6 col-md-5 col-sm-12 col-xs-12">
                                                <div class="column-inner  bg-size-cover">
                                                    <div class="column-content-inner">
                                                        <div class="widget gsc-heading  align-left style-1 text-dark ">

                                                            <h3 class="title"><span>{{__('msg.Qui-Sommes-Nous')}}</span>
                                                            </h3>

                                                            <div class="title-desc"><span>
                                                                    <p>{{__('msg.section1_p')}}</p>
                                                                </span></div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="widget gsc-icon-box left text-dark mb20">


                                                                    <div class="highlight-icon">
                                                                        <i class="fa fa-exchange"></i>
                                                                    </div>

                                                                    <div class="highlight_content">
                                                                        <h4 class="color-primary">
                                                                            {{__('msg.Communication')}} </h4>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="widget gsc-icon-box left text-dark mb20">


                                                                    <div class="highlight-icon"><i
                                                                            class="fa fa-link color-alt-1">&nbsp;</i>
                                                                    </div>

                                                                    <div class="highlight_content">
                                                                        <h4 class="color-alt-1"> {{__('msg.Cooperation')}}
                                                                        </h4>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <div class="widget gsc-icon-box left text-dark mb20">


                                                                    <div class="highlight-icon"><i
                                                                            class="fa fa-user color-alt-2">&nbsp;</i>
                                                                    </div>

                                                                    <div class="highlight_content">
                                                                        <h4 class="color-alt-2">
                                                                            {{__('msg.Responsabilisation')}} </h4>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="widget gsc-icon-box left text-dark mb20">


                                                                    <div class="highlight-icon"><i
                                                                            class="fa fa-male">&nbsp;</i>
                                                                    </div>

                                                                    <div class="highlight_content">
                                                                        <h4 class="color-primary"> {{__('msg.Autonomie')}}
                                                                        </h4>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>




                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=" gbb-row" style="background-color:#f5f5f5">
                            <div class="bb-inner default">
                                <div class="bb-container container">
                                    <div class="row">
                                        <div class="row-wrapper clearfix">
                                            <div class="gsc-column col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="column-inner  bg-size-cover">
                                                    <div class="column-content-inner">

                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="gsc-column col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <div class="column-inner  bg-size-cover">
                                                    <span class="box-title">
                                                        <h2 class="approche-title"><a class="color-primary">
                                                                {{__('msg.Notre_Mission')}} </a></h2>
                                                    </span>
                                                    <div class="column-content-inner">
                                                        <div class="widget gsc-hover-box gavias-metro-box ">
                                                            <div class="flipper">
                                                                <div class="front">
                                                                    <div class="gavias-metro-box-header">
                                                                        <a class="gavias-icon-boxed"><i
                                                                                class="fa fa-bullseye"></i></a>

                                                                    </div>
                                                                    <div>
                                                                        <p>{{__('msg.Notre_Mission_p')}}</p>
                                                                    </div>
                                                                </div>
                                                                <div class="back w-100 pt-30">
                                                                    <a href="{{route('about',session()->get('locale'))}}#mission" class="color-white" href="about.html">{{__('msg.Savoir_Plus')}} <i class="fa fa-arrow-right"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="gsc-column col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <div class="column-inner  bg-size-cover">
                                                    <span class="box-title">
                                                        <h2 class="approche-title"><a class="color-alt-1">
                                                                {{__('msg.Notre_Vision')}} </a></h2>
                                                    </span>
                                                    <div class="column-content-inner">
                                                        <div class="widget gsc-hover-box gavias-metro-box ">
                                                            <div class="flipper">
                                                                <div class="front">
                                                                    <div class="gavias-metro-box-header">
                                                                        <a class="gavias-icon-boxed"><i
                                                                                class="fa fa-eye color-alt-1"></i></a>

                                                                    </div>
                                                                    <div>
                                                                            <p>{{__('msg.Notre_Vision_p')}}
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                                <div class="back w-100 pt-30 bg-alt-1">
                                                                    <a href="{{route('about',session()->get('locale'))}}#vision" class="color-white" href="about.html">{{__('msg.Savoir_Plus')}}<i class="fa fa-arrow-right"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="gsc-column col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                                <div class="column-inner  bg-size-cover">
                                                    <span class="box-title">
                                                        <h2 class="approche-title"><a class="color-alt-2">
                                                                {{__('msg.Nos_Valeurs')}} </a></h2>
                                                    </span>
                                                    <div class="column-content-inner">
                                                        <div class="widget gsc-hover-box gavias-metro-box ">
                                                            <div class="flipper">
                                                                <div class="front">
                                                                    <div class="gavias-metro-box-header">
                                                                        <a class="gavias-icon-boxed"><i
                                                                                class="fa fa-balance-scale color-alt-2"></i></a>

                                                                    </div>
                                                                    <div>
                                                                        <p>{{__('msg.Nos_Valeurs_p')}} </p>
                                                                    </div>
                                                                </div>
                                                                <div class="back w-100 pt-30 bg-alt-2">
                                                                    <a href="{{route('about',session()->get('locale'))}}#Valeurs" class="color-white" href="about.html">{{__('msg.Savoir_Plus')}}<i class="fa fa-arrow-right"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class=" gbb-row prjPeda">
                            <div class="bb-inner default">
                                <div class="bb-container container">
                                    <div class="row">
                                        <div class="row-wrapper clearfix">
                                            <div class="gsc-column col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="widget gsc-heading  align-center style-1 text-dark ">
                                                    <h3 style="color: #6ebb4c;" class="title"><span>{{__('msg.Projet_Pedagogique')}}</span>
                                                    </h3>

                                                    <div class="title-desc"><span style="font-size: 19px;">{{__('msg.Projet_Pedagogique_title')}}</span>
                                                    </div>
                                                </div>
                                                <div class="column-inner  bg-size-cover">
                                                    <div class="column-content-inner">

                                                        <div class="clearfix"></div>
                                                        <div class="gsc-column col-lg-6 col-md-4 col-sm-12 col-xs-12">
                                                            <div class="column-inner  bg-size-cover">
                                                                <div class="column-content-inner">

                                                                    <!-- <div
                                                                        class="gsc-hover-background clearfix ">
                                                                        <div class="front"
                                                                            style="background: #f5f5f5">
                                                                            <div class="icon"><i
                                                                                    class="fa fa-users"></i>
                                                                            </div>

                                                                            <h2>NOUS ASSOCIONS LES FAMILLES
                                                                                ET LES PARENTS</h2>

                                                                        </div>

                                                                        <div style="background: url('#sites/default/files/gbb-uploads/5T4vhz-about-1.jpg') no-repeat center center"
                                                                            class="back">
                                                                            <div class="content">
                                                                                <div class="content-text">
                                                                                    Constructor explains how
                                                                                    you can enjoy high end
                                                                                    flooring trends like
                                                                                    textured wood and
                                                                                    realistic stones with
                                                                                    new laminate flooring.
                                                                                </div>
                                                                                <div class="readmore"><a
                                                                                        class="btn-theme btn btn-sm"
                                                                                        href="#">Read
                                                                                        more</a></div>
                                                                            </div>
                                                                        </div>
                                                                    </div> -->
                                                                    <!-- <div
                                                                        class="gsc-hover-background clearfix ">
                                                                        <div class="front"
                                                                            style="background: #f5f5f5">
                                                                            <div class="icon"><i
                                                                                    class="fa fa-paint-brush"></i>
                                                                            </div>

                                                                            <h2>NOUS FAVORISONS
                                                                                L’ÉPANOUISSEMENT</h2>

                                                                        </div>

                                                                        <div style="background: url('#sites/default/files/gbb-uploads/5T4vhz-about-1.jpg') no-repeat center center"
                                                                            class="back">
                                                                            <div class="content">
                                                                                <div class="content-text">
                                                                                    Constructor explains how
                                                                                    you can enjoy high end
                                                                                    flooring trends like
                                                                                    textured wood and
                                                                                    realistic stones with
                                                                                    new laminate flooring.
                                                                                </div>
                                                                                <div class="readmore"><a
                                                                                        class="btn-theme btn btn-sm"
                                                                                        href="#">Read
                                                                                        more</a></div>
                                                                            </div>
                                                                        </div>
                                                                    </div> -->
                                                                    <div class="widget gsc-team team-vertical">
                                                                        <div class="widget-content text-center">
                                                                            <div class="team-header text-center">
                                                                                <img alt="John joe"
                                                                                    src="{{ asset('assets/front/images/parent.jpg')}}"
                                                                                    class="img-responsive">
                                                                            </div>
                                                                            <div class="team-body text-center">
                                                                                <div class="team-body-content">
                                                                                    <h3 class="team-name">
                                                                                        {{__('msg.Projet_Pedagogique_img1')}}</h3>
                                                                                </div>
                                                                                <!-- <p class="team-info">Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.</p>      -->

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="widget gsc-team team-vertical">
                                                                        <div class="widget-content text-center">
                                                                            <div class="team-header text-center">
                                                                                <img alt="John joe"
                                                                                    src="{{ asset('assets/front/images/epanouissement.jpg')}}"
                                                                                    class="img-responsive">
                                                                            </div>
                                                                            <div class="team-body text-center">
                                                                                <div class="team-body-content">
                                                                                    <h3 class="team-name">
                                                                                        {{__('msg.Projet_Pedagogique_img2')}}
                                                                                    </h3>
                                                                                </div>
                                                                                <!-- <p class="team-info">Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.</p>      -->

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="gsc-column col-lg-6 col-md-4 col-sm-12 col-xs-12">
                                                            <div class="column-inner  bg-size-cover">
                                                                <div class="column-content-inner">
                                                                    <div class="widget gsc-team team-vertical">
                                                                        <div class="widget-content text-center">
                                                                            <div class="team-header text-center">
                                                                                <img alt="John joe"
                                                                                    src="{{ asset('assets/front/images/communication.jpg')}}"
                                                                                    class="img-responsive">
                                                                            </div>
                                                                            <div class="team-body text-center">
                                                                                <div class="team-body-content">
                                                                                    <h3 class="team-name">
                                                                                        {{__('msg.Projet_Pedagogique_img3')}}
                                                                                    </h3>
                                                                                </div>
                                                                                <!-- <p class="team-info">Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.</p>      -->

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="widget gsc-team team-vertical">
                                                                        <div class="widget-content text-center">
                                                                            <div class="team-header text-center">
                                                                                <img alt="John joe"
                                                                                    src="{{ asset('assets/front/images/innovation.jpg')}}"
                                                                                    class="img-responsive">
                                                                            </div>
                                                                            <div class="team-body text-center">
                                                                                <div class="team-body-content">
                                                                                    <h3 class="team-name">
                                                                                        {{__('msg.Projet_Pedagogique_img4')}}
                                                                                    </h3>
                                                                                </div>
                                                                                <!-- <p class="team-info">Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.</p>      -->

                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="gsc-column col-lg-6 col-md-4 col-sm-12 col-xs-12">
                                                            <div class="column-inner  bg-size-cover">
                                                                <div class="column-content-inner">

                                                                    <!-- <div
                                                                        class="gsc-hover-background clearfix ">
                                                                        <div class="front"
                                                                            style="background: #f5f5f5">
                                                                            <div class="icon"><i
                                                                                    class="fa fa-users"></i>
                                                                            </div>

                                                                            <h2>NOUS ASSOCIONS LES FAMILLES
                                                                                ET LES PARENTS</h2>

                                                                        </div>

                                                                        <div style="background: url('#sites/default/files/gbb-uploads/5T4vhz-about-1.jpg') no-repeat center center"
                                                                            class="back">
                                                                            <div class="content">
                                                                                <div class="content-text">
                                                                                    Constructor explains how
                                                                                    you can enjoy high end
                                                                                    flooring trends like
                                                                                    textured wood and
                                                                                    realistic stones with
                                                                                    new laminate flooring.
                                                                                </div>
                                                                                <div class="readmore"><a
                                                                                        class="btn-theme btn btn-sm"
                                                                                        href="#">Read
                                                                                        more</a></div>
                                                                            </div>
                                                                        </div>
                                                                    </div> -->
                                                                    <!-- <div
                                                                        class="gsc-hover-background clearfix ">
                                                                        <div class="front"
                                                                            style="background: #f5f5f5">
                                                                            <div class="icon"><i
                                                                                    class="fa fa-paint-brush"></i>
                                                                            </div>

                                                                            <h2>NOUS FAVORISONS
                                                                                L’ÉPANOUISSEMENT</h2>

                                                                        </div>

                                                                        <div style="background: url('#sites/default/files/gbb-uploads/5T4vhz-about-1.jpg') no-repeat center center"
                                                                            class="back">
                                                                            <div class="content">
                                                                                <div class="content-text">
                                                                                    Constructor explains how
                                                                                    you can enjoy high end
                                                                                    flooring trends like
                                                                                    textured wood and
                                                                                    realistic stones with
                                                                                    new laminate flooring.
                                                                                </div>
                                                                                <div class="readmore"><a
                                                                                        class="btn-theme btn btn-sm"
                                                                                        href="#">Read
                                                                                        more</a></div>
                                                                            </div>
                                                                        </div>
                                                                    </div> -->
                                                                    <div class="widget gsc-team team-vertical">
                                                                        <div class="widget-content text-center">
                                                                            <div class="team-header text-center">
                                                                                <img alt="John joe"
                                                                                    src="{{ asset('assets/front/images/citoy.jpg')}}"
                                                                                    class="img-responsive">
                                                                            </div>
                                                                            <div class="team-body text-center">
                                                                                <div class="team-body-content">
                                                                                    <h3 class="team-name">
                                                                                        {{__('msg.Projet_Pedagogique_img5')}}
                                                                                    </h3>
                                                                                </div>
                                                                                <!-- <p class="team-info">Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.</p>      -->

                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="gsc-column col-lg-6 col-md-4 col-sm-12 col-xs-12">
                                                            <div class="column-inner  bg-size-cover">
                                                                <div class="column-content-inner">
                                                                    <div class="widget gsc-team team-vertical">
                                                                        <div class="widget-content text-center">
                                                                            <div class="team-header text-center">
                                                                                <img alt="John joe"
                                                                                    src="{{ asset('assets/front/images/critique.jpg')}}"
                                                                                    class="img-responsive">
                                                                            </div>
                                                                            <div class="team-body text-center">
                                                                                <div class="team-body-content">
                                                                                    <h3 class="team-name">
                                                                                        {{__('msg.Projet_Pedagogique_img6')}}

                                                                                    </h3>
                                                                                </div>
                                                                                <!-- <p class="team-info">Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit.</p>      -->

                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>



                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=" gbb-row secEns" style=" background:#f5f5f5;">
                            <div class="bb-inner default">
                                <div class="bb-container container">
                                    <div class="row">
                                        <div class="row-wrapper clearfix">
                                            <div class="gsc-column col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="column-inner  bg-size-cover">
                                                    <div class="column-content-inner">
                                                        <div
                                                            class="widget gsc-heading  align-center style-1 text-dark ">
                                                            <h3 class="title"><span>{{__('msg.Enseignement_Education')}}</span>
                                                            </h3>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                        <div class="widget gsc-box-info  content-align-left"
                                                            style="min-height: 360px">
                                                            <div class="clearfix">
                                                                <div class="image"
                                                                    style="background-image:url('{{ asset('assets/front/images/maternelle.jpg')}}')">
                                                                </div>
                                                                <div class="content text-dark">
                                                                    <div class="content-bg"></div>
                                                                    <div class="content-inner">
                                                                        <div class="title">
                                                                            <h2 style="text-align: left;">
                                                                                {{__('msg.maternelle')}}</h2>
                                                                        </div>


                                                                        <div class="desc">{{__('msg.section_p1')}}</div>


                                                                        <div class="readmore"><a
                                                                                class="btn-theme btn btn-sm"
                                                                                href="{{route('enseignementEducation',session()->get('locale'))}}#maternelle.html">{{__('msg.Savoir_Plus')}}</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="widget gsc-box-info  content-align-right"
                                                            style="min-height: 360px">
                                                            <div class="clearfix">
                                                                <div class="image"
                                                                    style="background-image:url('{{ asset('assets/front/images/primaire.jpg')}}')">
                                                                </div>
                                                                <div class="content text-dark">
                                                                    <div class="content-bg"></div>
                                                                    <div class="content-inner sec-2">
                                                                        <div class="title">
                                                                            <h2 style="text-align: left;">
                                                                                {{__('msg.primaire')}} </h2>
                                                                        </div>


                                                                        <div class="desc">{{__('msg.section_p2')}}</div>


                                                                        <div class="readmore"><a
                                                                                class="btn-theme btn btn-sm"
                                                                                href="{{route('enseignementEducation',session()->get('locale'))}}#primaire">{{__('msg.Savoir_Plus')}}</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="widget gsc-box-info  content-align-left"
                                                            style="min-height: 360px">
                                                            <div class="clearfix">
                                                                <div class="image"
                                                                    style="background-image:url('{{ asset('assets/front/images/college.jpg')}}')">
                                                                </div>
                                                                <div class="content text-dark">
                                                                    <div class="content-bg"></div>
                                                                    <div class="content-inner sec-3">
                                                                        <div class="title">
                                                                            <h2 style="text-align: left;">
                                                                                {{__('msg.college')}} </h2>
                                                                        </div>


                                                                        <div class="desc">{{__('msg.section_p3')}}</div>


                                                                        <div class="readmore"><a
                                                                                class="btn-theme btn btn-sm"
                                                                                href="{{route('enseignementEducation',session()->get('locale'))}}#college">{{__('msg.Savoir_Plus')}}</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="widget gsc-box-info  content-align-right"
                                                            style="min-height: 360px">
                                                            <div class="clearfix">
                                                                <div class="image"
                                                                    style="background-image:url('{{ asset('assets/front/images/lycee.jpg')}}')">
                                                                </div>
                                                                <div class="content text-dark">
                                                                    <div class="content-bg"></div>
                                                                    <div class="content-inner">
                                                                        <div class="title">
                                                                            <h2 style="text-align: left;">
                                                                                {{__('msg.lycee')}} </h2>
                                                                        </div>


                                                                        <div class="desc">{{__('msg.section_p4')}} </div>


                                                                        <div class="readmore"><a
                                                                                class="btn-theme btn btn-sm"
                                                                                href="{{route('enseignementEducation',session()->get('locale'))}}#centre">{{__('msg.Savoir_Plus')}}</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class=" gbb-row" style="background-color:#144fa3">
                            <div class="bb-inner default">
                                <div class="bb-container container-fluid">
                                    <div class="row">
                                        <div class="row-wrapper clearfix">
                                            <div class="gsc-column col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="column-inner  bg-size-cover">
                                                    <div class="column-content-inner">
                                                        <div
                                                            class="widget gsc-call-to-action  button-bottom text-light">
                                                            <div class="content-inner clearfix">
                                                                <div class="content">
                                                                    <h3 class="title"><span> {{__('msg.section5_titre')}}</span></h3>
                                                                    {{__('msg.section5_sub_titre')}}
                                                                </div>
                                                                <div class="button-action">
                                                                    <a href="inscription.html" class="btn-2 btn btn-md">
                                                                        <span>{{__('msg.section5_btn')}}</span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>

    <div class="clearfix"></div>
    <div class="clearfix"></div>
    <div id="content" class="content content-full">
        <div class="container">
            <div class="content-main-inner">
                <div class="row">



                    <div id="page-main-content" class="main-content col-md-12 col-xs-12">

                        <div class="main-content-inner">


                            <div class="content-main">
                                <div>


                                    <article data-history-node-id="35" role="article" typeof="schema:WebPage"
                                        class="node node--type-page node--view-mode-full clearfix">
                                        <header>


                                        </header>
                                        <div class="node__content clearfix">

                                        </div>
                                    </article>

                                </div>

                            </div>

                        </div>

                    </div>

                    <!-- Sidebar Left -->
                    <!-- End Sidebar Left -->

                    <!-- Sidebar Right -->
                    <!-- End Sidebar Right -->

                </div>
            </div>
        </div>
    </div>




</div>
@endsection

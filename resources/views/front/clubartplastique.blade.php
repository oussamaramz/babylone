@extends('front.layout.layout')
@section('content')
<div role="main" class="main main-page">

    <div class="clearfix"></div>
    <div class="slideshow_content area">

    </div>

    <div class="help">
        <div class="container">
            <div class="content-inner">
                <div>
                    <div data-drupal-messages-fallback="" class="hidden"></div>

                </div>

            </div>
        </div>
    </div>


    <div class="clearfix"></div>
    <div class="clearfix"></div>
    <div id="content" class="content content-full club">
        <div class="container-fluid p-0">
            <div class="content-main-inner">

                <div class="row m-4">
                    <div class="col-12 text-center">

                    </div>

                </div>
                <div class="row">

                    <div id="page-main-content" class="main-content col-xs-12 col-md-12 sb-r ">

                        <div class="main-content-inner">


                            <div class="content-main">
                                <div>
                                    <div id="block-gavias-emon-content"
                                        class="block block-system block-system-main-block no-title">


                                        <div class="content block-content">





                                            <div class=" gbb-row" style="">
                                                <div class="bb-inner default">
                                                    <div class="bb-container container">
                                                        <div class="row">
                                                            <div class="row-wrapper clearfix">
                                                                <section class="club-theatre pad-tb">
                                                                    <div class="container"
                                                                        style="padding-bottom: 70px;">
                                                                        <h2 id="theatre">{{__('msg.plastiqueTitre')}}
                                                                        </h2>
                                                                        <div class="row">
                                                                            <div class="col-lg-6 v-center">
                                                                                <div class="h-i">
                                                                                    <img src="{{asset('assets/front/images/art-plastique.png')}}"
                                                                                        alt="lecture" />
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-6">
                                                                                <div class="common-heading text-l">


                                                                                    <div style=" font-size: 20px" ;
                                                                                        class="column-content-inner">

                                                                                        <p style="font-size: 20px;">
                                                                                           {!! __('msg.plastique_text') !!} </p>

                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </section>

                                                                <div
                                                                    class="gsc-column col-lg-12 col-md-12 col-sm-12 col-xs-12 margin-top-10">
                                                                    <div
                                                                        class="column-inner  bg-size-cover margin-top-10">
                                                                        <div class="column-content-inner margin-top-10">

                                                                            <h2 class="m-20" id="h-gallerie">{{__('msg.gallerie')}}
                                                                            </h2>
                                                                            <div>
                                                                                <div
                                                                                    class="gsc-column col-4 col-md-4 col-sm-12 col-xs-12">
                                                                                    <img src="{{asset('assets/front/images/art-plastique.png')}}"
                                                                                        class="img-fluid"
                                                                                        style="max-height: 100vh;">
                                                                                    <div id="robo" class=" text-center">
                                                                                        <p>{{__('msg.plastique_img1')}}</p>
                                                                                    </div>

                                                                                </div>
                                                                                <div
                                                                                    class="gsc-column col-4 col-md-4 col-sm-12 col-xs-12">
                                                                                    <img src="{{asset('assets/front/images/art-plastique.png')}}"
                                                                                        class="img-fluid"
                                                                                        style="max-height: 100vh;">
                                                                                    <div class="text-center" id="robo">
                                                                                        <p>{{__('msg.plastique_img2')}}
                                                                                        </p>
                                                                                    </div>
                                                                                </div>
                                                                                <div
                                                                                    class="gsc-column col-4 col-md-4 col-sm-12 col-xs-12">
                                                                                    <img src="{{asset('assets/front/images/art-plastique.png')}}"
                                                                                        class="img-fluid"
                                                                                        style="max-height: 100vh;">
                                                                                    <div id="robo" class=" text-center">
                                                                                        <p>{{__('msg.plastique_img3')}}</p>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>


                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>




</div>
@endsection

<!DOCTYPE html>
<html lang="{{ Config::get('app.locale') }}" dir="ltr">
    <?php

    if (Config::get('app.locale') == 'fr') {
    $adress= \App\Contact::where('id',2)->first();
    }else{
    $adress= \App\Contact::where('id',7)->first();
    }

    $email= \App\Contact::where('id',1)->first();
    $tel= \App\Contact::where('id',3)->first();
    $facebook= \App\Contact::where('id',4)->first();
    $instagram= \App\Contact::where('id',5)->first();
    $youtube= \App\Contact::where('id',6)->first();
    ?>
<head>
    @include('front.layout.head')
</head>

<body class="layout-no-sidebars not-preloader">

    <a href="#main-content" class="visually-hidden focusable">
        Skip to main content
    </a>

    <div class="dialog-off-canvas-main-canvas" data-off-canvas-main-canvas>
        <div class="body-page">
            @include('front.layout.menu')

            @yield('content')

            @include('front.layout.footer')
        </div>

    </div>


    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script src="https://use.fontawesome.com/f55dd9372e.js"></script>
<script src="{{ asset('assets/front/js/addons.js')}}"></script>
<script src="{{ asset('assets/front/js/custom.js')}}"></script>
@yield('script')

</body>

</html>

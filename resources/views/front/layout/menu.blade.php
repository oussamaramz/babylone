<header id="header" class="header-v3 mt-0">
    <nav class="header-top d-sm-none d-md-none">
        <div class="container-fluid">
            <div class="header-main-inner p-relative">
                <div class="row text-center">

                    <div class="col-md-3 col-sm-12 text-left mt-2">
                        <a class="badge bg-primary" href="{{route('enseignementEducation',session()->get('locale'))}}#maternelle">{{ __('msg.maternelle') }}</a>
                        <a class="badge bg-alt-1" href="{{route('enseignementEducation',session()->get('locale'))}}#primaire">{{ __('msg.primaire') }}</a>

                        <a class="badge bg-primary" href="{{route('enseignementEducation',session()->get('locale'))}}#college">{{ __('msg.college') }}</a>
                        <a class="badge bg-alt-1" href="{{route('enseignementEducation',session()->get('locale'))}}#centre">{{ __('msg.lycee') }}</a>


                    </div>

                    <div id="nav2" class="  col-md-5 col-sm-12 mt-2 text-left">

                        <a href="#" class="my-auto "> {{$tel->value}}</a>
                        <a href="#" class="my-auto-n "> {{$adress->value}}</a>

                        <a href="#" class="my-auto-n"> {{$email->value}}</a>




                    </div>
                    <div id="nav1" class=" col-md-4 col-sm-12 mt-1 text-sm-center text-right" style="
                     margin-top: 1.5rem;">
                        <a href="{{$facebook->value}}"><img class="social-icons" src="{{ asset('assets/front/images/facebook.png')}}"
                                alt=""></i></a>
                        <a href="{{$instagram->value}}"> <img class="social-icons" src="{{ asset('assets/front/images/instagram.jpg')}}"
                                alt=""> </a>
                        <a href="{{$youtube->value}}"><img class="social-icons" src="{{ asset('assets/front/images/youtube.png')}}"
                                alt=""></a>
                        <a id="btn-nav" href="{{route('contact',session()->get('locale'))}}"
                            class="btn-white btn py-1 ml-2 mb-1">{{__('msg.NousContactez')}}</a>
                        <a href="{{ route('languageSwitch','fr') }}" class=" ml-0"> <img class="nav3"
                                src="{{ asset('assets/front/images/france.png')}}"></a>
                        <a href="{{ route('languageSwitch', 'en') }}" class=" ml-0" href=""><img class="nav3 opa-6"
                                src="{{ asset('assets/front/images/united-kingdom.png')}}"></a>

                    </div>






                </div>
            </div>
        </div>
    </nav>



    <div class="header-main gv-sticky-menu">
        <div class="container-fluid">
            <div class="header-main-inner p-relative">
                <div class="row">
                    <div class="col-md-2 col-xs-6 branding p-0 ">
                        <div>
                            <div id="block-gavias-emon-sitebranding"
                                class="margin-top-10 clearfix site-branding block block-system block-system-branding-block no-title">
                                <a href="{{route('home',session()->get('locale'))}}" title="Home" rel="home"
                                    class="site-branding-logo padding-top-20">
                                    <img src="{{ asset('assets/front/images/new-logo-babylone.svg')}}" alt="Home"
                                        class="logoMObile" />
                                </a>


                            </div>

                        </div>

                    </div>

                    <div class="col-md-10 col-xs-6 p-static">
                        <div class="main-menu">
                            <div class="navigation area-main-menu">
                                <div class="area-inner">
                                    <div>
                                        <nav role="navigation" aria-labelledby="block-gavias-emon-mainnavigation-menu"
                                            id="block-gavias-emon-mainnavigation"
                                            class="block block-menu navigation menu--main">

                                            <h2 class="visually-hidden" id="block-gavias-emon-mainnavigation-menu">Main
                                                navigation
                                            </h2>


                                            <div class="content">


                                                <div class="gva-navigation">

                                                    <ul class="clearfix gva_menu gva_menu_main">

                                                        {{-- <li
                                                            class="menu-item menu-item--expanded menu-item--active-trail {{$navkey=='home' ? 'active':''}}">
                                                            <a href="{{route('home',session()->get('locale'))}}"> {{ __('msg.accueil') }} </a>
                                                        </li> --}}

                                                        <li
                                                            class="menu-item menu-item--expanded menu-item--active-trail a {{$navkey=='home' ? 'active':''}} {{$navkey=='about' ? 'active':''}}">
                                                            <a href="{{route('home',session()->get('locale'))}}">
                                                                {{ __('msg.accueil') }} <span
                                                                    class="icaret nav-plus fa fa-angle-down"></span></a>
                                                            <ul class="menu sub-menu">

                                                                <li class="menu-item menu-item--active-trail ">
                                                                    <a
                                                                        href="{{route('about',session()->get('locale'))}}">
                                                                        {{ __('msg.babylone_Academy') }} </a>
                                                                    <a
                                                                        href="{{route('about',session()->get('locale'))}}#mission">
                                                                        {{ __('msg.Mission') }}</a>
                                                                    <a
                                                                        href="{{route('about',session()->get('locale'))}}#vision">
                                                                        {{ __('msg.Vision') }}
                                                                    </a>
                                                                    <a href="{{route('about',session()->get('locale'))}}#Valeurs">
                                                                        {{ __('msg.Valeurs') }}
                                                                    </a>
                                                                    <a href="{{route('about',session()->get('locale'))}}#Projet_Pedagogique">
                                                                        {{ __('msg.Projet_Pedagogique') }} </a>

                                                                </li>
                                                            </ul>
                                                        </li>


                                                        <li
                                                            class="menu-item menu-item--expanded menu-item--active-trail a {{$navkey=='about' ? 'active':''}}">
                                                            <a href="{{route('about',session()->get('locale'))}}"> {{ __('msg.babylone_Academy') }} </a>
                                                        </li>

                                                        <li
                                                            class="menu-item menu-item--expanded menu-item--active-trail a {{$navkey=='enseignementEducation' ? 'active':''}}">
                                                            <a href="{{route('enseignementEducation',session()->get('locale'))}}">
                                                                {{ __('msg.enseignement') }} <span
                                                                    class="icaret nav-plus fa fa-angle-down"></span></a>
                                                            <ul class="menu sub-menu">

                                                                <li class="menu-item menu-item--active-trail ">
                                                                    <a
                                                                        href="{{route('enseignementEducation',session()->get('locale'))}}#maternelle">
                                                                        {{ __('msg.maternelle') }} </a>
                                                                    <a
                                                                        href="{{route('enseignementEducation',session()->get('locale'))}}#primaire">
                                                                        {{ __('msg.primaire') }}</a>
                                                                    <a
                                                                        href="{{route('enseignementEducation',session()->get('locale'))}}#college">
                                                                        {{ __('msg.college') }}
                                                                    </a>
                                                                    <a href="{{route('enseignementEducation',session()->get('locale'))}}#lycee">
                                                                        {{ __('msg.lycee') }}
                                                                    </a>
                                                                    <a href="{{route('enseignementEducation',session()->get('locale'))}}#centre">
                                                                        {{ __('msg.centre_orientation') }} </a>

                                                                </li>
                                                            </ul>
                                                        </li>

                                                        <li
                                                            class="menu-item menu-item--expanded menu-item--active-trail a {{$navkey=='epanouissement' ? 'active':''}}">
                                                            <a href="#">{{ __('msg.epanouissement') }}<span
                                                                    class="icaret nav-plus fa fa-angle-down"></span></a>
                                                            <ul class="menu sub-menu">

                                                                <li class="menu-item menu-item--active-trail ">
                                                                    <a href="{{route('clubtheatre',session()->get('locale'))}}">{{ __('msg.activite_theatre') }}

                                                                    </a>
                                                                    <a href="{{route('clubcoding',session()->get('locale'))}}">{{ __('msg.activite_coding__robotique') }}</a>
                                                                    <a href="{{route('clublecture',session()->get('locale'))}}">{{ __('msg.activite_lecture') }}</a>
                                                                    <a href="{{route('clubartplastique',session()->get('locale'))}}">{{ __('msg.activite_arts_plastiques') }}</a>
                                                                    <a href="{{route('clubsport',session()->get('locale'))}}">{{ __('msg.ctivite_sport') }}</a>


                                                                </li>
                                                            </ul>
                                                        </li>

                                                        <li
                                                            class="menu-item menu-item--expanded menu-item--active-trail a {{$navkey=='nouveautes' ? 'active':''}}">
                                                    <a href="{{route('nouveautes',session()->get('locale'))}}"> {{__('msg.nouveautes')}} </a>
                                                        </li>

                                                        <li
                                                            class="menu-item menu-item--expanded menu-item--active-trail a {{$navkey=='gallerie' ? 'active':''}}">
                                                    <a href="{{route('gallerie',session()->get('locale'))}}"> {{__('msg.gallerie')}} </a>
                                                        </li>

                                                        <li
                                                            class="menu-item menu-item--expanded menu-item--active-trail a {{$navkey=='s_identifier' ? 'active':''}}">
                                                            <a href="{{route('s_identifier',session()->get('locale'))}}"> {{__('msg.acces_parent')}} </a>
                                                        </li>

                                                        <li
                                                            class="menu-item menu-item--expanded menu-item--active-trail a {{$navkey=='inscription' ? 'active':''}}">
                                                            <a href="{{route('inscription',session()->get('locale'))}}"
                                                                class="btn btn-theme">{{__('msg.inscription')}}</a>
                                                        </li>
                                                    </ul>
                                                </div>

                                                <div id="menu-bar" class="menu-bar">
                                                    <span class="one"></span>
                                                    <span class="two"></span>
                                                    <span class="three"></span>
                                                </div>
                                                <div id="menu-bar" class="menu-bar langMobile">
                                                    <a href="{{ route('languageSwitch','fr') }}" class=" ml-0"> <img class="nav3"
                                                        src="{{ asset('assets/front/images/france.png')}}"></a>
                                                </div>

                                                <div id="menu-bar" class="menu-bar langMobile">
                                                    <a href="{{ route('languageSwitch', 'en') }}" class=" ml-0" href=""><img class="nav3 opa-6"
                                                        src="{{ asset('assets/front/images/united-kingdom.png')}}"></a>
                                                </div>



                                                
                                                
                                            </div>
                                        </nav>

                                    </div>



                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

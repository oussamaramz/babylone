<meta charset="utf-8" />

<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="shortcut icon" href="{{ asset('assets/front/images/favicon.png')}}" />


<title>Babylone Academy : groupe scolaire babylone academy</title>

<link rel="stylesheet" media="all" href="{{ asset('assets/front/css/custom.css')}}" />
<link rel="stylesheet" media="all" href="{{ asset('assets/front/css/style.css')}}" />
<link rel="stylesheet" media="all" href="{{ asset('assets/front/css/addons.css')}}" />
<link rel="stylesheet" media="all" href="{{ asset('assets/front/css/fontawesome.css')}}" />
<link rel="stylesheet" media="all" href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" />
<link rel="stylesheet" media="all" href="https://fonts.googleapis.com/css?family=Montserrat:400,700" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" />

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
@yield('style')

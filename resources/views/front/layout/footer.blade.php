<footer id="footer" class="footer">

    <div class="footer-center text-dark">
        <div class="container">
            <div class="row">
                <div class="footer-4col col-lg-4 col-md-3 col-md-6 col-xs-12 column">
                    <div>
                        <div id="block-gavias-emon-introfooter"
                            class="block block-block-content block-block-contentd1d0770c-bcf2-4c17-b000-d10dd2288acd">

                            <img src="{{ asset('assets/front/images/logo.png')}}" alt="">

                            <div class="content block-content">

                                <div
                                    class="field field--name-body field--type-text-with-summary field--label-hidden field__item">
                                    <p>{{__('msg.footre_about')}}</p>
                                    <a class="btn btn-2 color-white float-right" href="about.html">{{__('msg.Savoir_Plus')}}</a>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>



                <div class="footer-4col col-lg-4 col-md-3 col-md-6 col-xs-12 column">
                    <div>
                        <div id="block-gavias-emon-linkfooter"
                            class="block block-block-content block-block-contente03afdfc-b255-481b-9605-5a0e153ccacf">

                            <h2 class="block-title"><span>{{__('msg.Liens_Utiles')}}</span></h2>

                            <div class="content block-content">

                                <div
                                    class="field field--name-body field--type-text-with-summary field--label-hidden field__item">
                                    <ul class="menu">
                                        <li><a href="{{route('home',session()->get('locale'))}}">{{__('msg.accueil')}}</a></li>
                                        <li><a href="{{route('about',session()->get('locale'))}}s">{{__('msg.Qui-Sommes-Nous')}}</a></li>
                                        <li><a href="{{route('enseignementEducation',session()->get('locale'))}}">{{__('msg.Enseignement_Education')}}</a></li>
                                        <li><a href="{{route('gallerie',session()->get('locale'))}}"> {{__('msg.gallerie')}}</a></li>
                                        <li><a href="{{route('contact',session()->get('locale'))}}"> contact</a></li>
                                        <li><a href="{{route('inscription',session()->get('locale'))}}"> {{__('msg.section5_btn')}}</a></li>
                                    </ul>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>

                <div class="footer-4col col-lg-4 col-md-3 col-md-6 col-xs-12 column">
                    <div>
                        <div id="block-gavias-emon-contactinfo"
                            class="block block-block-content block-block-content0c1ecc6e-1d39-4ec0-a502-2c557762945e">

                            <h2 class="block-title"><span>{{__('msg.Information_contact')}}</span></h2>

                            <div class="content block-content">

                                <div
                                    class="field field--name-body field--type-text-with-summary field--label-hidden field__item">
                                    <div class="contact-info">
                                        <ul class="contact-info">
                                            <li><span><i class="fa fa-home"></i> {{$adress->value}}
                                                </span> </li>
                                            <li><span><a href="tel:{{$tel->value}}"><i class="fa fa-mobile-phone"></i>
                                                {{$tel->value}}</a></span></li>
                                            <li><a href="mailto:{{$email->value}}"><i class="fa fa-envelope-o"></i>
                                                {{$email->value}}</a>
                                            </li>
                                            <li class="socials"><a href="{{$facebook->value}}"><i class="fa fa-facebook-square"></i></a>
                                                <a href="{{$instagram->value}}"><i class="fa fa-instagram"></i></a>

                                                <a href="{{$youtube->value}}"><i class="fa fa-youtube-square"></i></a>

                                            </li>
                                    </div>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>


    <div class="copyright">
        <div class="container">
            <div class="copyright-inner">
                <div>
                    <div id="block-gavias-emon-copyright"
                        class="block block-block-content block-block-content6a74724d-d485-4216-a560-962e52758fde no-title">


                        <div class="content block-content">

                            <div
                                class="field field--name-body field--type-text-with-summary field--label-hidden field__item">
                                <div class="text-center"> {{__('msg.developpar')}} <a href="http://blinkagency.ma">Blink
                                        Agency</a>
                                </div>
                                <div class="text-center">
                                    {{__('msg.Copyright')}}
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

</footer>

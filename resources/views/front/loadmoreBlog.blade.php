@foreach ($blogs as $item)
<div class="col-md-4">
    <div class="card-content">
        <div class="card-img">
        <a href="{{route('singleBlog',['alias'=>Config::get('app.locale') == 'fr' ? $item->aliasFR:$item->aliasENG])}}"> <img src="{{asset('back/images/blogs/'.$item->image)}}" alt=""></a>
        <a href="{{route('singleBlog',['alias'=>Config::get('app.locale') == 'fr' ? $item->aliasFR:$item->aliasENG])}}"><span><h4>{{Config::get('app.locale') == 'fr' ? $item->labelFR:$item->labelENG}}</h4></span></a>
        </div>
        <div class="card-desc">
            <a href=""><h3>{{Config::get('app.locale') == 'fr' ? $item->labelFR:$item->labelENG}}</h3></a>
            <a href=""><p>{{Config::get('app.locale') == 'fr' ? $item->introFR:$item->introENG}}</p>
            </a>
            <div class="card-lire text-right">
                <a href="{{route('singleBlog',['alias'=>Config::get('app.locale') == 'fr' ? $item->aliasFR:$item->aliasENG])}}" >{{__('msg.Lire')}}</a>
            </div>
        </div>
    </div>
</div>
@endforeach
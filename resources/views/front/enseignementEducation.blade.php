@extends('front.layout.layout')
@section('content')
<div class=" gbb-row secEns" style="padding-bottom:30px; background:#f5f5f5;">
    <div class="bb-inner default">
        <div class="bb-container container">
            <div class="row">
                <div class="row-wrapper clearfix">
                    <div class="gsc-column col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="column-inner  bg-size-cover">
                            <div class="column-content-inner">
                                <div class="widget gsc-heading  align-center style-1 text-dark ">
                                    <h3 class="title"><span>{{__('msg.education_titel')}}</span></h3>

                                    <div class="title-desc"><span>{{__('msg.education_sub_titel')}}.</span></div>
                                </div>
                                <div class="clearfix"></div>
                                <a class="anchor" id="maternelle"></a>
                                <div class="widget gsc-box-info  content-align-left" style="min-height: 360px">
                                    <div class="clearfix">
                                        <div class="image"
                                            style="background-image:url('{{ asset('assets/front/images/maternelle.jpg')}}')">
                                        </div>
                                        <div class="content text-dark">
                                            <div class="content-bg"></div>
                                            <div class="content-inner">



                                                <div class="title">
                                                    <h2>{{__('msg.maternelle')}}</h2>
                                                </div>


                                                <div class="desc">{!! __('msg.education_meternelle_text') !!}

                                                </div>

                                                <div class="readmore text-center"><a href="{{route('inscription',session()->get('locale'))}}"
                                                        class="btn-theme btn btn-sm"
                                                        href="enseignement-education.html">{{__('msg.inscription')}}</a>
                                                </div>



                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a class="anchor" id="primaire"></a>
                                <div class="widget gsc-box-info  content-align-right" style="min-height: 360px">
                                    <div class="clearfix">
                                        <div class="image"
                                            style="background-image:url('{{ asset('assets/front/images/primaire.jpg')}}')">
                                        </div>
                                        <div class="content text-dark">
                                            <div class="content-bg"></div>
                                            <div class="content-inner sec-2">



                                                <div class="title">
                                                    <h2>{{__('msg.primaire')}}</h2>
                                                </div>


                                                <div class="desc">
                                                    {!! __('msg.education_primaire_text') !!}

                                                </div>

                                                <div class="readmore text-center"><a href="{{route('inscription',session()->get('locale'))}}"
                                                        class="btn-theme btn btn-sm"
                                                        href="enseignement-education.html">{{__('msg.inscription')}}</a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a class="anchor" id="college"></a>
                                <div class="widget gsc-box-info  content-align-left" style="min-height: 360px">
                                    <div class="clearfix">
                                        <div class="image"
                                            style="background-image:url('{{ asset('assets/front/images/college.jpg')}}')">
                                        </div>
                                        <div class="content text-dark">
                                            <div class="content-bg"></div>
                                            <div class="content-inner sec-3">



                                                <div class="title">
                                                    <h2>{{__('msg.college')}}</h2>
                                                </div>


                                                <div class="desc">
                                                    {!! __('msg.education_college_text') !!}

                                                </div>
                                                <div class="readmore text-center"><a href="{{route('inscription',session()->get('locale'))}}"
                                                        class="btn-theme btn btn-sm"
                                                        href="enseignement-education.html">{{__('msg.inscription')}}</a>
                                                </div>


                                            </div>





                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <a class="anchor" id="centre"></a>
                <div class="widget gsc-box-info  content-align-right" style="min-height: 360px">
                    <div class="clearfix">
                        <div class="image" style="background-image:url('{{ asset('assets/front/images/centre.jpg')}}')">
                        </div>
                        <div class="content text-dark">
                            <div class="content-bg"></div>
                            <div class="content-inner sec-2">
                                <div class="title">
                                    <h2>{{__('msg.centre_orientation')}} </h2>
                                </div>
                                <div class="desc">
                                    {!! __('msg.education_centre_orientation_text') !!}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <a class="anchor" id="lycee"></a>
                <div class="widget gsc-box-info  content-align-left" style="min-height: 360px">
                    <div class="clearfix">
                        <div class="image" style="background-image:url('{{ asset('assets/front/images/lycee.jpg')}}')">
                        </div>
                        <div class="content text-dark">
                            <div class="content-bg"></div>
                            <div class="content-inner">



                                <div class="title">
                                    <h2>{{__('msg.lycee')}}</h2>
                                </div>


                                <div class="desc">
                                    {!! __('msg.education_lycee_text') !!}
                                </div>

                                <div class="readmore text-center"><a href="{{route('inscription',session()->get('locale'))}}"
                                        class="btn-theme btn btn-sm" href="enseignement-education.html">{{__('msg.inscription')}}</a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class=" gbb-row" style="background-color:#144fa3">
    <div class="bb-inner default">
        <div class="bb-container container-fluid">
            <div class="row">
                <div class="row-wrapper clearfix">
                    <div class="gsc-column col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="column-inner  bg-size-cover">
                            <div class="column-content-inner">
                                <div class="widget gsc-call-to-action  button-bottom text-light">
                                    <div class="content-inner clearfix">
                                        <div class="content">
                                            <span class="subtitle">{{__('msg.section5_btn')}}</span>
                                            <h3 class="title"><span> {{__('msg.education_title_2')}}</span></h3>
                                            {{__('msg.education_sub_title_2')}}
                                        </div>
                                        <div class="button-action">
                                            <a href="{{route('inscription',session()->get('locale'))}}" class="btn-2 btn btn-md">
                                                <span>{{__('msg.inscription')}}</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('front.layout.layout')
@section('style')
<style>
    /* Style the buttons */
.btn {
  border: none;
  outline: none;
  padding: 12px 16px;
  background-color: rgb(199, 199, 199);
  cursor: pointer;
  color: black;
}

.btn:hover {
  background-color: #ddd;
}

.btn.active {
  background-color: #666;
  color: white;
}
</style>
@endsection
@section('content')
<section id="gallerie">
    <div class = "container">
        <h1 class="h-t" >{{__('msg.GallerieTitre')}}</h1>
        <div class="row mtb-20">
            <div class="col-xs-12">
                <ul class="nav nav-pills gallery-filters">
                    <li role="presentation" class="active" gallery-filter="all"><a href="#">{{__('msg.All')}}</a></li>
                    @foreach ($albums as $item)
                    <li role="presentation" gallery-filter="{{$item->id}}"><a href="#">{{$item->label}}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>

        <div class="row">
            @foreach ($gallereis as $item)
            <div class = "col-1-of-3 gallery-img-holder" filter-category="{{$item->album}}">
                <div class = "itemm  zoom-out column">
                  <img src="{{asset('back/images/gallery/'.$item->img)}}" alt="" onclick="openModal();currentSlide(1)">
                  {{-- <i class ="fa fa-plus"> </i> <!-- icon--> --}}
                </div>
            </div>
            @endforeach
        </div>
        <!-- The Modal/Lightbox -->
        <div id="myModal" class="modal">
            <span class="close cursor" onclick="closeModal()">&times;</span>
            <div class="modal-content">
                @foreach ($gallereis as $item)
                <div class="mySlides">
                    <div class="numbertext">1 / 9</div>
                    <img src="{{asset('back/images/gallery/'.$item->img)}}" style="width:100%">
                </div>
                @endforeach
                <!-- Next/previous controls -->
                <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                <a class="next" onclick="plusSlides(1)">&#10095;</a>
                <!-- Caption text -->
                <div class="caption-container">
                    <p id="caption"></p>
                </div>

                <!-- Thumbnail image controls -->
            </div>
        </div>


</div>

    </div>

</section>
@endsection

@section('script')
<script>
    // Open the Modal
    function openModal() {
      document.getElementById("myModal").style.display = "block";
    }

    // Close the Modal
    function closeModal() {
      document.getElementById("myModal").style.display = "none";
    }

    var slideIndex = 1;
    showSlides(slideIndex);

    // Next/previous controls
    function plusSlides(n) {
      showSlides(slideIndex += n);
    }

    // Thumbnail image controls
    function currentSlide(n) {
      showSlides(slideIndex = n);
    }

    function showSlides(n) {
      var i;
      var slides = document.getElementsByClassName("mySlides");
      var dots = document.getElementsByClassName("demo");
      var captionText = document.getElementById("caption");
      if (n > slides.length) {slideIndex = 1}
      if (n < 1) {slideIndex = slides.length}
      for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
      }
      for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
      }
      slides[slideIndex-1].style.display = "block";
      dots[slideIndex-1].className += " active";
      captionText.innerHTML = dots[slideIndex-1].alt;
    }
</script>
<script>
    jQuery('document').ready( function($) {
    $('.gallery-filters li').on('click', function() {
        $('.gallery-filters li').removeClass('active');
        $(this).addClass('active');
        filter = $(this).attr('gallery-filter');

        $('.gallery-img-holder').each( function() {
            if (filter == 'all') {
                $(this).fadeIn();
            } else {
                $(this).hide();
                if ($(this).attr('filter-category') == filter) {
                    $(this).fadeIn();
                }
            }
        });
    });
});
</script>
@endsection

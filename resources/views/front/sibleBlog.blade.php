@extends('front.layout.layout')
@section('style')
<!-- Title -->
<title>Babylone - {!! session()->get('locale') == 'fr' ? $blog->metaTitleFR : $blog->metaTitleFR !!} </title>
<meta name="description" content="{!! session()->get('locale') == 'fr' ? $blog->metaDescriptionFR : $blog->metaDescriptionENG !!}">
{{-- facebook meta  --}}
<meta property="og:title" content="{!! session()->get('locale') == 'fr' ? $blog->labelFR : $blog->labelENG !!}">
<!--  title of fb  -->
<meta property="og:image" content="{{$blog->imageOG}}"> <!-- image of fb -->
<meta property="og:image:alt"
	content="{!! session()->get('locale') == 'fr' ? $blog->metaTitleFR : $blog->metaTitleENG !!}">
<!--  alt image of fb -->
<meta property="og:description"
	content="{!! session()->get('locale') == 'fr' ? $blog->metaDescriptionFR : $blog->metaDescriptionENG !!}">
<!--  description if fb -->
{{-- twitter meta --}}
<meta name="twitter:title" content="{!! session()->get('locale') == 'fr' ? $blog->metaTitleFR : $blog->metaTitleENG !!}">
<!--  title of twitter -->
<meta name="twitter:description"
	content="{!! session()->get('locale') == 'fr' ? $blog->metaDescriptionFR : $blog->metaDescriptionENG !!}">
<!-- description of twitter  -->
<meta name="twitter:image" content="{{$blog->imageOG}}"> <!-- image of twitter -->
<meta name="twitter:image:alt"
	content="{!! session()->get('locale') == 'fr' ? $blog->metaTitleFR : $blog->metaTitleENG !!}">
<!-- alt image of twitter -->
{{-- <link rel="stylesheet" media="all" href="{{ asset('assets/front/css/customm.css')}}" /> --}}
@endsection
@section('content')
<div id="single_blog" class="row">
    <!--<h1 class="ht" >Le groupe scolaire Babylone Academy</h1>-->
    <div class="leftcolumn">

      <div class="card">

        <div class="fakeimg" >
            <img src="{{asset('back/images/blogs/'.$blog->banner)}}" alt="">
        </div>
        <h2>{!! session()->get('locale') == 'fr' ? $blog->labelFR : $blog->labelENG !!}</h2>
        <h5>{!! session()->get('locale') == 'fr' ? $blog->introFR : $blog->introENg !!}, {{($blog->created_at->format('F j, Y'))}}</h5>
        <p>{!! session()->get('locale') == 'fr' ? $blog->detailsFR : $blog->detailsENg !!}</p>
      </div>
    </div>
    <div class="rightcolumn">
      <div class="card-right">
        <a href=""><h5>AUTRES ARTICLES</h5></a>
      </div>
      <div class="card-buttom">

        @foreach ($blogs as $item)
        <div class="fake-img">
            <a href="{{route('singleBlog',['alias'=>Config::get('app.locale') == 'fr' ? $item->aliasFR:$item->aliasENG])}}"><img src="{{asset('back/images/blogs/'.$item->image)}}" alt=""></a>
            <a href="{{route('singleBlog',['alias'=>Config::get('app.locale') == 'fr' ? $item->aliasFR:$item->aliasENG])}}"><h5>{!! session()->get('locale') == 'fr' ? $item->labelFR : $item->labelENG !!}</h5></a>
            <a href="{{route('singleBlog',['alias'=>Config::get('app.locale') == 'fr' ? $item->aliasFR:$item->aliasENG])}}"><h4>{!! session()->get('locale') == 'fr' ? $item->introFR : $item->introENg !!}, {{($item->created_at->format('F j, Y'))}}</h4></a>
        </div>
        @endforeach

      </div>

    </div>
</div>
@endsection

@extends('front.layout.layout')
@section('content')
<div class=" gbb-row secEns" style="padding-bottom:30px; ;">
    <div class="bb-inner default">
        <div class="bb-container container">
            <div class="row">
                <div class="row-wrapper clearfix">
                    <div class="gsc-column col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="column-inner  bg-size-cover">
                            <div class="column-content-inner">
                                @include('front.layout.alert')
                                <div class="widget gsc-heading  align-center style-1 text-dark ">
                                    {{-- <p style="color: #6ebb4c;">Année scolaire 2020/2021</p> --}}
                                    <h3 class="title" style="color:#144fa3 !important;"><span>{{__('msg.S_identifier')}} </span></h3>

                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-8 col-md-offset-2">
                                <form method="post" class="pt-1" action="{{--route('etudiant.login')--}}">
                                    
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="text" class="main-input form-control" name="username"
                                                        placeholder="{{__('msg.username')}} *" required oninvalid="this.setCustomValidity('Le nom d\'utilisateur est obligatoire')">
                                                        @if ($errors->has('username'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('username') }}</strong>
                                                        </span>
                                                        @endif
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input class="main-input form-control date-picker" name="password"
                                                        type="password" placeholder="{{__('msg.password')}}" required oninvalid="this.setCustomValidity('Le mot de pass est obligatoire')">
                                                        @if ($errors->has('password'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('password') }}</strong>
                                                        </span>
                                                        @endif
                                                </div>
                                            </div>
                                        </div>
                                        <!-- end row -->
                                        <div class="row text-center">
                                            <button class="btn btn-theme"
                                                style="padding: 15px 15px;">{{__('msg.S_identifier')}}</button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
@endsection

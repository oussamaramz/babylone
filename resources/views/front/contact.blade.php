@extends('front.layout.layout')
@section('content')
<div class=" gbb-row secEns" style="padding-bottom:30px; ;">
    <div class="bb-inner default">
        <div class="bb-container container">
            <div class="row">
                <div class="row-wrapper clearfix">
                    <div class="gsc-column col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="column-inner  bg-size-cover">
                            <div class="column-content-inner">
                                <div class="widget gsc-heading  align-center style-1 text-dark ">
                                    <h3 class="title" style="color:#144fa3;"><span>{{__('msg.NousContactez')}}</span></h3>

                                </div>
                                @include('front.layout.alert')
                                <div class="clearfix"></div>
                                <div class="col-md-8 col-md-offset-2">
                                <form method="post" class="pt-1" action="{{route('messagrieStore')}}">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="main-input form-control" name="nom"
                                                        placeholder="{{__('msg.nom')}} *" required oninvalid="this.setCustomValidity('Le nom est obligatoire')">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input class="main-input form-control " name="prenom" type="text"
                                                        placeholder="{{__('msg.prenom')}} *" required oninvalid="this.setCustomValidity('Le prenom est obligatoire')">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- end row -->
                                        <div class="row">
                                            <div class="col-md-6">

                                            </div>
                                            <div class="col-md-6">

                                            </div>

                                        </div>
                                        <!-- end row -->
                                        <div class="row">

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="email" class="main-input form-control" name="email"
                                                        placeholder="Email *" required oninvalid="this.setCustomValidity('L\'email est obligatoire')">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="main-input form-control" name="tel"
                                                        placeholder="{{__('msg.tel')}} *" required oninvalid="this.setCustomValidity('Le telephone est obligatoire')">
                                                </div>
                                            </div>

                                        </div>
                                        <!-- end row -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <textarea rows="6" class="main-input form-control" name="message"
                                                        placeholder="{{__('msg.message')}}" required oninvalid="this.setCustomValidity('Le message est obligatoire')"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- end row -->
                                        <input type="hidden" name="action" value="validate_captcha">
                                        <div class="row text-center">
                                            <button type="submit" class="btn btn-theme" style="padding: 15px 15px;">{{__('msg.envoyer')}}</button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
@endsection

@section('script')
<script src="https://www.google.com/recaptcha/api.js?render=your reCAPTCHA site key here"></script>
<script>
    grecaptcha.ready(function() {
    // do request for recaptcha token
    // response is promise with passed token
        grecaptcha.execute('your reCAPTCHA site key here', {action:'validate_captcha'})
                  .then(function(token) {
            // add token value to form
            document.getElementById('g-recaptcha-response').value = token;
        });
    });
</script>
@endsection

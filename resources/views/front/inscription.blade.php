@extends('front.layout.layout')
@section('style')
    <style>
        #photoUpload,#noteUpload {
  position: relative;
  font-family: calibri;
  height: 150px;
  padding: 10px;
  margin-bottom: 15px;
  -webkit-border-radius: 5px;
  -moz-border-radius: 5px;
  border: 1px dashed #BBB;
  text-align: center;
  background-color: #DDD;
  cursor: pointer;
}
    </style>
@endsection
@section('content')
<div class=" gbb-row secEns" style="padding-bottom:30px; ;">
    <div class="bb-inner default">
        <div class="bb-container container">
            <div class="row">
                <div class="row-wrapper clearfix">
                    <div class="gsc-column col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="column-inner  bg-size-cover">
                            <div class="column-content-inner">
                                <div class="widget gsc-heading  align-center style-1 text-dark ">
                                    <p style="color: #6ebb4c;">{{__('msg.Annee_scolaire')}}</p>
                                    <h3 class="title" style="color:#144fa3 !important;"><span>{{__('msg.incriptionTitle')}}</span></h3>
                                </div>
                                @include('front.layout.alert')
                                <div class="clearfix"></div>
                                <div class="col-md-8 col-md-offset-2">
                                <form method="post" class="pt-1" action="{{route('Auth.register')}}" enctype="multipart/form-data">
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="main-input form-control" name="nomCompletEtudiant"
                                                        placeholder="{{__('msg.nom_enfant')}} *" required value="{{ old('nomCompletEtudiant') }}" validate-msg="Le nom est obligatoire">
                                                        @if ($errors->has('nomCompletEtudiant'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('nomCompletEtudiant') }}</strong>
                                                        </span>
                                                        @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input class="main-input form-control date-picker" name="dateNaissance"
                                                        type="date" placeholder="{{__('msg.Date_Naissance')}} *" required value="{{ old('dateNaissance') }}" validate-msg="La dtae de naissance obligatoire">
                                                        @if ($errors->has('nomCompletEtudiant'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('nomCompletEtudiant') }}</strong>
                                                        </span>
                                                        @endif
                                                </div>
                                            </div>
                                        </div>
                                        <!-- end row -->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <select name="niveauActuel" class="main-input form-control" required validate-msg="Le niveaux actual obligatoire">
                                                        <option disabled="" selected="">{{__('msg.Niveau_Actuel')}}</option>
                                                        <optgroup label="{{__('msg.maternelle')}}">
                                                            <option value="tps">{{__('msg.tps')}}</option>
                                                            <option value="ps">{{__('msg.Petite_section')}}</option>
                                                            <option value="ms">{{__('msg.Moyenne_section')}}</option>
                                                            <option value="gs">{{__('msg.Grande_section')}}</option>
                                                        </optgroup>
                                                        <optgroup label="{{__('msg.primaire')}}">
                                                            <option value="CP">CP</option>
                                                            <option value="CE1">CE1</option>
                                                            <option value="CE2">CE2</option>
                                                            <option value="CM1">CM1</option>
                                                            <option value="CM2">CM2</option>
                                                            <option value="C6">C6</option>
                                                        </optgroup>
                                                        <optgroup label="{{__('msg.college')}}">
                                                            <option value="1AC">1 AC</option>
                                                            <option value="2AC">2 AC</option>
                                                            <option value="3AC">3 AC</option>
                                                        </optgroup>
                                                        <optgroup label="{{__('msg.lycee')}}">
                                                            <option value="TC">TC</option>
                                                            <option value="1AS">1 AS</option>
                                                            <option value="2AS">2 AS</option>
                                                        </optgroup>
                                                    </select>
                                                    @if ($errors->has('niveauActuel'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('niveauActuel') }}</strong>
                                                        </span>
                                                        @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <select name="niveauDemande" class="main-input form-control" required validate-msg="Le niveau demande obligatoire">
                                                        <option disabled="" selected="">{{__('msg.Niveau_Demande')}}</option>
                                                        <optgroup label="{{__('msg.maternelle')}}">
                                                            <option value="tps">{{__('msg.tps')}}</option>
                                                            <option value="ps">{{__('msg.Petite_section')}}</option>
                                                            <option value="ms">{{__('msg.Moyenne_section')}}</option>
                                                            <option value="gs">{{__('msg.Grande_section')}}</option>
                                                        </optgroup>
                                                        <optgroup label="{{__('msg.primaire')}}">
                                                            <option value="CP">CP</option>
                                                            <option value="CE1">CE1</option>
                                                            <option value="CE2">CE2</option>
                                                            <option value="CM1">CM1</option>
                                                            <option value="CM2">CM2</option>
                                                            <option value="C6">C6</option>
                                                        </optgroup>
                                                        <optgroup label="{{__('msg.college')}}">
                                                            <option value="1AC">1 AC</option>
                                                            <option value="2AC">2 AC</option>
                                                            <option value="3AC">3 AC</option>
                                                        </optgroup>
                                                        <optgroup label="{{__('msg.lycee')}}">
                                                            <option value="TC">TC</option>
                                                            <option value="1AS">1 AS</option>
                                                            <option value="2AS">2 AS</option>
                                                        </optgroup>
                                                    </select>
                                                    @if ($errors->has('niveauDemande'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('niveauDemande') }}</strong>
                                                        </span>
                                                        @endif
                                                </div>
                                            </div>

                                        </div>
                                        <!-- end row -->
                                        <div class="row">

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <select name="sexe" class="main-input form-control" required validate-msg="Le sexe obligatoire">
                                                        <option value="1" selected="">{{__('msg.garcon')}}</option>
                                                        <option value="2">{{__('msg.fille')}}</option>
                                                    </select>
                                                    @if ($errors->has('sexe'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('sexe') }}</strong>
                                                        </span>
                                                        @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="main-input form-control" name="Etablissement_actuel"
                                                        placeholder="{{__('msg.Etablissement_actuel')}} *" required validate-msg="Etablissement actuel obligatoire" value="{{ old('Etablissement_actuel') }}">
                                                        @if ($errors->has('Etablissement_actuel'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('Etablissement_actuel') }}</strong>
                                                        </span>
                                                        @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div id="photoUpload" onclick="getphoto()">{{__('msg.photo')}}</div>
                                                <div style='height: 0px;width: 0px; overflow:hidden;'>
                                                    <input id="photo" name="photo" type="file" onchange="photoFUn(this)" accept="image/*" /></div>
                                            </div>
                                            <div class="col-md-6">
                                                <div id="noteUpload" onclick="getnote()">{{__('msg.bulletins')}}</div>
                                                <div style='height: 0px;width: 0px; overflow:hidden;'>
                                                    <input id="notes" name="notes[]" type="file" onchange="note(this)" multiple /></div>
                                            </div>
                                            </div>
                                        <!-- end row -->
                                        <div class="row">

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="main-input form-control" name="nomCompletTuteur"
                                                        placeholder="{{__('msg.nom_tuteur')}} *" required value="{{ old('nomCompletTuteur') }}" validate-msg="Le nom de tuteur st obligatoire">
                                                        @if ($errors->has('nomCompletTuteur'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('nomCompletTuteur') }}</strong>
                                                        </span>
                                                        @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="text" class="main-input form-control" name="tel"
                                                        placeholder="{{__('msg.tel')}} *" required validate-msg="Le tel obligatoire" value="{{ old('tel') }}">
                                                        @if ($errors->has('tel'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('tel') }}</strong>
                                                        </span>
                                                        @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="email" class="main-input form-control" name="email"
                                                        placeholder="Email *" required validate-msg="L'email obligatoire" value="{{ old('email') }}">
                                                        @if ($errors->has('email'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                        </span>
                                                        @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input type="password" class="main-input form-control" name="password"
                                                        placeholder="{{__('msg.password')}}*" required value="{{ old('password') }}" validate-msg="Le mot de passe obligatoire">
                                                        @if ($errors->has('password'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('password') }}</strong>
                                                        </span>
                                                        @endif
                                                </div>
                                            </div>
                                        </div>
                                        <!-- end row -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <textarea rows="5" class="main-input form-control" name="message"
                                                        placeholder="{{__('msg.message')}}" required validate-msg="Le message obligatoire">{{ old('message') }}</textarea>
                                                        @if ($errors->has('message'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('message') }}</strong>
                                                        </span>
                                                        @endif
                                                </div>
                                            </div>
                                        </div>
                                        <!-- end row -->
                                        <div class="row text-center">
                                            <button class="btn btn-theme" type="submit"
                                                style="padding: 15px 15px;">{{__('msg.Inscrivez-vous')}}</button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
@endsection

@section('script')
    <script>
        $("form :input").each(function(){
                    var input = $(this);
                    var msg   = input.attr('validate-msg');
                    input.on('change invalid input', function(){
                        input[0].setCustomValidity('');
                        if(!(input[0].validity.tooLong || input[0].validity.tooShort)){
                            if (! input[0].validity.valid) {
                                input[0].setCustomValidity(msg);
                            }
                        }


                    });
                });
        
                function getphoto() {
                document.getElementById("photo").click();
                }

                function photoFUn(obj) {
                var file = obj.value;
                var fileName = file.split("\\");
                document.getElementById("photoUpload").innerHTML = fileName[fileName.length - 1];
                event.preventDefault();
                }


                function getnote() {
                document.getElementById("notes").click();
                }

                function note(obj) {
                var file = obj.value;
                var fileName = file.split("\\");
                document.getElementById("noteUpload").innerHTML = fileName[fileName.length - 1];
                event.preventDefault();
                }
    </script>
@endsection

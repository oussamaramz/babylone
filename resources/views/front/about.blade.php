@extends('front.layout.layout')
@section('content')
<div role="main" class="main main-page">

    <div class="clearfix"></div>
    <div class="slideshow_content area">

    </div>

    <div class="help">
        <div class="container">
            <div class="content-inner">
                <div>
                    <div data-drupal-messages-fallback class="hidden"></div>

                </div>

            </div>
        </div>
    </div>

    <div class="fw-before-content area">
        <div>
            <div id="block-gavias-emon-gaviasblockbuideraboutus"
                class="block block-gavias-blockbuilder block-gavias-blockbuilder-blockgavias-blockbuilder-block____6 no-title">


                <div class="content block-content">
                    <div class="gavias-blockbuilder-content">

                        <div class=" gbb-row" style="">
                            <div class="bb-inner default">
                                <div class="bb-container container">
                                <h2 class="h-t">{{__('msg.slider_sub_title_1')}} <br> {{__('msg.slider_sub_title_1-1')}}</h2>
                                    <div class="row">

                                        <div class="row-wrapper clearfix">
                                            <div class="gsc-column col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                <div class="column-inner  bg-size-cover">
                                                    <div class="column-content-inner">

                                                        <div
                                                            class="widget gsc-call-to-action  button-bottom-left text-dark">
                                                            <div class="content-inner clearfix">

                                                                <div class="content">

                                                                    <h3 class="title color-primary">
                                                                        <span style="font-size: 24px;">{{__('msg.Qui-Sommes-Nous')}}</span>
                                                                    </h3>
                                                                    <div
                                                                        class="text-medium text-black margin-bottom-30">
                                                                        {{__('msg.about_sub_title')}}
                                                                    </div>
                                                                    <div class="paragraphe">
                                                                    <p>{!! __('msg.about_text') !!}</p>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="gsc-column col-lg-6 col-md-6 col-sm-12 col-xs-12 my-10">
                                                <div class="column-inner  bg-size-cover">
                                                    <div class="column-content-inner">
                                                        <div class="widget gsc-image text-center ">
                                                            <div class="widget-content">

                                                                <img src="{{ asset('assets/front/images/like.png')}}"
                                                                    alt="" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="margin-bottom-0 pt0 gbb-row mvv" style="">
                            <div class="bb-inner pt0 remove_padding_bottom">
                                <div class="bb-container container-fw">
                                    <div class="row">
                                        <div class="row-wrapper clearfix">
                                            <div class="gsc-column col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="column-inner  bg-size-cover">
                                                    <div class="column-content-inner">
                                                        <a class="anchor" id="mission"></a>
                                                        <div class="widget gsc-box-info  content-align-left"
                                                            style="min-height: 500px">
                                                            <div class="clearfix">
                                                                <div class="image"
                                                                    style="background-image:url('{{ asset('assets/front/images/mission.jpg')}}')">
                                                                </div>
                                                                <div class="content text-light"
                                                                    style="background-color: #f3f3f3">
                                                                    <div class="content-bg"
                                                                        style="background-color:  #f3f3f3">
                                                                    </div>
                                                                    <div id="ab" class="content-inner">



                                                                        <div class="title">
                                                                            <h2 style="color: #144fa3; font-weight: 700"
                                                                                ;>{{ __('msg.Mission') }}</h2>
                                                                        </div>


                                                                        <div class="desc" style="color: #000000;"> Tout
                                                                            {{__('msg.mission_text')}}
                                                                        </div>


                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <a class="anchor" id="vision"></a>
                                                        <div class="widget gsc-box-info  content-align-right"
                                                            style="min-height: 500px">
                                                            <div class="clearfix">
                                                                <div class="image"
                                                                    style="background-image:url('{{ asset('assets/front/images/vision.jpg')}}')">
                                                                </div>
                                                                <div class="content text-light"
                                                                    style="background-color: #f3f3f3">
                                                                    <div class="content-bg"
                                                                        style="background-color: #f3f3f3">
                                                                    </div>
                                                                    <div id="ab" class="content-inner">



                                                                        <div class="title">
                                                                            <h2
                                                                                style="color: #6ebb4c; font-weight: 700">
                                                                                {{__('msg.Vision')}}</h2>
                                                                        </div>


                                                                        <div class="desc" style="color: #000000;">Le
                                                                            {{__('msg.Vision_text')}}
                                                                        </div>


                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <a class="anchor" id="Valeurs"></a>
                                                        <div class="widget gsc-box-info  content-align-left"
                                                            style="min-height: 500px">
                                                            <div class="clearfix">
                                                                <div class="image"
                                                                    style="background-image:url('{{ asset('assets/front/images/valeurs.jpg')}}')">
                                                                </div>
                                                                <div class="content text-light"
                                                                    style="background-color: #f3f3f3">
                                                                    <div class="content-bg"
                                                                        style="background-color: #f3f3f3">
                                                                    </div>
                                                                    <div id="ab" class="content-inner">



                                                                        <div class="title">
                                                                            <h2
                                                                                style="color: #ed213e; font-weight: 700">
                                                                                {{__('msg.Valeurs')}}</h2>
                                                                        </div>


                                                                        <div class="desc" style="color: #000000;">
                                                                            {{-- {{__('msg.Valeurs_text')}} --}}
                                                                            <ul>
                                                                                {!! __('msg.valeurs_list') !!}
                                                                            </ul>
                                                                        </div>


                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a class="anchor" id="Projet_Pedagogique"></a>
                        <div class=" gbb-row prjPeda">
                            <div class="bb-inner default">
                                <div class="bb-container container">
                                    <div class="row">
                                        <div class="row-wrapper clearfix">
                                            <div class="gsc-column col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="widget gsc-heading  align-center style-1 text-dark ">
                                                    <h3 class="title"><span>{{__('msg.Projet_Pedagogique')}}</span>
                                                    </h3>

                                                    <div class="title-desc"><span>{{__('msg.About_Projet_Pedagogique_subtitle')}}</span>
                                                    </div>
                                                </div>
                                                <div class="column-inner  bg-size-cover">
                                                    <div class="column-content-inner">

                                                        <div class="clearfix"></div>
                                                        <div class="gsc-column col-lg-6 col-md-4 col-sm-12 col-xs-12">
                                                            <div class="column-inner  bg-size-cover">
                                                                <div class="column-content-inner">
                                                                    <div class="widget gsc-team team-vertical">
                                                                        <div class="widget-content text-center">
                                                                            <div class="team-header text-center">
                                                                                <img alt="John joe"
                                                                                    src="{{ asset('assets/front/images/parent.jpg')}}"
                                                                                    class="img-responsive">
                                                                            </div>
                                                                            <div class="team-body text-center">
                                                                                <div class="team-body-content">
                                                                                    <h3 class="team-name">
                                                                                        {{__('msg.about_img_1')}}</h3>


                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="gsc-column col-lg-6 col-md-4 col-sm-12 col-xs-12">
                                                            <div class="column-inner  bg-size-cover">
                                                                <div class="column-content-inner">
                                                                    <div class="widget gsc-team team-vertical">
                                                                        <div class="widget-content text-center">
                                                                            <div class="team-header text-center">
                                                                                <img alt="John joe"
                                                                                    src="{{ asset('assets/front/images/epanouissement.jpg')}}"
                                                                                    class="img-responsive">
                                                                            </div>
                                                                            <div class="team-body text-center">
                                                                                <div class="team-body-content">
                                                                                    <h3 class="team-name">
                                                                                        {{__('msg.about_img_2')}}
                                                                                    </h3>
                                                                                    <div class="team-paragraphe">

                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="gsc-column col-lg-6 col-md-4 col-sm-12 col-xs-12">
                                                            <div class="column-inner  bg-size-cover">
                                                                <div class="column-content-inner">
                                                                    <div class="widget gsc-team team-vertical">
                                                                        <div class="widget-content text-center">
                                                                            <div class="team-header text-center">
                                                                                <img alt="John joe"
                                                                                    src="{{ asset('assets/front/images/communication.jpg')}}"
                                                                                    class="img-responsive">
                                                                            </div>
                                                                            <div class="team-body text-center">
                                                                                <div class="team-body-content">
                                                                                    <h3 class="team-name">
                                                                                        {{__('msg.about_img_3')}}
                                                                                    </h3>
                                                                                    <div class="team-paragraphe">

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="gsc-column col-lg-6 col-md-4 col-sm-12 col-xs-12">
                                                            <div class="column-inner  bg-size-cover">
                                                                <div class="column-content-inner">

                                                                    <div class="widget gsc-team team-vertical">
                                                                        <div class="widget-content text-center">
                                                                            <div class="team-header text-center">
                                                                                <img alt="John joe"
                                                                                    src="{{ asset('assets/front/images/innovation.jpg')}}"
                                                                                    class="img-responsive">
                                                                            </div>
                                                                            <div class="team-body text-center">
                                                                                <div class="team-body-content">
                                                                                    <h3 class="team-name">
                                                                                        {{__('msg.about_img_4')}}
                                                                                    </h3>
                                                                                    <div class="team-paragraphe">

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="gsc-column col-lg-6 col-md-4 col-sm-12 col-xs-12">
                                                            <div class="column-inner  bg-size-cover">
                                                                <div class="column-content-inner">

                                                                    <div class="widget gsc-team team-vertical">
                                                                        <div class="widget-content text-center">
                                                                            <div class="team-header text-center">
                                                                                <img alt="John joe"
                                                                                    src="{{ asset('assets/front/images/citoy.jpg')}}"
                                                                                    class="img-responsive">
                                                                            </div>
                                                                            <div class="team-body text-center">
                                                                                <div class="team-body-content">
                                                                                    <h3 class="team-name">
                                                                                        {{__('msg.about_img_5')}}
                                                                                    </h3>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="gsc-column col-lg-6 col-md-4 col-sm-12 col-xs-12">
                                                            <div class="column-inner  bg-size-cover">
                                                                <div class="column-content-inner">
                                                                    <div class="widget gsc-team team-vertical">
                                                                        <div class="widget-content text-center">
                                                                            <div class="team-header text-center">
                                                                                <img alt="John joe"
                                                                                    src="{{ asset('assets/front/images/critique.jpg')}}"
                                                                                    class="img-responsive">
                                                                            </div>
                                                                            <div class="team-body text-center">
                                                                                <div class="team-body-content">
                                                                                    <h3 class="team-name">
                                                                                        {{__('msg.about_img_6')}}
                                                                                    </h3>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>



                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

            </div>

        </div>

        <div class="clearfix"></div>
        <div class="clearfix"></div>
        <div id="content" class="content content-full">
            <div class="container">
                <div class="content-main-inner">
                    <div class="row">



                        <div id="page-main-content" class="main-content col-md-12 col-xs-12">

                            <div class="main-content-inner">


                                <div class="content-main">
                                    <div>
                                        <div id="block-gavias-emon-content"
                                            class="block block-system block-system-main-block no-title">


                                            <div class="content block-content">


                                                <article data-history-node-id="81" role="article"
                                                    typeof="schema:WebPage"
                                                    class="node node--type-page node--view-mode-full clearfix">
                                                    <header>


                                                    </header>
                                                    <div class="node__content clearfix">

                                                    </div>
                                                </article>

                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>




    </div>
</div>
@endsection

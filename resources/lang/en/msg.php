<?php

return [
    'accueil' => 'Home',
    'babylone_Academy' => 'Babylon Academy',
    'enseignement' => 'Education',
    'maternelle' => 'KINDERGARTEN',
    'primaire' => 'Primary',
    'college' => 'SECONDARY SCHOOL',
    'lycee' => 'High school',
    'centre_orientation' => 'Orientation Center',
    'epanouissement' => 'Clubs',
    'activite_theatre' => 'Theater activity',
    'activite_coding__robotique' => 'Coding and Robotics activity',
    'activite_lecture' => 'Reading activity',
    'activite_arts_plastiques' => 'Plastic arts activity',
    'ctivite_sport' => 'Sport activity',
    'nouveautes' => 'News',
    'gallerie' => 'Gallery',
    'acces_parent' => 'Parent Space',
    'inscription' => 'ENROLLMENT',
    'NousContactez' => 'Contact us',

    'slider_title_1' =>'A LIVING SPACE FOR FULFILLMENT',
    'slider_title_1-1' =>'AND YOUR CHILD\'S DEVELOPMENT',
        'slider_sub_title_1' =>'Babylone Academy',
    'slider_sub_title_1-1' =>'school',
    'slider_sub_title_1-2' =>'Kindergarten - Primary - College - High School',
        'Decouvrir_Plus' =>'Learn more',
        'slider_sub_title_2' =>'Babylone Academy  school ',
    'slider_sub_title_2-1' =>'Development ',
    'slider_sub_title_2-2' =>'of autonomy',
    'slider_sub_title_3-1' =>'Openness to ',
    'slider_sub_title_3-2' =>'other cultures',
    'Qui-Sommes-Nous' =>'Who are we ?',
        'section1_p' =>'Babylone Academy offers training and
        a high value educational project. While relying on
        skills essential for
        personality development
        as :',
        'Communication' =>'Communication',
        'Cooperation' =>'Cooperation',
        'Responsabilisation'=>'RESPONSABILITY',
        'Autonomie'=>'Autonomy',
        'Notre_Mission'=>'OUR MISSION',
        'Notre_Mission_p'=>'While maintaining a high degree of professional expertise at all levels.',
        'Savoir_Plus'=>'Know more',
        'Notre_Vision'=>'Our vision',
        'Notre_Vision_p'=>'The Babylon School group
        Academy has a vision
        oriented towards learning and
        the development of the child ...',
        'Nos_Valeurs'=>'Our values',
        'Nos_Valeurs_p'=>'The values of the educational project of
        Babylone Academy group are
        as follows ...',
        'Projet_Pedagogique'=>'Educational project',
        'Projet_Pedagogique_title'=>'Babylone Academy pays particular  attention to the progress of each child using an open and adapted pedagogy',
        'Projet_Pedagogique_img1'=>'We associate the
        families and
        parents',
        'Projet_Pedagogique_img2'=>'We promote the personal development of the child
        Opening up our students to other cultures
        ',
        'Projet_Pedagogique_img3'=>'We promote language learning',
        'Projet_Pedagogique_img4'=>'We promote openness to other cultures',
        'Projet_Pedagogique_img5'=>'Citizenship and
        civic values',
        'Projet_Pedagogique_img6'=>'Development of the mind and critical thinking',
        'Enseignement_Education'=>'Teaching, Education and Orientation',
        'section_p1'=>'Kinderkarten level  of the School is well adapted to childrens with a specific support to  their transitions and, taking into  consideration their development. The cycle of Kinderkartne organizes specific modalities of learning.',
        'section_p2'=>'The transition to primary  level is an important stage for the development of childrens. Babylon Academy offers a dynamic  teaching and welcoming  framework, where students learn while having pleasure and happiness.',
        'section_p3'=>'The secondary School inside Babylone Academy privileges a global training of the personality in order to awaken potentialities of each student: intellectual, moral, physical, spiritual, and reveal his qualities which are always the sign of a possible trajectory and a vocation.',
        'section_p4'=>'High school is a time of revelation. Our students are invited to assert themselves more and take responsibilities. Because we don\'t knows well that in the gift of self. babylone Academy supports its students in the choice of studies after the Baccalaureate. The orientation center is created for this purpose.',
        'section5_titre'=>'Accompany your child',
        'section5_sub_titre'=>'The Babylone Academy school group will support your child in his education and development. Trust us and you will see the results.',
        'section5_btn'=>'Join us',

        'about_sub_title'=>'The Babylone Academy (GBA) school offers training and an educational project with high added value.',
        'about_text'=>'With their teaching experiences and education, the founders of Babylone Academy, have designed an innovative and educational offer meeting the requirements of the development of your child.<br><br>
        The school educational project is based mainly on the development of suc caracteristics such as : <br><br>Personality, Communication,Cooperation,Responsability and the Autonomy of the child. All our  professors adopts  an approach centered on child while meeting the demands of good citizenship and respect of others.<br><br>
        The Babylone Academy  school, is a living environment where each child created projects based on his fields of interest, develop his autonomy,  express his/her self using technologies and various means, learn to manage time and its ambitions.  Activities extracurricular and the different clubs (Theater, reading, music, IT / Robotics, arts plastic) are moments for developping  softs  Skills of your child.<br><br>
        In a spirit of openness to other cultures, the Babylone Academy school allowed of primary importance to learn foreign languages. Besides the French and English from a young age and throughout secondary school, children are brought to talk and practice several modern languages like Chinese and Spanish. Many projects are set to developp the children tability of communication.<br><br>
        From its creation, the school has set set up a guidance and orientation center and partnerships with schools and universities in abroad to promote culture of openness in favor of our students. <br><br>
        Indeed the E-learning platform of  Babylone Academy offers a distance learning environment and  promote assimilation, interactivity and permanent communication between the school, the children, parents and school administration.
        ',
        'Mission'=>'MISSION',
        'mission_text'=>'By maintaining a high degree of professional expertise at all levels, the Babylone Academy school’s mission is to develop people who stand out through their capacity for reflection and action in their living environment.',
        'Vision'=>'Vision',
        'Vision_text'=>'The Babylone Academy has a vision oriented towards the learning and development of the child. The Babylone Academy School offers a personalized setting for each child, respecting their pace and development.',
        'Valeurs'=>'Values',
        'Valeurs_text'=>'The values of the Babylone Academy group\'s educational project are as follows:',
        'valeurs_list'=>'<li> Personal development </li>
        <li> Cooperation and openness </li>
        <li> Involvement and commitment </li>
        <li> Recognition and encouragement </li>',
        'About_Projet_Pedagogique_subtitle'=>'Babylone Academy\'s educational project pays particular attention to the progress of each child through open and adapted pedagogy.',
        'about_img_1'=>'We involve families and parents',
        'about_img_2'=>'We promote the development and personal development of the child',
        'about_img_3'=>'We promote language learning and openness to other cultures',
        'about_img_4'=>'Learning foreign languages and opening up to other cultures',
        'about_img_5'=>'Citizenship and civic values',
        'about_img_6'=>'Development of the mind and critical thinking',


        'education_titel'=>'Teaching, education and guidance',
        'education_sub_titel'=>'The Babylone Academy (GBA) school group offers training and an educational project with high added value.',
        'centre_orientation'=>'Orientation Center',
        'education_meternelle_text'=>'The Kinderkaten  of the babylone academy school is  a unique cycle including  fundamental for the success of the child.
        The Kinderkaten  of Babylone Academy is well  adapted to children with specific support for their transitions, taking into consideration the specificities of child  development. 
        ',
        'education_primaire_text'=>'The transition to elementary school is an important stage for the development of children. Babylone Academy offers a dynamic, rigorous and welcoming teaching environment, where students learn while having fun.
        To ensure that your child acquires all the fundamental knowledge that will allow him to develop hi/Her intellectual capacities and talents , Babylone Academy follows an innovative training program.
        ',
        'education_college_text'=>'The secondary inside  Babylone Academy favors a global conception of the personality in order to awaken the potentialities of each child: intellectual, moral, physical, spiritual, and reveal his/her qualities which are always the sign of a possible trajectory and  a vocation. 
        Developing a taste for a job well done, acquiring a good and solid general culture, encouraging a sense of effort and perseverance, consolidating the achievements through rigorous learning are our educational objectives.
        In order to prepare our students for the national exams, a knowledge assessment is organized every year for all levels.
        ',
        'education_lycee_text'=>'The time of high school is a period of revelation. Our students are invited to be more assertive and to take responsibilities by managing project in real life. A teaching team and professors has the constant mission to make everyone grow. Succeeding in high school at Babylone Academy requires in-depth and regular work. The students are helped by a competent, experienced, demanding and very attentive teaching staff. The aim of teachers is to develop the academic qualities essential for success in exams, as well as a good general culture.',
        'education_centre_orientation_text'=>'The Babylone Academy guidance center and orientation aims to participate in the identification process of student’s career and future orientation. Whether in individual or group meetings, the orientation center implements a series of activities related to self-knowledge, studies, the job market and admission to universities abroad. 
        Among the tools available to them, students have access to a various documentation center containing a wealth of information on study programs and different professions.
        ',
        'education_title_2'=>'Accompany your child',
        'education_sub_title_2'=>'The Babylone Academy school group will support your child in his education and development. Trust us and you will see the results.',



        'blog-title'=>'Blog',
        'Charge_plus'=>'Loaded more',
        'Lire'=>'Read',
        'GallerieTitre'=>'Galerie',
        'All'=>'All',
        'S_identifier'=>'login',
        'username'=>'Username',
        'password'=>'Password',

        'Annee_scolaire'=>'2020/2021 school year',
        'incriptionTitle'=>'Registration open !',
        'nom_enfant'=>'Name and first name of the child',
        'Date_Naissance'=>'Date of Birth',
        'Niveau_Actuel'=>'Actual level',
        'Petite_section'=>'Little section',
        'Moyenne_section'=>'Middle section',
        'Grande_section'=>'Big section',
        'tps'=>'Very Small Section',
        'Niveau_Demande'=>'Required level',
        'nom_tuteur'=>'Name and first name of the tutor',
        'tel'=>'Phone',
        'message'=>'Message',
        'Inscrivez-vous'=>'Sign up',

        'nom'=>'Fisrt Name',
        'prenom'=>'Last Name',
        'envoyer'=>'send',

        // LE CLUB THÉATRE
    'theatreTitre'=>'LE CLUB THÉATRE',
    'theatre_text'=>'The theater activity  brings an important help at the level of the oral expression, the development of the artistic faculties, the spatial organization, the contact with the written word, the discovery of one\'s body and others. Childrens apprehend new means of oral and physical expression.
    The theatrical techniques aim to make the individual not only more comfortable in his body but also in the handling of his language and his behavior. Two factors taht are essential to overall success. The theatrical activity opens up multiple horizons for childrens:
    ',
        'theatre_list'=>'<li>Opportunity to express oneself through speech and gesture.</li>
    <li>Discovery of new situations.</li>
    <li>Expression of unforeseen feelings.</li>
    <li>Victory over shyness.</li>
    <li>Better knowledge of others.</li>
    <li>Public Speaking.',
    'theatre_img1'=>'End of year party 2019/2020',
    'theatre_img2'=>'Independence Day',
    'theatre_img3'=>'End of year party 2018/2019',
    // LE CLUB CODING
    'codingTitre'=>'THE CODING AND ROBOTICS CLUB',
    'coding_text'=>'Learning to code requires a programming language. And like all languages: the sooner you learn, the better is! At Babylone Academy, we offer a children\'s activity where they learn to program using a programmable robot that they build themselves.<br><br>
    The type of programmable robot we use is one of a kind and designed specifically for a kid\'s activity. To learn how to code their robots, students use two children\'s programming languages depending on their level. For a child, the most fascinating aspect of the programmable robot is the metamorphosis of computer code into physical action in the real world. 
    ',
    'coding_img1'=>'End of year 2019/2020 project',
    'coding_img2'=>'"Creative Kid" competition',
    'coding_img3'=>'Parent-Student Workshop',
    // LE CLUB LECTURE
    'lectureTitre'=>'THE READING CLUB',
    'lecture_text'=>'Reading activity is one of the best strategies for forming a community, creating a common understanding base for discussion,modeling expressive, fluid and motivating reading. <br><br>
    The activity consists of reading a carefully chosen book and involving them by asking questions, making predictions, finding the meanings of words and exploring various reading strategies. All students participate in the activity.
    ',
    'lecture_img1'=>'End of year party 2019/2020',
    'lecture_img2'=>'"ReadLife" Competition',
    'lecture_img3'=>'Collective conference workshop',

    // LE CLUB Plastiques
    'plastiqueTitre'=>'Le Club Des Arts Plastiques',
    'plastique_text'=>'The visual arts activity develops in children the sense of creation and the representation of real objects. This activity stimulates creativity in children while realizing miniaturized representations with a lot of sense of development. Manual work is privileged at this level.',
    'plastique_img1'=>'End of year party 2019/2020',
    'plastique_img2'=>'"ReadLife" Competition',
    'plastique_img3'=>'Collective conference workshop',

    // LE CLUB sport
    'sportTitre'=>'Le Club De Sport',
    'sport_text'=>'Sport is good for the body but also for the mind. Sports activity is therefore essential for the well-being of the child. In additionto playing a role in the psychomotor development of the child, "sport accompanies him well beyond the borders of the field, itis the school of life". </p>
<p> Meeting opponents or playing with teammates allows , in addition, to develop sociability, team spirit, but also respect for others. On the social level, the sport practiced in a club widens the relationships of the child outside the school context. The intellectual level is not to be outdone. Sport helps speed up decision-making and promotes concentration',
    'sport_img1'=>'Football competition',
    'sport_img2'=>'Swimming competition',
    'sport_img3'=>'E-Sport Competition',


         // ---------------------------------------------------------------------------------------------------------
        // footre
        'footre_about'=>'The Babylone Academy (GBA) school group offers training and an educational project with high added value ...',
        'Liens_Utiles'=>'Useful links',
        'Information_contact'=>'Contact information',
        'developpar'=>'Developped by',
        'Copyright'=>'Copyright © 2020 Babylone Academy. All rights reserved.',

        // inscription add
    'sexe'=>'gender',
    'Etablissement_actuel'=>'Current establishment',
    'photo'=>'click to upload your photo',
    'bulletins'=>'click to download your newsletters',
    'Autre_f_s'=>'Other Brother (s) or Sister (s)',
    'Autre_f_s_Descrpt'=>'Please specify for each student: The current level, The requested class, The first name, The gender, The date of birth and the current establishment (1 student per line)',
    'garcon'=>'Boy',
    'fille'=>'Girl',
];

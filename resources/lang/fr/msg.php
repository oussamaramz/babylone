<?php

return [
    'accueil' => 'Accueil',
    'babylone_Academy' => 'Babylone Academy',
    'enseignement' => 'Enseignement',
    'maternelle' => 'Maternelle',
    'primaire' => 'Primaire',
    'college' => 'Collège',
    'lycee' => 'Lycée',
    'centre_orientation' => 'Centre d’Orientation',
    'epanouissement' => 'Epanouissement',
    'activite_theatre' => 'Activité Théâtre',
    'activite_coding__robotique' => 'Activité Coding et Robotique',
    'activite_lecture' => 'Activité Lecture',
    'activite_arts_plastiques' => 'Activité Arts plastiques',
    'ctivite_sport' => 'Activité Sport',
    'nouveautes' => 'Nouveautés',
    'gallerie' => 'Gallerie',
    'acces_parent' => 'Accès Parent',
    'inscription' => 'Inscription',
    'maternelle' => 'Maternelle',
    'primaire' => 'Primaire',
    'college' => 'Collège',
    'lycee' => 'Lycée',
    'NousContactez' => 'Nous-Contactez',

    // ---------------------------------------------------------------------------------------------------------------
    // acceuil
    // ---------------------- slider
    'slider_title_1' =>'UN ESPACE DE VIE POUR L\'ÉPANOUISSEMENT',
    'slider_title_1-1' =>'ET LE DÉVELOPPEMENT DE VOTRE ENFANT',
    'slider_sub_title_1' =>'Groupe Scolaire',
    'slider_sub_title_1-1' =>'Babylone Academy',
    'slider_sub_title_1-2' =>'Maternelle - Primaire - Collège - Lycée',
    'Decouvrir_Plus' => 'Découvrir Plus',
    // --------------
    'slider_sub_title_2' =>'Groupe Scolaire Babylone Academy',
    'slider_sub_title_2-1' =>'Développement',
    'slider_sub_title_2-2' =>'de l\'autonomie',
    // --------------
    'slider_sub_title_3-1' =>'L’ouverture ',
    'slider_sub_title_3-2' =>'sur d’autres cultures',

    // ------------ section 1
    'Qui-Sommes-Nous' => 'Qui Sommes-Nous ?',
    'section1_p' => 'Babylone Academy est un groupe scolaire qui offre une formation et un projet éducatif à forte valeur ajoutée. Tout en se basant sur des compétences indispensables pour le développement de la personnalité 
    comme :',
    'Communication' => 'Communication',
    'Cooperation' => 'Coopération',
    'Responsabilisation'=>'Responsabilisation',
    'Autonomie'=>'Autonomie',
    // ----------- section 2
    'Notre_Mission'=>'NOTRE MISSION',
    'Notre_Mission_p'=>'Tout en maintenant un haut degré
    d’expertise professionnelle à
    tous les niveaux ..',
    'Savoir_Plus'=>'Savoir Plus',
    'Notre_Vision'=>'Notre Vision',
    'Notre_Vision_p'=>'Le groupe Scolaire Babylone
    Academy dispose d’une vision
    orientée vers l’apprentissage et
    l’épanouissement de l’enfant...',
    'Nos_Valeurs'=>'Nos Valeurs',
    'Nos_Valeurs_p'=>'Les valeurs du projet éducatif du
    groupe Babylone Academy sont
    come suit...',
    // ----------------------- section 3
    'Projet_Pedagogique'=>'Projet Pédagogique',
    'Projet_Pedagogique_title'=>'Le projet
    Pédagogique
    de
    Babylone Academy porte une attention
    particulière à la progression de chaque enfant
    grâce à une pédagogie ouverte et adaptée.',
    'Projet_Pedagogique_img1'=>'Nous associons les
    familles et les
    parents',
    'Projet_Pedagogique_img2'=>'Nous favorisons
    l\'épanouissement et
    le développement
    personnel de
    l\'enfant',
    'Projet_Pedagogique_img3'=>'Nous favorisons l’apprentissage des langues étrangères',
    'Projet_Pedagogique_img4'=>'Nous favorisons l\'ouverture sur d\'autres cultures',
    'Projet_Pedagogique_img5'=>'Citoyenneté et
    valeurs civiques',
    'Projet_Pedagogique_img6'=>'    Développement de
    l\'esprit et
    réflexion critique',

    //  ------------------ section 4
    'Enseignement_Education'=> 'Enseignement et Éducation',
    'section_p1'=>'L’école maternelle
    du groupe scolaire babylone
    academy s’adapte aux enfants
    avec un accompagnement
    spécifique dans leurs
    transitions, en tenant compte de
    leur développement. Le cycle de
    la maternelle organise des
    modalités spécifiques
    d’apprentissage.',
    'section_p2'=>'Le passage au
    primaire est une étape
    importante pour le développement
    des enfants. Babylone Academy
    propose un cadre d’enseignement
    dynamique, rigoureux et
    accueillant, où les élèves
    apprennent tout en ayant du
    plaisir.',
    'section_p3'=>'Le collège au sein
    de Babylone Academy privilégie
    une formation globale de la
    personnalité afin d’éveiller les
    potentialités de chaque élève:
    intellectuelle, morale,
    physique, spirituelle, et
    révéler ses qualités qui sont
    toujours le signe d’une
    trajectoire possible et d’une
    vocation.',
    'section_p4'=>'Le temps du lycée
    est un temps de révélation. Nos
    élèves sont invités à s’affirmer
    davantage et à prendre des
    responsabilités. Car on ne se
    connait bien que dans le don de
    soi.',

    //  ------------------------------------- seection 5
    'section5_titre'=>'Accompagnez votre enfant',
    'section5_sub_titre'=>'Le groupe scolaire Babylone Academy
    accompagne votre enfant dans son
    éducation et son développement .Faites
    nous confiance et vous verrez le
    résultat.',
    'section5_btn'=>'Nous-Rejoindre',


    // ---------------------------------------------------------------------------------------------------------------
    // about

    'about_sub_title'=>'Babylone Academy est un groupe scolaire qui offre une formation et un projet éducatif à forte valeur ajoutée. Tout en se basant sur des compétences indispensables pour le développement de la personnalité ',
    'about_text'=>'En capitalisant sur  leurs expériences en enseignement et éducation, les fondateurs du groupe Babylone Academy, ont conçu une offre pédagogique innovante répondant aux impératives du développement et l’épanouissement de votre enfant.<br><br>
    Le projet éducatif du groupe Scolaire Babylone Academy est basé principalement sur le développement de la personnalité, de la communication, de la coopération, de la responsabilisation et de l’autonomie de l’enfant. Le corps professoral privilégie une approche centrée sur l’enfant, non sur la matière, tout en répondant aux exigences du civisme et du respect de l’autre.<br><br>
    Le groupe scolaire Babylone Academy, est un milieu de vie où chaque enfant crée des projets basés sur ses champs d’intérêts, développe son autonomie, s’exprime en utilisant les technologies et divers moyens artistiques, apprend à gérer son temps et ses ambitions, à bien se connaître et à coopérer avec les autres. Les activités parascolaires et les différents clubs (Théâtre, lecture, musique, informatique/Robotique, arts plastiques) sont des moments pour développer les softs Skills de votre enfant.<br><br>
    Dans un esprit d’ouverture sur d’autres cultures, le Groupe scolaire Babylone Academy accorde une importance principale à l’apprentissage des langues étrangères. Outre le Français et l’anglais, dès le jeune âge et durant toute la scolarité au sein du groupe, les enfants sont amenés à parler et pratiquer plusieurs langues vivantes comme le chinois et l’espagnol. De nombreux projets accompagnent les enfants tout au long de l’année scolaire.<br><br>
    Dès sa création, le groupe scolaire Babylone Academy a mis en place un centre d’orientation et des partenariats avec des écoles et universités à l’étranger pour favoriser la culture d’ouverture chez nos jeunes élèves et étudiants. Les technologies de l’information et de la communication font partie intégrante de l’offre pédagogique du groupe scolaire Babylone Academy .<br><br>
    En effet la plate forme d’enseignement à distance de Babylone Academy offre un environnement d’apprentissage en ligne favorisant l’assimilation, l’interactivité et la communication permanente entre le corps professoral, les enfants, les parents et la direction de l’école.
    ',

    'Mission'=>'MISSION',
    'mission_text'=>'en maintenant un haut degré d’expertise professionnelle à tous les niveaux, le groupe scolaire Babylone Academy a pour mission de développer des personnes qui se démarquent par leur capacité de réflexion et d’action dans leur milieu de vie.',
    'Vision'=>'Vision',
    'Vision_text'=>'groupe Scolaire Babylone Academy dispose d’une vision orientée vers l’apprentissage et l’épanouissement de l’enfant. Le Groupe scolaire babylone Academy offre un cadre personnalisé à chaque enfant en respectant son rythme et son développement.',
    'Valeurs'=>'Valeurs',
    'Valeurs_text'=>'Les valeurs du projet éducatif du groupe Babylone Academy sont come suit :',
    'valeurs_list'=>'<li>Développement personnel</li>
    <li>Coopération et ouverture</li>
    <li>Implication et engagement</li>
    <li>Reconnaissance et encouragements</li>',

    'About_Projet_Pedagogique_subtitle'=>'Le projet Pédagogique de Babylone Academy porte une attention particulière à la progression de chaque enfant grâce à une pédagogie ouverte et adaptée.',
    'about_img_1'=>'Nous associons les familles et les parents',
    'about_img_2'=>'Nous favorisons l\'épanouissement et le développement personnel de l\'enfant',
    'about_img_3'=>'Nous favorisons l\'apprentissage des langues et l\'ouverture sur d\'autres cultures',
    'about_img_4'=>'Apprentissage des langues étrangères et ouverture sur d\'autres cultures',
    'about_img_5'=>'Citoyenneté et valeurs civiques',
    'about_img_6'=>'Développement de l\'esprit et réflexion critique',

    // ----------------------------------------------------------------------------------------------------
    // education
    'education_titel'=>'Enseignement, éducation et Orientation',
    'education_sub_titel'=>'Le groupe scolaire Babylone Academy (GBA) offre une formation et un projet éducatif à forte valeur ajoutée.',
    'centre_orientation'=>'Centre d’Orientation',
    'education_meternelle_text'=>'La maternelle du groupe scolaire babylone academy s’adapte aux enfants avec un accompagnement spécifique et personnalisé par rapport à chaque enfant. Le cycle de la maternelle organise des modalités spécifiques d’apprentissage et de découvertes',
    'education_primaire_text'=>'Le passage au primaire est une étape importante pour le développement des enfants. Babylone Academy propose un cadre d’enseignement dynamique, rigoureux et accueillant, où les élèves apprennent tout en ayant du plaisir.<br>Pour s’assurer que votre enfant acquière toutes les connaissances fondamentales qui lui permettront de développer ses capacités intellectuelles et ses talents, Babylone Academy suit un programme de formation innovant.',
    'education_college_text'=>'Le collège au sein de Babylone Academy privilégie une formation globale de la personnalité afin d’éveiller les potentialités de chaque élève: intellectuelle, morale, physique, spirituelle, et révéler ses qualités qui sont toujours le signe d’une trajectoire possible et d’une vocation. La qualité du travail scolaire est notre priorité.<br> Développer le goût du travail bien fait, acquérir une bonne et solide culture générale, encourager le sens de l’effort et de la persévérance, consolider les acquis par un apprentissage rigoureux sont nos objectifs pédagogiques.<br> Afin de préparer nos élèves aux examens nationaux, une évaluation des connaissances est organisée chaque année pour tous les niveaux.',
    'education_lycee_text'=>'Le temps du lycée est un temps de révélation. Nos élèves sont invités à s’affirmer davantage et à prendre des responsabilités. Car on ne se connait bien que dans le don de soi. Une équipe pédagogique constituée autour d\'un programme des études encadre et accompagne les élèves. <br> Elle a la mission constante de faire grandir chacun. Réussir ses études en lycée à Babylone Academy demande un travail approfondi et régulier. Les élèves sont aidés par un corps professoral compétent, expérimenté, exigeant et très attentif à chacun. L’objectif des enseignants est de développer les qualités scolaires indispensables à la réussite aux examens, ainsi qu’une bonne culture générale.',
    'education_centre_orientation_text'=>'Le centre d’orientation de Babylone Academy a pour objectif de participer au développement identitaire des élèves et de les soutenir dans leurs différentes démarches en vue de déterminer leur choix de carrière. Que ce soit en rencontres individuelles ou de groupe, le centre d’orientation met en œuvre une série d’activités liées à la connaissance de soi, aux études, au marché du travail et à l’admission aux universités, et ce, dès l’entame du Lycée. <br> Parmi les outils mis à leur disposition, les élèves ont accès à un centre de documentation regroupant une multitude de renseignements sur les programmes d’études et les différentes professions.',
    'education_title_2'=>'Accompagnez votre enfant',
    'education_sub_title_2'=>'Le groupe scolaire Babylone Academy accompagne votre enfant dans son éducation et son développement .Faites nous confiance et vous verrez le résultat.',

    // ------------------------------------------------------------------------------------------------------
    // blog gallery sing in sing up
    'blog-title'=>'Blog',
    'Charge_plus'=>'Chargé plus',
    'Lire'=>'Lire',
    'GallerieTitre'=>'Gallerie',
    'All'=>'toute',
    'S_identifier'=>'S\'identifier',
    'username'=>'Nom d\'utilisateur',
    'password'=>'Mot de passe',

    'Annee_scolaire'=>'Année scolaire 2020/2021',
    'incriptionTitle'=>'Inscriptions ouvertes !',
    'nom_enfant'=>'Nom et Prénom de l\'enfant',
    'Date_Naissance'=>'Date de Naissance',
    'Niveau_Actuel'=>'Niveau Actuel',
    'Petite_section'=>'Petite section',
    'Moyenne_section'=>'Moyenne section',
    'Grande_section'=>'Grande section',
    'tps'=>'Toute Petite Section',
    'Niveau_Demande'=>'Niveau Demandé',
    'nom_tuteur'=>'Nom et prénom du tuteur',
    'tel'=>'Téléphone',
    'message'=>'Message',
    'Inscrivez-vous'=>'Inscrivez-vous',

    'nom'=>'Nom',
    'prenom'=>'Prenom',
    'envoyer'=>'envoyer',

    // ---------------------------------------------------------------------------------------------------------
    // LE CLUB THÉATRE
    'theatreTitre'=>'LE CLUB THÉATRE',
    'theatre_text'=>'Le théâtre pour enfants, intégrée à la pédagogie, apporte une aide importante au niveau de l’expression orale, du développement des facultés artistiques, de l’organisation spatiale, du contact avec l’écrit, de la découverte de son corps et des autres par de nouveaux moyens d’expression.</p>
<p> Les techniques théâtrales visent à rendre l’individu non seulement plus à l’aise dans son corps mais également dans le maniement de sa langue et de son comportement deux facteurs essentiels à sa réussite globale. L’activité théâtrale ouvre chez l’enfant des horizons multiples :',
    'theatre_list'=>'<li>Possibilité de s’exprimer par le geste et la parole.</li>
<li>Découverte de situations nouvelles.</li>
<li>Expression de sentiments imprévus.</li>
<li>Victoire sur la timidité.</li>
<li>Meilleure connaissance des autres.</li>',
    'theatre_img1'=>'Fête fin d\'année 2019/2020',
    'theatre_img2'=>'Fête fin de l\'indépendence',
    'theatre_img3'=>'Fête fin d\'année 2018/2019',
     // LE CLUB CODING
     'codingTitre'=>'LE CLUB CODING ET ROBOTIQUE',
     'coding_text'=>'Apprendre à coder, ça passe par un langage de programmation. Et comme tous les langages : plus tôt on apprend, mieux c’est ! Chez Babylone Academy , nous proposons une activité enfant où ils apprennent à programmer grâce à <b> un robot programmable </b> qu’ils construisent eux-même.</p>
    <p style="font-size: 19px;">Le type de robot programmable que nous utilisons est unique en son genre et conçu spécifiquement pour une activité enfant. Pour apprendre à coder leurs robots, les élèves utilisent deux langages de programmation pour enfants en fonction de leur niveau.</p>
<p style="font-size: 19px;"> Pour un enfant, l’aspect le plus fascinant du robot programmable c’est la métamorphose du code informatique en action physique dans le monde réel.',
     'coding_list'=>'<li>Possibilité de s’exprimer par le geste et la parole.</li>
 <li>Découverte de situations nouvelles.</li>
 <li>Expression de sentiments imprévus.</li>
 <li>Victoire sur la timidité.</li>
 <li>Meilleure connaissance des autres.</li>',
     'coding_img1'=>'Projet fin d\'année 2019/2020',
     'coding_img2'=>'Compétition "Creative Kid"',
     'coding_img3'=>'Atelier Parent-Elève',

     // LE CLUB LECTURE
    'lectureTitre'=>'LE CLUB DE LECTURE',
    'lecture_text'=>'L’activité de lecture est l’une des meilleures stratégies pour former une communauté, créer une base de compréhension commune pour la discussion, modéliser une lecture expressive, fluide et motivante</p>
<p style="font-size: 20px;">L’activité consiste à lire un livre choisi avec soin et à les faire participer en posant des questions, en faisant des prédictions, en cherchant le sens des mots et en explorant diverses stratégies de lecture. Tous les élèves participent à l’activité.',
    'lecture_img1'=>'Fête fin d\'année 2019/2020',
    'lecture_img2'=>'Competition "ReadLife"',
    'lecture_img3'=>'Atelier lecture collectif',
    // LE CLUB Plastiques
    'plastiqueTitre'=>'Le Club Des Arts Plastiques',
    'plastique_text'=>' L’activité de lecture est l’une des meilleures stratégies pour former une communauté, créer une base de compréhension commune  pour la discussion,  modéliser une lecture  expressive, fluide et  motivante</p>
<p style="font-size: 20px;">L’activité consiste à lire un livre choisi avec soin et à les faire participer en posant des questions, en faisant des prédictions, en cherchant le sens des mots et en explorant diverses stratégies de lecture. Tous les élèves participent à l’activité.',
    'plastique_img1'=>'Fête fin d\'année 2019/2020',
    'plastique_img2'=>'Competition "ReadLife"',
    'plastique_img3'=>'Atelier plastique collectif',

    // LE CLUB sport
    'sportTitre'=>'Le Club De Sport',
    'sport_text'=>'Le sport, c’est bon pour le corps mais aussi pour l’esprit.</p>
<p> En plus de jouer un rôle dans le développement psychomoteur de l’enfant, « le sport l’accompagne bien au-delà des frontières du terrain, c’est l’école de la vie », Rencontrer des adversaires ou jouer avec des coéquipiers permet, en outre, de développer la sociabilité, l\'esprit d\'équipe, mais aussi le respect de l’autre.</p>
<p>Une activité sportive est donc indispensable pour le bien-être de l’enfant.</p>
<p>Sur le plan social, le sport pratiqué dans un club élargit les relations de l’enfant hors contexte scolaire. Le plan intellectuel n’est pas en reste. Le sport permet d’accélérer la prise de décision et favorise la concentration.',
    'sport_img1'=>'Compétition de football',
    'sport_img2'=>'Compétition de natation',
    'sport_img3'=>'Compétition E-Sport',

    // ---------------------------------------------------------------------------------------------------------
    // footre
    'footre_about'=>'Le groupe scolaire Babylone Academy (GBA) offre une formation et un projet éducatif à forte valeur ajoutée...',
    'Liens_Utiles'=>'Liens Utiles',
    'Information_contact'=>'Information de contact',
    'developpar'=>'Développé par',
    'Copyright'=>'Copyright © 2020 Babylone Academy. Tous Droits Réservés.',







    // inscription add
    'sexe'=>'sexe',
    'Etablissement_actuel'=>'Etablissement actuel',
    'photo'=>'cliquez pour télécharger Votre Photo',
    'bulletins'=>'cliquez pour télécharger Votres bulletins ',
    'Autre_f_s'=>'Autre(s) Frère(s) ou Soeur(s)',
    'Autre_f_s_Descrpt'=>'Merci de préciser pour chaque élève: Le niveau actuel, La classe demandée, Le prénom, Le sexe, La date de naissance et l\'établissement actuel (1 élève par ligne)',
    'garcon'=>'garcon',
    'fille'=>'fille',
];

-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 29, 2020 at 02:16 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `babylone`
--

-- --------------------------------------------------------

--
-- Table structure for table `albums`
--

CREATE TABLE `albums` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `albums`
--

INSERT INTO `albums` (`id`, `label`, `alias`, `created_at`, `updated_at`) VALUES
(1, 'album 1', '1-album-1', '2020-11-26 01:09:14', '2020-11-26 01:09:14'),
(2, 'album 2', '2-album-2', '2020-11-26 01:09:50', '2020-11-26 01:09:50'),
(4, 'album 3', '4-album-3', '2020-11-27 15:18:32', '2020-11-27 15:18:32');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `aliasFR` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aliasENG` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `labelFR` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `labelENG` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `introFR` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `introENg` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `detailsFR` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `detailsENg` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `banner` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metaTitleFR` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metaDescriptionFR` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keyWordsFR` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metaTitleENG` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metaDescriptionENG` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keyWordsENG` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `imageOG` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `enebled` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `aliasFR`, `aliasENG`, `labelFR`, `labelENG`, `introFR`, `introENg`, `detailsFR`, `detailsENg`, `image`, `banner`, `metaTitleFR`, `metaDescriptionFR`, `keyWordsFR`, `metaTitleENG`, `metaDescriptionENG`, `keyWordsENG`, `imageOG`, `enebled`, `created_at`, `updated_at`) VALUES
(4, '4-article-babylone-1', '4-babylon-article-1', 'Article babylone 1', 'Babylon article 1', 'Introduction de article 1', 'Introduction of article 1', 'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l\'imprimerie depuis les années 1500, quand un imprimeur anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n\'a pas fait que survivre cinq siècles, mais s\'est aussi adapté à la bureautique informatique, sans que son contenu n\'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.  Pourquoi l\'utiliser? On sait depuis longtemps que travailler avec du texte lisible et contenant du sens est source de distractions, et empêche de se concentrer sur la mise en page elle-même. L\'avantage du Lorem Ipsum sur un texte générique comme \'Du texte. Du texte. Du texte.\' est qu\'il possède une distribution de lettres plus ou moins normale, et en tout cas comparable avec celle du français standard. De nombreuses suites logicielles de mise en page ou éditeurs de sites Web ont fait du Lorem Ipsum leur faux texte par défaut, et une recherche pour \'Lorem Ipsum\' vous conduira vers de nombreux sites qui n\'en sont encore qu\'à leur phase de construction. Plusieurs versions sont apparues avec le temps, parfois par accident, souvent intentionnellement (histoire d\'y rajouter de petits clins d\'oeil, voire des phrases embarassantes).   D\'où vient-il? Contrairement à une opinion répandue, le Lorem Ipsum n\'est pas simplement du texte aléatoire. Il trouve ses racines dans une oeuvre de la littérature latine classique datant de 45 av. J.-C., le rendant vieux de 2000 ans. Un professeur du Hampden-Sydney College, en Virginie, s\'est intéressé à un des mots latins les plus obscurs, consectetur, extrait d\'un passage du Lorem Ipsum, et en étudiant tous les usages de ce mot dans la littérature classique, découvrit la source incontestable du Lorem Ipsum. Il provient en fait des sections 1.10.32 et 1.10.33 du \"De Finibus Bonorum et Malorum\" (Des Suprêmes Biens et des Suprêmes Maux) de Cicéron. Cet ouvrage, très populaire pendant la Renaissance, est un traité sur la théorie de l\'éthique. Les premières lignes du Lorem Ipsum, \"Lorem ipsum dolor sit amet...\", proviennent de la section 1.10.32.  L\'extrait standard de Lorem Ipsum utilisé depuis le XVIè siècle est reproduit ci-dessous pour les curieux. Les sections 1.10.32 et 1.10.33 du \"De Finibus Bonorum et Malorum\" de Cicéron sont aussi reproduites dans leur version originale, accompagnée de la traduction anglaise de H. Rackham (1914).', 'What is Lorem Ipsum? Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.  Why do we use it? It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).   Where does it come from? Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.  The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.', '2020-11-27-16-46-31-Image-centre2.jpeg', '2020-11-27-16-45-57-Banner-1.jpg', 'Article babylone 1', 'Introduction de article 1', 'key1,key2,key3', 'Babylon article 1', 'Introduction of article 1', 'key en1,key en2,key en3', '2020-11-27-16-45-57-ogImage-abtsec.jpg', 1, '2020-11-25 02:02:08', '2020-11-27 16:46:31'),
(5, '5-article-babylone-2', '5-babylon-article-2', 'Article babylone 2', 'Babylon article 2', 'Introduction de article 2', 'Introduction of article 2', 'Qu\'est-ce que le Lorem Ipsum?\r\nLe Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l\'imprimerie depuis les années 1500, quand un imprimeur anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n\'a pas fait que survivre cinq siècles, mais s\'est aussi adapté à la bureautique informatique, sans que son contenu n\'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.\r\n\r\nPourquoi l\'utiliser?\r\nOn sait depuis longtemps que travailler avec du texte lisible et contenant du sens est source de distractions, et empêche de se concentrer sur la mise en page elle-même. L\'avantage du Lorem Ipsum sur un texte générique comme \'Du texte. Du texte. Du texte.\' est qu\'il possède une distribution de lettres plus ou moins normale, et en tout cas comparable avec celle du français standard. De nombreuses suites logicielles de mise en page ou éditeurs de sites Web ont fait du Lorem Ipsum leur faux texte par défaut, et une recherche pour \'Lorem Ipsum\' vous conduira vers de nombreux sites qui n\'en sont encore qu\'à leur phase de construction. Plusieurs versions sont apparues avec le temps, parfois par accident, souvent intentionnellement (histoire d\'y rajouter de petits clins d\'oeil, voire des phrases embarassantes).\r\n\r\n\r\nD\'où vient-il?\r\nContrairement à une opinion répandue, le Lorem Ipsum n\'est pas simplement du texte aléatoire. Il trouve ses racines dans une oeuvre de la littérature latine classique datant de 45 av. J.-C., le rendant vieux de 2000 ans. Un professeur du Hampden-Sydney College, en Virginie, s\'est intéressé à un des mots latins les plus obscurs, consectetur, extrait d\'un passage du Lorem Ipsum, et en étudiant tous les usages de ce mot dans la littérature classique, découvrit la source incontestable du Lorem Ipsum. Il provient en fait des sections 1.10.32 et 1.10.33 du \"De Finibus Bonorum et Malorum\" (Des Suprêmes Biens et des Suprêmes Maux) de Cicéron. Cet ouvrage, très populaire pendant la Renaissance, est un traité sur la théorie de l\'éthique. Les premières lignes du Lorem Ipsum, \"Lorem ipsum dolor sit amet...\", proviennent de la section 1.10.32.\r\n\r\nL\'extrait standard de Lorem Ipsum utilisé depuis le XVIè siècle est reproduit ci-dessous pour les curieux. Les sections 1.10.32 et 1.10.33 du \"De Finibus Bonorum et Malorum\" de Cicéron sont aussi reproduites dans leur version originale, accompagnée de la traduction anglaise de H. Rackham (1914).', 'What is Lorem Ipsum?\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\nWhy do we use it?\r\nIt is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).\r\n\r\n\r\nWhere does it come from?\r\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.\r\n\r\nThe standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.', '2020-11-27-16-54-54-Image-critique.jpg', '2020-11-27-16-54-54-Banner-coll.jpg', 'Article babylone 2', 'Introduction de article 2', 'key1', 'Babylon article 2', 'Introduction of article 2', 'key', '2020-11-27-16-54-54-ogImage-critique.jpg', 1, '2020-11-27 16:54:54', '2020-11-27 16:54:54');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `key`, `value`, `alias`, `created_at`, `updated_at`) VALUES
(1, 'email', 'contact@babyloneacademy.ma', '', '2020-11-25 17:07:21', '2020-11-25 17:55:54'),
(2, 'adress', '82 rue de Rome , 2mars', '', '2020-11-25 17:07:21', '2020-11-25 18:03:30'),
(3, 'tel', '06 25 66 35 16', '', '2020-11-25 17:07:22', '2020-11-25 18:03:30'),
(4, 'facebook', 'https://www.facebook.com/', '', '2020-11-25 17:07:22', '2020-11-25 18:03:30'),
(5, 'instagram', 'https://www.instagram.com/', '', '2020-11-25 17:07:22', '2020-11-25 18:03:30'),
(6, 'youtube', 'https://www.youtube.com/', '', '2020-11-25 17:07:22', '2020-11-25 18:03:30'),
(7, 'adresseENG', '82 rue de Rome , 2mars ENG', '', '2020-11-25 17:30:16', '2020-11-25 18:03:30');

-- --------------------------------------------------------

--
-- Table structure for table `etudiants`
--

CREATE TABLE `etudiants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role` int(11) NOT NULL,
  `nomCompletEtudiant` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nomCompletTuteur` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `niveauActuel` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `niveauDemande` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `Etablissement_actuel` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `messageInscription` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dateNaissance` date NOT NULL,
  `gender` int(11) DEFAULT NULL,
  `enabled` datetime DEFAULT NULL,
  `active` tinyint(4) DEFAULT 0,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `etudiants`
--

INSERT INTO `etudiants` (`id`, `role`, `nomCompletEtudiant`, `nomCompletTuteur`, `niveauActuel`, `niveauDemande`, `Etablissement_actuel`, `messageInscription`, `tel`, `email`, `password`, `image`, `dateNaissance`, `gender`, `enabled`, `active`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 2, 'oussama ramz', 'hassan ramz', 'CP', 'CE1', '', 'test meg', '0601151786', 'oussama@blinkagency.com', '$2y$10$DPhib3B0C1rrWd3IJUfJleHG79KOyDiw2UtGXSDiQg6JruiOCOQIu', NULL, '2020-11-17', NULL, '2020-11-27 15:16:48', 1, NULL, '2020-11-25 02:18:52', '2020-11-27 15:16:48'),
(2, 2, 'taha yahya', 'yassine yahya', 'CM1', 'C6', '', 'no message', '0612166938', 'yassineyahya@gmail.com', '$2y$10$xmLUxgRwo5KMBxkVWSYzTOGkpg3bC9qPMDy7/me8JS3YQ2ep5.isK', NULL, '2008-02-12', NULL, '2020-11-27 15:16:54', 0, NULL, '2020-11-27 11:19:11', '2020-11-27 15:16:54'),
(4, 2, 'taha yahya', 'yassine yahya', 'CP', 'gs', '', 'test email', '0601151786', 'yassineyahya@gmail.com', '$2y$10$AsEQc/NRASfIRymK5u8veuB1E4H8vKN93nb89wG7iA04qatZQv/tW', NULL, '2020-11-05', NULL, '2020-11-27 14:36:46', 1, NULL, '2020-11-27 11:35:10', '2020-11-27 14:36:46'),
(5, 2, 'hassan elalaoui', 'abdeladim elalaoui', 'ps', 'ms', '', 'test', '0638794884', 'abdeladim.alaoui@gail.com', '$2y$10$54OqGFOqt/WsoNXmlKqOdeIgcyBeJpnvX4A0x2zcSt0LleyCqtJeO', NULL, '2020-11-11', NULL, '2020-11-27 17:54:00', 0, NULL, '2020-11-27 16:31:22', '2020-11-27 17:54:00'),
(16, 2, 'test new', 'test tutor', 'gs', 'CM2', 'nice ecole', 'gdfgdgdfgdfgfgf', '0621445577', 'test.test@gmail.com', '$2y$10$0zsRj6rWdR8cJBj6m9So0O5Hfwu.kKKJcRnNGawowrSpNeNRv0EQ2', '2020-12-16-16-16-05-photo-w3logo.jpg', '2020-12-10', 1, NULL, 0, NULL, '2020-12-16 15:16:05', '2020-12-16 15:16:05');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `album` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `img`, `album`, `created_at`, `updated_at`) VALUES
(17, '2020-11-27-16-24-32-gallery-centre2.jpeg', 1, '2020-11-27 16:24:32', '2020-11-27 16:24:32'),
(18, '2020-11-27-16-24-32-gallery-citoy.jpg', 1, '2020-11-27 16:24:32', '2020-11-27 16:24:32'),
(19, '2020-11-27-16-24-41-gallery-Beneylu-Psst_Kit-Karel_Travail-atelier-classe.jpg', 2, '2020-11-27 16:24:41', '2020-11-27 16:24:41'),
(20, '2020-11-27-16-24-41-gallery-centre.jpg', 2, '2020-11-27 16:24:41', '2020-11-27 16:24:41'),
(21, '2020-11-27-16-24-49-gallery-cole-maternelle.jpg', 4, '2020-11-27 16:24:49', '2020-11-27 16:24:49'),
(22, '2020-11-27-16-24-49-gallery-coll.jpg', 4, '2020-11-27 16:24:49', '2020-11-27 16:24:49');

-- --------------------------------------------------------

--
-- Table structure for table `messagries`
--

CREATE TABLE `messagries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `lu` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `messagries`
--

INSERT INTO `messagries` (`id`, `nom`, `prenom`, `email`, `tel`, `message`, `lu`, `created_at`, `updated_at`) VALUES
(2, 'ramz', 'oussama', 'oussamaramz.dev@gmail.com', '0601151786', 'ff', 1, '2020-11-27 10:09:19', '2020-11-27 10:16:20'),
(3, 'ramz', 'oussama', 'oussamaramz.dev@gmail.com', '0601151786', 'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l\'imprimerie depuis les années 1500, quand un imprimeur anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n\'a pas fait que survivre cinq siècles, mais s\'est aussi adapté à la bureautique informatique, sans que son contenu n\'en soit modifié. Il a été popularisé dans les années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.', 1, '2020-11-27 10:10:16', '2020-11-27 17:47:53');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(5, '2020_11_21_213903_create_etudiants_table', 2),
(6, '2020_11_24_122337_create_blogs_table', 3),
(7, '2020_11_25_093348_create_contacts_table', 4),
(8, '2020_11_25_101703_create_messagries_table', 5),
(9, '2020_11_25_145057_create_galleries_table', 6),
(10, '2020_11_25_162400_create_albums_table', 7),
(11, '2020_12_16_153728_create_notes_table', 8);

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

CREATE TABLE `notes` (
  `id` int(10) UNSIGNED NOT NULL,
  `etudiant` int(11) NOT NULL,
  `noteFile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notes`
--

INSERT INTO `notes` (`id`, `etudiant`, `noteFile`, `created_at`, `updated_at`) VALUES
(1, 16, '2020-12-16-16-16-05-note-Remarques sur le site Web de babylone version du 1410.docx', '2020-12-16 15:16:05', '2020-12-16 15:16:05');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role` int(11) NOT NULL,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prenom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `tel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dateNaissance` date DEFAULT NULL,
  `gender` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role`, `nom`, `prenom`, `email`, `email_verified_at`, `tel`, `password`, `image`, `dateNaissance`, `gender`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', 'admin', 'admin@email.com', '2020-11-27 16:22:20', '0600000000', '$2y$10$bf6hCUL0TONrkQh0gk1tBOzLY5RUGP1ZffUSAFhdmSbM7DHscZH4.', NULL, NULL, 1, 'JBOVpZ3Dy7KCiHhkzVvN4ShBMYx3WiaRy2hRJSoeFjHVHhdXxUXWCuwwP4eh', '2020-11-20 10:13:02', '2020-11-27 16:22:18');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `albums`
--
ALTER TABLE `albums`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `etudiants`
--
ALTER TABLE `etudiants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messagries`
--
ALTER TABLE `messagries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `albums`
--
ALTER TABLE `albums`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `etudiants`
--
ALTER TABLE `etudiants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `messagries`
--
ALTER TABLE `messagries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `notes`
--
ALTER TABLE `notes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return redirect(session()->get('locale'));
})->name('babylone')->middleware('setlocale');

Route::get('/single_blog/{alias}', function ($alias) {
    if (session()->get('locale') == 'fr') {
        $blog = \App\Blog::where('aliasFR',$alias)->first();
    }else{
        $blog = \App\Blog::where('aliasENG',$alias)->first();
    }
    $blogs = \App\Blog::orderBy('id', 'desc')->take(5)->get();
    $navkey = 'home';
    return view('front.sibleBlog',compact('blog','navkey','blogs'));
})->name('singleBlog')->middleware('setlocale');


// languageSwitch
Route::get('language-switch/{local}', 'LangController@languageSwitch')->name('languageSwitch');

Route::group([
    'prefix' => '{locale}',
  'middleware' => 'setlocale',
],function () {

Route::get('/', function () {
    $navkey = 'home';
    return view('front.index',compact('navkey'));
})->name('home');

Route::get('/a_propos_de_nous', function () {
    $navkey = 'about';
    return view('front.about',compact('navkey'));
})->name('about');

Route::get('/enseignement_education', function () {
    $navkey = 'enseignementEducation';
    return view('front.enseignementEducation',compact('navkey'));
})->name('enseignementEducation');

Route::get('/clubtheatre', function () {
    $navkey = 'epanouissement';
    return view('front.clubtheatre',compact('navkey'));
})->name('clubtheatre');

Route::get('/clubcoding', function () {
    $navkey = 'epanouissement';
    return view('front.clubcoding',compact('navkey'));
})->name('clubcoding');

Route::get('/clublecture', function () {
    $navkey = 'epanouissement';
    return view('front.clublecture',compact('navkey'));
})->name('clublecture');

Route::get('/clubartplastique', function () {
    $navkey = 'epanouissement';
    return view('front.clubartplastique',compact('navkey'));
})->name('clubartplastique');

Route::get('/clubsport', function () {
    $navkey = 'epanouissement';
    return view('front.clubsport',compact('navkey'));
})->name('clubsport');

Route::get('/inscription', function () {
    $navkey = 'inscription';
    return view('front.inscription',compact('navkey'));
})->name('inscription');

Route::get('/contact', function () {
    $navkey = 'contact';
    return view('front.contact',compact('navkey'));
})->name('contact');

Route::get('/s_identifier', function () {
    $navkey = 's_identifier';
    return view('front.s_identifier',compact('navkey'));
})->name('s_identifier');

Route::get('/nouveautes', function (\Illuminate\Http\Request $request) {
    // $blogs = \App\Blog::where('enebled',1)->get();
    // $navkey = 'nouveautes';
    // return view('front.nouveautes',compact('blogs','navkey'));


    $blogs = \App\Blog::where('enebled',1)->orderBy('created_at', 'desc')->paginate(15);
    $navkey = 'nouveautes';
    if ($request->ajax()) {
        $view = view('front.loadmoreBlog', compact('blogs'))->render();
        return response()->json(['html'=>$view]);
    }

    return view('front.nouveautes',compact('blogs','navkey'));
})->name('nouveautes');

Route::get('/gallerie', function () {
    $gallereis = \App\Gallerie::get();
    $albums = \App\Album::get();
    $navkey = 'gallerie';
    return view('front.gallerie',compact('gallereis','albums','navkey'));
})->name('gallerie');

});

// messagrei route
Route::post('messagrieStore', 'MessagrieController@store')->name('messagrieStore');


Route::post('register', 'Auth\RegisterController@create')->name('Auth.register');


//admin routes

Route::prefix('admin/')->group(function () {

    Route::get('/profile','UserController@profile')->name('admin.profile');
    Route::post('/profileUpdate','UserController@profileUpdate')->name('admin.profileUpdate');
    /* Auth routes****************/
    Route::get('/login', function () {
        return view('back.admin.adminLogin');
    })->name('adminLogin');
    Route::post('login', 'Auth\LoginController@login')->name('Auth.login');
    Route::get('logout', 'Auth\LoginController@logout')->name('Auth.logout');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('Auth.password.email');
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('Auth.password.request');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('Auth.password.reset');


    // routes etudiant
    Route::get('/dashboard', 'UserController@dash')->name('admin.dash');
    Route::get('/subscribers', 'UserController@subscribers')->name('admin.subscribers');
    Route::get('/etudiants', 'UserController@etudiants')->name('admin.etudiants');
    Route::get('/Etudiant/Details/{id}', 'UserController@EtudiantDetails')->name('admin.EtudiantDetails');
    Route::get('/Etudiant/Edit/{id}', 'UserController@EtudiantEdit')->name('admin.EtudiantEdit');
    Route::post('/Etudiant/update/{id}', 'UserController@EtudiantUpdate')->name('admin.EtudiantUpdate');
    Route::get('/Etudiant/Delete/{id}', 'UserController@EtudiantDelete')->name('admin.EtudiantDelete');
    Route::get('/Etudiant/enabled/{id}', 'UserController@EtudiantEnabled')->name('admin.EtudiantEnabled');
    Route::post('/Etudiant/password/{id}', 'UserController@EtudiantpasswordEdit')->name('admin.EtudiantpasswordEdit');

    // routes blog
    Route::get('/blogs', 'BlogController@index')->name('admin.blogs');
    Route::get('/blog_details/{id}', 'BlogController@BlogDetails')->name('admin.BlogDetails');
    Route::get('/ajouter_blog', 'BlogController@blogAdd')->name('admin.blogAdd');
    Route::post('/blog_store', 'BlogController@blogStore')->name('admin.blogStore');
    Route::get('/blog_active/{id}', 'BlogController@blogActive')->name('admin.blogActive');
    Route::get('/blog_edit/{id}', 'BlogController@blogEdit')->name('admin.blogEdit');
    Route::post('/blog_uodate/{id}', 'BlogController@blogUpdate')->name('admin.blogUpdate');
    Route::get('/blog_delete.{id}', 'BlogController@blogDelete')->name('admin.blogDelete');

    // contact route
    Route::get('/contact_config', 'UserController@contactConfig')->name('admin.contactConfig');
    Route::post('/contact_update', 'UserController@contactUpdate')->name('admin.contactUpdate');

    // messagrie route
    Route::get('/messagrie', 'UserController@messagrieList')->name('admin.messagrieList');
    Route::get('/message_details/{id}', 'UserController@messagedetails')->name('admin.messagedetails');
    Route::get('/deleteMessage/{id}', 'UserController@deleteMessage')->name('admin.deleteMessage');

    // album route
    Route::get('/album', 'UserController@albumList')->name('admin.albumList');
    Route::post('/store_album', 'UserController@albumStore')->name('admin.albumStore');
    Route::post('/album_update', 'UserController@albumUpdate')->name('admin.albumUpdate');
    Route::get('/album_delete/{id}', 'UserController@albumDelete')->name('admin.albumDelete');

    // gallerie route
    Route::get('/galleries', 'UserController@gallerieList')->name('admin.gallerieList');
    Route::get('/ajoute-gallerie', 'UserController@gallerieAdd')->name('admin.gallerieAdd');
    Route::post('/store_gallerie', 'UserController@gallerieStore')->name('admin.gallerieStore');
    Route::get('/delete_gallerie/{id}', 'UserController@galleryDelete')->name('admin.galleryDelete');
});


// clear cache
Route::get('/clear', function() {
    Artisan::call('cache:clear');
       Artisan::call('config:clear');
       Artisan::call('config:cache');
       Artisan::call('view:clear');
       Artisan::call('storage:link');
      return "Cleared!";
});


//etudiant routes
// Route::prefix('etudiant/')->group(function () {

//     /* Auth routes****************/
//     Route::post('login', 'Auth\etudiant\LoginController@login')->name('etudiant.login');
//     Route::get('logout', 'Auth\etudiant\LoginController@logout')->name('etudiant.logout');
//     Route::post('password/email', 'Auth\etudiant\ForgotPasswordController@sendResetLinkEmail')->name('etudiant.password.email');
//     Route::get('password/reset', 'Auth\etudiant\ForgotPasswordController@showLinkRequestForm')->name('etudiant.password.request');
//     Route::post('password/reset', 'Auth\etudiant\ResetPasswordController@reset');
//     Route::get('password/reset/{token}', 'Auth\etudiant\ResetPasswordController@showResetForm')->name('etudiant.password.reset');


//     // routes
//     Route::get('/dashboard', 'EtudiantController@dash')->name('etudiant.dash');

// });
